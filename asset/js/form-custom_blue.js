var switcheryBlueCustomForm = function () {
    return {
        init: function () {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

            // Colored switches
            var blue_1 = document.querySelector('.js-switch-blue-1');
            var switchery = new Switchery(blue_1, {
                color: '#17c3e5'
            });
            var blue_2 = document.querySelector('.js-switch-blue-2');
            var switchery = new Switchery(blue_2, {
                color: '#17c3e5'
            });
        }
    };
}();

$(function () {
    "use strict";
    switcheryBlueCustomForm.init();
});