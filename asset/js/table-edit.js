var editableTable = function () {

    var oTable;

    function events() {

        $('.datatable').on("click", "a.delete", function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
        });
    }

    function plugins() {
        oTable = $('.datatable').dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-6'l <'toolbar'>><'col-xs-6'f>r>t<'datatable-bottom'<'pull-left'i><'pull-right'p>>"
        });

        $('.chosen').chosen({
            width: "80px"
        });
    }

    return {
        init: function () {
            events();
            plugins();
        }
    };
}();

$(function () {
    "use strict";
    editableTable.init();
});