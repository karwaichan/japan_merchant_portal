var dateFormPickers = function () {

    function plugins() {

        $('.datepicker').datepicker();
    }

    return {
        init: function () {
            plugins();
        }
    };
}();

$(function () {
    "use strict";
    dateFormPickers.init();
});