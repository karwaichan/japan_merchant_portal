                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">News and Events</h4>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table id="newsevents" class="table table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Created Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($newsevents) && !empty($newsevents)): ?>
                                                <?php foreach($newsevents as $newsevent): ?>
                                                    <tr class="list-repair-order">
                                                        <td><a href="<?= base_url(); ?>newsevent/newseventdetail/<?= $newsevent->id ?>"><p class="text-primary"><?= $newsevent->title ?></p></a></td>
                                                        <td><?= gmdate("m/d/Y", $newsevent->created_at/1000) ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
        $(document).ready(function() {
            $('#newsevents').DataTable( {
                destroy: true,
                "order": [[ 1, "desc" ]],
                "language": {
                    "lengthMenu": "Display _MENU_ records per page",
                    "zeroRecords": "No matching records found",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "Showing 0 to 0 of 0 entries",
                    "infoFiltered": "(filtered from _MAX_ total entries)"
                }
            } );
        } );
    </script>
    <!-- /page script -->