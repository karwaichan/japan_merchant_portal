                        <section class="panel">
                            <header class="panel-heading">
                                <h4><?= $newsevent->title ?></h4>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <small><?= $newsevent->description ?></small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= preg_replace('/(^[\"\']|[\"\']$)/', '', $newsevent->content) ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <!-- /page script -->