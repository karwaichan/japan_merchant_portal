                        <section class="panel">
                            <header class="panel-heading">
                                <h4>Update Category for Outlet <?= $outlet->id ?> - <?= $outlet->name ?></h4>
                            </header>
                            <div class="panel-body">
                                <form id="edit-repairorder" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Category</label>
                                                <div class="col-sm-10">
                                                    <select data-placeholder="Select Product Type" name="outlet_category[]" style="width:50%;" multiple class="chosen">
                                                        <option disabled>Select a Category</option>
                                                        <?php if(isset($merchant_categories) && !empty($merchant_categories)):?>
                                                            <?php foreach($merchant_categories as $key => $value): ?>
                                                                <?php if(in_array($value, $outlet_categories)):?>
                                                                <option selected value="<?= $value ?>"  ><?= $categories[$value] ?></option>
                                                                <?php else: ?>
                                                                <option value="<?= $value ?>"  ><?= $categories[$value] ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach;?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="<?= $this->lang->line('reset'); ?>" />
                                                    <input id="updateProfile" class="btn btn-primary btn-lg btn-parsley" type="submit" value="<?= $this->lang->line('submit'); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    </script>
    <!-- /page script -->