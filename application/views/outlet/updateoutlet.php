                        <section class="panel">
                            <header class="panel-heading">
                                <h4>Update Outlet <?= $outlet->id ?> - <?= $outlet->name ?></h4>
                            </header>
                            <div class="panel-body">
                                <form id="edit-repairorder" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Outlet Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="name" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->name ?>">
                                                    <input type="hidden" name="id" value="<?= $outlet->id ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Outlet Address</label>
                                                <div>
                                                    <input type="text" class="form-control" name="address" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->address ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Building Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="building_name" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->building_name ?>">
                                                </div>
                                            </div>
                                            
<!--                                            <div class="form-group">
                                                <label>Shopping Mall</label>
                                                <div>
                                                    <input type="text" class="form-control" name="mall_id" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->mall_id ?>">
                                                </div>
                                            </div>-->
                                            
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <div>
                                                    <input type="text" class="form-control" name="postal_code" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->postal_code ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?= $this->lang->line('outlet_number'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="phone_number" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->phone_number ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Contact Email</label>
                                                <div>
                                                    <input type="text" class="form-control" name="email" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->email ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Longitude</label>
                                                <div>
                                                    <input type="text" class="form-control" name="longitude" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->longitude ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Latitude</label>
                                                <div>
                                                    <input type="text" class="form-control" name="latitude" data-parsley-required="true" data-parsley-trigger="change" value="<?= $outlet->latitude ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Tax Office</label>
                                                <div>
                                                    <input type="text" class="form-control" name="tax_office" data-parsley-trigger="change" value="<?= $outlet->tax_office ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Tax Place</label>
                                                <div>
                                                    <input type="text" class="form-control" name="tax_place" data-parsley-trigger="change" value="<?= $outlet->tax_place ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Outlet Gstn</label>
                                                <div>
                                                    <input type="text" class="form-control" name="outlet_gstn" data-parsley-trigger="change" value="<?= $outlet->outlet_gstn ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Share Link</label>
                                                <div>
                                                    <input type="text" class="form-control" name="share_link" data-parsley-trigger="change" value="<?= $outlet->share_link ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="<?= $this->lang->line('reset'); ?>" />
                                                    <input id="updateProfile" class="btn btn-primary btn-lg btn-parsley" type="submit" value="<?= $this->lang->line('submit'); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    </script>
    <!-- /page script -->