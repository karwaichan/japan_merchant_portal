<section class="panel">
    <header class="panel-heading">
        <h4><?= $this->lang->line('merchant_information'); ?></h4>
    </header>
    <div class="panel-body">
        <form id="create-saleorder" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label><?= $this->lang->line('merchant'); ?>:</label>
                        <div>
                            <?= $user_profile->merchant->retailer_name ?> - <?= is_array($user_profile->outlet) ? $user_profile->outlet[0]->name : $user_profile->outlet->name ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><?= $this->lang->line('cashier'); ?>:</label>
                        <div>
                            <?= $this->session->userdata('username'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <img src="<?= $qr_image ?>" height="200" width="200">
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </form>
    </div>
</section>
</div>
<!-- /inner content wrapper -->

</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>
</section>
<!-- /main content -->
</section>
</div>

<!-- page script -->
<script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
<script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
<script>
</script>
<!-- /page script -->