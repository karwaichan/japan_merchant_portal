                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Product Type</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>producttype/createproducttype"><i class="fa fa-plus"></i> Create New Product Type</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stocktype/stocktypelist"><i class="ti-menu"></i> Stock Types</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Product Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($producttypes) && !empty($producttypes)): ?>
                                                <?php foreach($producttypes as $producttype): ?>
                                                    <tr class="delete-product-type">
                                                        <td><?= $producttype->id ?></td>
                                                        <td><?= $producttype->name ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>producttype/updateproducttype/<?= $producttype->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="javascript:;" class="delete" id="<?= $producttype->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-product-type .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete Product Type id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>producttype/deleteproducttype',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('Product Type id = '+id+' has been deleted successfully');
                        } else {
                            if (data.message == "stocktypes"){
                                bootbox.alert('Delete Error. There are some stock types with this Product Type', function() {window.location.reload();});
                            } else {
                                bootbox.alert('Product Type id = '+id+' can\'t be deleted', function() {window.location.reload();});
                            }
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->