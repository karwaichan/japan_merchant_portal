                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Category</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>category/createcategory"><i class="fa fa-plus"></i> Create New Category</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stocktype/stocktypelist"><i class="ti-menu"></i> Stock Types</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Product Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($categories) && !empty($categories)): ?>
                                                <?php foreach($categories as $category): ?>
                                                    <tr class="delete-category">
                                                        <td><?= $category->id ?></td>
                                                        <td><?= $category->name ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>category/updatecategory/<?= $category->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="javascript:;" class="delete" id="<?= $category->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-category .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete Category id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>category/deletecategory',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('Category id = '+id+' has been deleted successfully');
                        } else {
                            if (data.message == "stocktypes"){
                                bootbox.alert('Delete Error. There are some stock types with this Category', function() {window.location.reload();});
                            } else {
                                bootbox.alert('Category id = '+id+' can\'t be deleted', function() {window.location.reload();});
                            }
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->