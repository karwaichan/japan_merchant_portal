                        <section class="panel">
                            <header class="panel-heading">
                                <h4><?= $this->lang->line('create_employee'); ?></h4>
                            </header>
                            <div class="panel-body">
                                <form id="update-employee" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <div class="input-group mb15">
                                                    <span class="input-group-addon"><?= $user_profile->merchant->retailer_prefix.'_' ?></span>
                                                    <input type="text" class="form-control" name="username" data-parsley-required="true" data-parsley-trigger="change" placeholder="Username" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div>
                                                    <input type="text" class="form-control" name="email" value="">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Role</label>
                                                <div>
                                                    <select name="role_id" data-placeholder="Role" style="width:100%;" class="chosen">
                                                        <?php foreach($roles as $role_id => $role): ?>
                                                            <option value="<?= $role_id ?>"><?= $role ?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone</label>
                                                <div>
                                                    <input type="text" class="form-control" name="phone_number" data-parsley-type="digits" data-rangelength="[11,20]" data-parsley-trigger="change" placeholder="18005551234" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label><?= $this->lang->line('last_name'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="last_name" placeholder="<?= $this->lang->line('last_name'); ?>" value="">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?= $this->lang->line('first_name'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="first_name" placeholder="<?= $this->lang->line('first_name'); ?>" value="">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Password</label>
                                                <div>
                                                    <input type="password" class="form-control" name="password" data-parsley-required="true" data-rangelength="[6,20]" data-parsley-trigger="change" placeholder="Password">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Retype Password</label>
                                                <div>
                                                    <input type="password" class="form-control" name="retype_password" data-parsley-required="true" data-rangelength="[6,20]" data-parsley-trigger="change" placeholder="Retype Password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <!-- /page script -->