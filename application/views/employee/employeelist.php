                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center"><?= $this->lang->line('employeelist'); ?> - <?= $merchant->retailer_name ?></h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>employee/createemployee"><i class="fa fa-plus"></i> <?= $this->lang->line('create_employee'); ?></a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th><?= $this->lang->line('id'); ?></th>
                                                <th><?= $this->lang->line('username'); ?></th>
                                                <th><?= $this->lang->line('full_name'); ?></th>
                                                <th><?= $this->lang->line('role'); ?></th>
                                                <th><?= $this->lang->line('outlet'); ?></th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($cashiers) && !empty($cashiers)): ?>
                                                <?php foreach($cashiers as $cashier): ?>
                                                    <tr class="employee-list">
                                                        <td><?= $cashier->user_id ?></td>
                                                        <td><?= $cashier->username ?></td>
                                                        <td><?= $cashier->name ?></td>
                                                        <td><?= $roles[$cashier->role_id] ?></td>
                                                        <td><?= $outlet->name ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>employee/updateemployee/<?= $cashier->user_id ?>" class="edit"><button type="button" class="btn btn-primary btn-outline">Edit</button></a>
                                                            <a href="javascript:;" class="delete" id="<?= $cashier->user_id ?>"><button type="button" class="btn btn-danger  btn-outline">Delete</button></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".employee-list .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete user id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>employee/deleteemployee',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('User id = '+id+' has been deleted successfully');
                        } else {
                            bootbox.alert('User id = '+id+' can\'t deleted');
                            location.reload();
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->