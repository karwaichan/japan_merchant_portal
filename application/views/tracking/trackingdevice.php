
                        <div class="timeline">
                            <div class="timeline-heading">
                                Activity Timeline<br>
                                Device: <?= $device->id; ?> - User: <?= $device->user_id; ?><br>
                                UUID: <?= $device->mobile_id; ?><br>
                            </div>

                            <?php if(isset($trips) && !empty($trips)): ?>
                                <?php foreach($trips as $trip): ?>
                                    <div class="timeline-panel">
                                        <section class="panel">
                                            <div class="timeline-date"><?= gmdate("m/d/Y H:i:s", $trip->start); ?> - <?= gmdate("m/d/Y H:i:s", $trip->end); ?></div>
                                            <div class="panel-body">
                                                <b>Number of Detections: <?= sizeof($trip->detections); ?></b>
                                                <?php foreach($trip->detections as $detection): ?>
                                                    <hr/>
                                                    <p>ID: <?= $detection->id; ?> - Beacon: <?= $detection->beacon_id; ?></p>
                                                    <p>Time: <?= gmdate("m/d/Y H:i:s", $detection->start); ?> - <?= gmdate("m/d/Y H:i:s", $detection->end); ?></p>
                                                <?php endforeach;?>
                                            </div>
                                        </section>
                                    </div>
                                <?php endforeach;?>
                            <?php endif; ?>
                            
                            
                        </div>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->

        </section>

    </div>

    <!-- core scripts -->
    <script src="plugins/jquery-1.11.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="plugins/jquery.slimscroll.min.js"></script>
    <script src="plugins/jquery.easing.min.js"></script>
    <script src="plugins/appear/jquery.appear.js"></script>
    <script src="plugins/jquery.placeholder.js"></script>
    <!-- /core scripts -->

    <!-- page level scripts -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- /page level scripts -->

    <!-- template scripts -->
    <script src="js/offscreen.js"></script>
    <script src="js/main.js"></script>
    <!-- /template scripts -->

    <!-- page script -->
    <!-- /page script -->
