                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center"><?= $this->lang->line('tracking_list'); ?></h4>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th><?= $this->lang->line('id'); ?></th>
                                                <th><?= $this->lang->line('user_id'); ?></th>
                                                <th><?= $this->lang->line('mobile_id'); ?></th>
                                                <th><?= $this->lang->line('created_date'); ?></th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($trackingdevices) && !empty($trackingdevices)): ?>
                                                <?php foreach($trackingdevices as $trackingdevice): ?>
                                                    <tr class="employee-list">
                                                        <td><?= $trackingdevice->id ?></td>
                                                        <td><?= $trackingdevice->user_id ?></td>
                                                        <td><?= $trackingdevice->mobile_id ?></td>
                                                        <td><?= gmdate("m/d/Y", $trackingdevice->created_date) ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>tracking/trackingdevice/<?= $trackingdevice->mobile_id ?>/<?= $trackingdevice->user_id ?>" class="edit"><button type="button" class="btn btn-primary btn-outline">View</button></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <!-- /page script -->