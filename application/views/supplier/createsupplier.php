                        <section class="panel">
                            <header class="panel-heading">
                                <h4>Create New Supplier</h4>
                            </header>
                            <div class="panel-body">
                                <form id="create-supplier" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="company_name" data-parsley-required="true" data-parsley-trigger="change" placeholder="Company Name">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="first_name" data-parsley-required="false" data-parsley-trigger="change" placeholder="First Name">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="last_name" data-parsley-required="false" data-parsley-trigger="change" placeholder="Last Name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div>
                                                    <input type="text" class="form-control" name="email"  data-parsley-type="email" data-parsley-required="true" data-parsley-trigger="change" placeholder="Email">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address 1</label>
                                                <div>
                                                    <input type="text" class="form-control" name="address_1" data-parsley-required="false" data-parsley-trigger="change" placeholder="Address 1">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address 2</label>
                                                <div>
                                                    <input type="text" class="form-control" name="address_2" data-parsley-required="false" data-parsley-trigger="change" placeholder="Address 2">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>City</label>
                                                <div>
                                                    <input type="text" class="form-control" name="city" data-parsley-required="false" data-parsley-trigger="change" placeholder="City">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>State</label>
                                                <div>
                                                    <input type="text" class="form-control" name="state" data-parsley-required="false" data-parsley-trigger="change" placeholder="State">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Zip</label>
                                                <div>
                                                    <input type="text" class="form-control" name="zip" data-parsley-required="false" data-parsley-trigger="change" placeholder="Zip">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Country</label>
                                                <div>
                                                    <select name="country" data-placeholder="Country" style="width:100%;" class="chosen">
                                                        <?php foreach($countries as $key => $value): ?>
                                                            <?php if ($value == 'Singapore'): ?>
                                                                <option value="<?= $value ?>" selected="selected"><?= $value ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $value ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Currency</label>
                                                <div>
                                                    <select id="currency" name="currency" data-placeholder="Currency" style="width:100%;" class="chosen">
                                                        <?php foreach($currencies as $key => $value): ?>
                                                            <?php if ($value == 'SGD'): ?>
                                                                <option value="<?= $value ?>" selected="selected"><?= $value ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $value ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone</label>
                                                <div>
                                                    <input type="text" class="form-control" name="phone" data-parsley-type="digits" data-rangelength="[11,20]" data-parsley-required="false" data-parsley-trigger="change" placeholder="Country">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Account Number</label>
                                                <div>
                                                    <input type="text" class="form-control" name="account_number" data-parsley-type="digits" data-parsley-required="false" data-parsley-trigger="change" placeholder="Account Number">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Bank Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="bank_name" data-parsley-required="false" data-parsley-trigger="change" placeholder="Bank Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Website</label>
                                                <div>
                                                    <input type="text" class="form-control" name="website_url" data-parsley-required="false" data-parsley-type="url" data-parsley-trigger="change" placeholder="Website">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Logo Upload</label>
                                                <div>
                                                    <input type="file" name="supplier_photo"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div>
                                                    <div class="thumb text-center">
                                                        <img class="img-thumbnail stocktype-thump" src="<?= base_url(); ?>asset/img/faceless.jpg" alt="Responsive image">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#create-supplier #signupForm").click(function(e) {
        e.preventDefault();
        var company_name = $( "#create-supplier input[name='company_name']" ).val();
            
        if(!signupClicked){
            signupClicked = true;
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>supplier/checksupplier',
                    data: 'company_name='+company_name,
                    dataType: 'json',
                    success: function(data){
                        signupClicked = false;
                        if (data.status == "true"){
                            bootbox.alert('Company name is existing, please select another!');
//                            alert(JSON.stringify(data));
                        } else {
                            $("#create-supplier").submit();
                        }
                            
                    },
                    error: function(data){  
                        signupClicked = false;
                    }
                });
        }

    });
        
        $("#create-supplier #currency").on('change', function(e) {
            var currency = $( "select[name='currency']").val();
            $.ajax({
                type: 'POST',
                url: '<?= base_url(); ?>currency/validateCurrencyPair',
                data: 'currency='+currency,
                dataType: 'json',
                success: function(data){
                    if (data.status == "false"){
                        if (data.message == "base_currency"){
                            document.location.href = '/currency/currencybase';
                        }
                    } else {
                        bootbox.alert(data.message);
                    }
                },
                error: function(data){  }
            });
        });
    </script>
    <!-- /page script -->