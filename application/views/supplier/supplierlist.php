                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Supplier</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>supplier/createsupplier"><i class="fa fa-plus"></i> Create New Supplier</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Company Names</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Currency</th>
                                                <th>Website</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($suppliers) && !empty($suppliers)): ?>
                                                <?php foreach($suppliers as $supplier): ?>
                                                    <tr class="delete-supplier">
                                                        <td><?= $supplier->id ?></td>
                                                        <td><?= $supplier->company_name ?></td>
                                                        <td><?= $supplier->email ?></td>
                                                        <td><?= $supplier->country ?></td>
                                                        <td><?= $supplier->currency ?></td>
                                                        <td><?= $supplier->website_url ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>supplier/updatesupplier/<?= $supplier->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="javascript:;" class="delete" id="<?= $supplier->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-supplier .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete supplier id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>supplier/deletesupplier',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('Supplier id = '+id+' has been deleted successfully');
                        } else {
                            if (data.message == "stocks"){
                                bootbox.alert('Delete Error. There are some stocks in this Supplier', function() {window.location.reload();});
                            } else {
                                bootbox.alert('Supplier id = '+id+' can\'t be deleted', function() {window.location.reload();});
                            }
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->