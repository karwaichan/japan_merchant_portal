<!--                <ol class="breadcrumb">
                    <li>
                        <a href="javascript:;"><i class="ti-home mr5"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="ti-window mr5"></i>Tables</a>
                    </li>
                    <li class="active">Basic Table Elements</li>
                </ol>-->
<div>
    <section class="panel">
        <header class="panel-heading no-b text-center">
            <h4><?= $this->lang->line('dashboard'); ?></h4>
        </header>
        <div class="panel-body text-center"><?= SITE_NAME ?></div>


        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#quote-carousel" data-slide-to="1"></li>
                        <li data-target="#quote-carousel" data-slide-to="2"></li>
                        <li data-target="#quote-carousel" data-slide-to="3"></li>
                        <li data-target="#quote-carousel" data-slide-to="4"></li>
                    </ol>

                    <div class="carousel-inner">
						<?php if (isset($newsevents) && !empty($newsevents)): ?>
							<?php foreach ($newsevents as $key => $newsevent): ?>
                                <div class="item <?= (!$key) ? 'active' : '' ?> ">
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <a href="<?= base_url(); ?>newsevent/newseventdetail/<?= $newsevent->id ?>">
                                                <p class="text-primary"><?= $newsevent->title ?></p></a>
                                        </div>
                                    </div>
                                </div>
							<?php endforeach; ?>
						<?php endif; ?>
                    </div>

                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control">
                        <i class="ti-arrow-circle-left"></i>
                    </a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control">
                        <i class="ti-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="col-md-4 col-md-offset-4" style="border:1px solid black; background:whitesmoke;padding:0 0">
        <div style="background:black; color:white;font-size:20px;font-weight:bold"><?= $this->lang->line('sales_account_information'); ?></div>
        <div style="font-size:15px">
            <div><?= $this->lang->line('today_sales'); ?> : ¥ <?= number_format($sales['today_sales_amount'], 0, '', ',') ?></div>
            <div><?= $this->lang->line('month_to_date_sale'); ?> : ¥ <?= number_format($sales['month_to_date_sales_amount'], 0, '', ',') ?></div>
            <div><?= $this->lang->line('rolling_6_month'); ?> : ¥ <?= number_format($sales['rolling_six_month_sales_amount'], 0, '', ',') ?></div>
        </div>
    </div>
</div>
<!-- /inner content wrapper -->

</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>
</section>
<!-- /main content -->
</section>

</div>