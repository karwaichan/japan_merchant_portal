                        <section class="panel">
                            <header class="panel-heading">
                                <h4>Import Stocks</h4>
                            </header>
                            <div class="panel-body">
                                <form id="import-stock-form" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Stock Upload</label>
                                                <div>
                                                    <input type="file" name="stock_import"/>
                                                    <input name="uploadStock" class="btn btn-primary" type="submit" value="Upload File" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <?php if(isset($stock_import_link) && !empty($stock_import_link)): ?>
                                                <div class="form-group">
                                                    <label>Status: Uploaded</label>
                                                    <label>Link: </label>
                                                    <!--<input type="text" class="form-control" name="stock_import_file" data-parsley-required="true" data-parsley-trigger="change" placeholder="Company Name" value="<?= base_url().'uploads/stocks/'.$parent_id.'/'.$stock_import_link ?>" readonly>-->
                                                    <input type="text" class="form-control" name="stock_import_file" data-parsley-required="true" data-parsley-trigger="change" placeholder="Company Name" value="<?= $stock_import_link ?>" readonly>
                                                </div>
                                                <div class="form-group text-center">
                                                    <label><img src="<?= base_url(); ?>/asset/img/ajax-loader.gif" id="loading-indicator" style="display:none" /></label>
                                                    <div>
                                                        <input id="import-stock-submit" name="importStockForm" class="btn btn-primary" type="submit" value="Import" />
                                                        <a id="back-stocktypelist" class="btn btn-primary btn-heading" href="/stocktype/stocktypelist" style="display:none"><i class="ti-menu"></i> Back To Stock Type</a>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
        var signupClicked = false;
        $("#import-stock-form #import-stock-submit").click(function(e) {
            e.preventDefault();
            var stock_import_file = $( "#import-stock-form input[name='stock_import_file']" ).val();
            if(!signupClicked){
                signupClicked = true;
                $('#loading-indicator').show();
                $.ajax({
                        type: 'POST',
                        url: '<?= base_url(); ?>stock/stockimportfile',
                        data: 'stock_import_file='+stock_import_file,
                        dataType: 'json',
                        success: function(data){
                            signupClicked = false;
                            if (data.status == true){
                                bootbox.alert('Stock import: Done!');
                                $('#import-stock-submit').hide();
                                $('#back-stocktypelist').show();
    //                            alert(JSON.stringify(data));
                            } else {
                                bootbox.alert('Stock failed to import. Please check the importing file again!');
                            }
                            $('#loading-indicator').hide();

                        },
                        error: function(data){
                            $('#loading-indicator').hide();
                            signupClicked = false;
                        }
                    });
            }

        });
    </script>
    <!-- /page script -->