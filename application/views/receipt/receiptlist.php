
<section class="panel panel-default">
    <header class="panel-heading">
        <h4 class="text-center"><?= $this->lang->line('list_of_receipt'); ?></h4>
    </header>

    <div class="panel-body">

        <div class="col-md-12">
        <div id="dateRange" class="mb10 col-md-offset-10 col-md-2"
             style="background: #C41532; color: #ffffff; cursor: pointer; padding: 5px 10px; display: inline-block">
            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
            <span></span> <b class="caret"></b>
        </div>
        </div>
        <div class="table-responsive no-border">
            <table id="receiptlist" class="table table-bordered table-striped mg-t datatable editable-datatable">
                <thead>
                <tr>
                    <th><?= $this->lang->line('date_of_purchase') ?></th>
                    <th><?= $this->lang->line('transaction_id') ?></th>
                    <th><?= $this->lang->line('status') ?></th>
                    <th><?= $this->lang->line('customer_name') ?></th>
                    <th><?= $this->lang->line('document_type') ?></th>
                    <th><?= $this->lang->line('number') ?></th>
                    <th><?= $this->lang->line('nationality') ?></th>
                    <th><?= $this->lang->line('general_category') ?></th>
                    <th><?= $this->lang->line('consumables_category') ?></th>
                    <th><?= $this->lang->line('tax') ?></th>
                    <th><?= $this->lang->line('action') ?></th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($tickets as $ticket) : ?>
                    <tr class="list-stock">
                        <td style="text-align: center"><?= gmdate('Y-m-d', $ticket->date_of_purchase) ?></td>
                        <td style="text-align: center"><?= $ticket->doc_id ?></td>
                        <td><?= $this->lang->line($ticket->status)?></td>
                        <td><?= empty($ticket->member->first_name) ? $this->lang->line('deleted_account') : $ticket->member->first_name . ' ' . (empty($ticket->member->last_name) ? '' : $ticket->member->last_name) ?></td>
                        <td><?= $this->lang->line('passport') ?></td>
                        <td><?= empty($ticket->member->passport_number) ? $this->lang->line('deleted_account') : $ticket->member->passport_number ?></td>
                        <td style="text-align: center"><?= empty($ticket->member->nationality->nicename) ? $this->lang->line('deleted_account') : $ticket->member->nationality->nicename ?></td>
                        <td style="text-align: right"><?= !empty($ticket->general_total) ? number_format($ticket->general_total, 0) : 0 ?></td>
                        <td style="text-align: right"><?= !empty($ticket->consumable_total) ? number_format($ticket->consumable_total, 0) : 0 ?></td>
                        <td style="text-align: right"><?= number_format($ticket->gst_amount, 0) ?></td>
                        <td>
                            <a target="_blank"
                               href="<?= base_url(); ?>receipt/jpcovenantofpurchase/<?= $ticket->doc_id ?>"
                               class="edit"><i class="fa fa-edit"> <?= $this->lang->line('cop'); ?> </i></a>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>


</div>
<!-- /inner content wrapper -->
</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>
</section>
<!-- /main content -->
</section>

</div>


<!-- page script -->
<script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>asset/daterangepicker/moment.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?= base_url(); ?>asset/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/daterangepicker/daterangepicker.css"/>
<script>
    $(document).ready(function () {

        $('#dateRange span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        receipt(moment().startOf('month'), moment());

        $('#dateRange').daterangepicker(
            {
                startDate: moment().startOf('month'),
                endDate: moment(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, receipt
        );

        function receipt(start,end) {
            $('#dateRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var min = $('#dateRange').data('daterangepicker').startDate;
                    var max = $('#dateRange').data('daterangepicker').endDate;
                    var startDate = new Date(data[0]);

                    if (min == null && max == null) { return true; }
                    if (min == null && startDate <= max) { return true;}
                    if(max == null && startDate >= min) {return true;}
                    if (startDate <= max && startDate >= min) { return true; }
                    return false;
                }
            );
            var table = $('#receiptlist').DataTable({
                destroy: true,
                "order": [[0, "desc"], [1, "desc"]],
                "pagingType": "full_numbers",
                "language": {
                    "url": "<?= base_url(); ?>asset/plugins/datatables/language/<?= unserialize(LANGUAGE_CODE)[$this->session->userdata('site_lang')] ?>.json"
                }
            });

            $('#min, #max').change(function () {
                table.draw();
            });
        }

    });


</script>
<!-- /page script -->