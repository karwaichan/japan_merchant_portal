<?php
$ticket_info = $receipt['ticket_info'][0];
?>
<div class="row">
    <div class="col-md-1">
        <div class="form-group">
        </div>
    </div>
    <div class="col-md-10">
        <div class="form-group">

            <table class="borderless">
                <tbody>
                    <tr>
                        <td width="10%"><img src="<?= base_url() ?>asset/img/jplogo.png"</td>
                        <td width="60%">
                            <h5>輸出免税物品購入記録票</h5>
                            <h5>Record of Purchase of Consumption Tax-Exempt for Export</h5>
                        </td>
                        <td width="30%">
                            <div style="text-align: center"><img src="<?= $qr_image ?>" /></div>
                            <br/>
                            <div style="text-align: center">Ref.No.: <?= $receipt['DOC_ID'] ?></div>
                            <!--<span style="border-bottom: 1px solid black; padding-left: 60%;"></span>-->
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="print-bordered" style="text-align: center;">
                <tbody>
                    <tr>
                        <td width="25%">
                            所 轄 税 務 署<br>
                            Tax Office Concerned
                        </td>
                        <td width="25%">
                            納　税　地<br>
                            Place for Tax Payment
                        </td>
                        <td width="25%">
                            販 売 場 所 在 地<br>
                            Selling Place
                        </td>
                        <td width="25%">
                            販 売 者 氏 名<br>
                            Seller's Name
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>渋谷税務署<br/><br/>
                        </td>
                        <td>
                            東京都渋谷区渋谷2-1-11
                        </td>
                        <td>
                            <?= $ticket_info->outlet->address ?>
                        </td>
                        <td style="text-align: center">
                            <?= $ticket_info->merchant->name ?> - <?= $ticket_info->outlet->name ?>
                        </td>
                    </tr>
                </tbody>
            </table>


            <table class="print-bordered">
                <tbody>
                    <tr>
                        <td width="15%">
                            購 入 年 月 日<br/>
                            Date of Purchase
                        </td>
                        <td width="35%" style="padding: 0px;">
                            <table class="borderless" width="100%" style="text-align: right">
                                <tbody>
                                    <tr>
                                        <td width="33%">
                                            <?= date('Y', $receipt['date_of_purchase']) ?> 年<br/>
                                            Year
                                        </td>
                                        <td width="33%">
                                            <?= date('m', $receipt['date_of_purchase']) ?> 月<br/>
                                            Month
                                        </td>
                                        <td width="34%">
                                            <?= date('d', $receipt['date_of_purchase']) ?> 日<br/>
                                            Day
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="20%">
                            在　留　資　格<br/>
                            Status of Residence
                        </td>
                        <td width="30%">
                            <?= (isset($resident_status_list[$receipt['resident_status']])) ? $resident_status_list[$receipt['resident_status']] : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="2" style="padding: 0px; vertical-align: top;">
                            <table class="print-bordered" style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td width="60%" colspan="2">
                                            旅券等の種類 Passport etc.
                                        </td>
                                        <td width="40%">
                                            番号 №
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" colspan="2">
                                            旅　券 PASSPORT
                                        </td>
                                        <td width="40%">
                                            <?= $ticket_info->member->passport_number ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            その他<br/>Other
                                        </td>
                                        <td width="40%">

                                        </td>
                                        <td width="40%">

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            国 　籍<br/>
                            Nationality
                        </td>
                        <td>
                            <?= $country[$ticket_info->member->nationality_iso3]['uppercase_name'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px">
                            購入者氏名（活字体）<br/>
                            Name in Full (in block Letters)
                        </td>
                        <td>
                            <?= strtoupper($ticket_info->member->first_name . ' ' . $ticket_info->member->last_name) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            上 陸 年 月 日<br/>
                            Date of Landing
                        </td>
                        <td style="padding: 0px;">
                            <table class="borderless" width="100%" style="text-align: right">
                                <tbody>
                                    <tr>
                                        <td width="33%">
                                            <?= $date_of_landing->format('Y') ?> 年<br/>
                                            Year
                                        </td>
                                        <td width="33%">
                                            <?= $date_of_landing->format('m') ?> 月<br/>
                                            Month
                                        </td>
                                        <td width="34%">
                                            <?= $date_of_landing->format('d') ?> 日<br/>
                                            Day
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="15%">
                            生 年 月 日<br/>
                            Date of Birth of Purchaser
                        </td>
                        <td style="padding: 0px;">
                            <?php
                            $date = DateTime::createFromFormat('Y-m-d H:s:i', $ticket_info->member->dob);
                            ?>
                            <table class="borderless" width="100%" style="text-align: right">
                                <tbody>
                                    <tr>
                                        <td width="33%">
                                            <?= $date->format('Y') ?> 年<br/>
                                            Year
                                        </td>
                                        <td width="33%">
                                            <?= $date->format('m') ?> 月<br/>
                                            Month
                                        </td>
                                        <td width="34%">
                                            <?= $date->format('d') ?> 日<br/>
                                            Day
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="print-bordered">
                    <?php
                    $receiptlist = $ticket_info->packages[0]->receipts;
                    $consumablelist = $commoditylist = $item_count = array();
                    foreach ($receiptlist as $receipt_order) {
                        if ($receipt_order->category->parent_id == 11) {
                            $consumablelist[] = $receipt_order;
                        }

                        if ($receipt_order->category->parent_id == 12) {
                            $commoditylist[] = $receipt_order;
                        }
                    }
                    $item_count = (sizeof($consumablelist) > sizeof($commoditylist)) ? sizeof($consumablelist) : sizeof($commoditylist);
                    $sum_commodity = $sum_consumable = 0;
                    if (isset($item_count) && $item_count):
                        ?>
                        <?php for ($i = 0; $i < $item_count; $i++): ?>
                            <?php
                                if (isset($consumablelist[$i]) && !empty($consumablelist[$i])) {
									$sum_consumable += $consumablelist[$i]->amount;
                                }
								if (isset($commoditylist[$i]) && !empty($commoditylist[$i])){
									$sum_commodity += $commoditylist[$i]->amount;
                                }
                            ?>
                        <?php endfor; ?>
                    <?php endif; ?>
            
            <table class="print-bordered" style="border:3px solid gray">
                <tbody>
                    <tr>
                        <td>
                            一般物品 / Commodities
                        </td>
                        <td>
                            <?= ($sum_commodity) ? $sum_commodity : '' ?>
                        </td>
                        <td rowspan="2">
                            合　計　価　額<br/>
                            Total Amount
                        </td>
                        <td rowspan="2">
                            <?= $receipt['amount'] ?>
                        </td>
                        <td colspan="4" rowspan="2">
                            本邦から出国する際又は居住者となる際に、その出港地を所轄する税関長又はその住所若しくは居所の所在地を所轄する税務署長に購入記録票を提出しなければなりません。<br/>
                            When departing Japan, or if becoming a resident of Japan, you are required to submit your “Record of Purchase Card” to either the Director of Customs that has jurisdiction over your
                            departure location or the head of the tax office that has jurisdiction over your place of residence or address.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            消耗品 / Consumables
                        </td>
                        <td>
                            <?= ($sum_consumable) ? $sum_consumable : '' ?>
                        </td>
            <!--            <td colspan="2">
                            合　計　価　額<br/>
                            Total Amount
                        </td>
                        <td colspan="2">
                        <?= $receipt->amount ?>
                        </td>-->
                    </tr>
                </tbody>
            </table>







        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
        </div>
    </div>
</div>























</div>

<!-- page script -->
<script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
<script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
<!-- /page script -->