<style>

</style>

<div style="font-size: 10px">
    <div class="col-md-10 col-md-offset-1">
        <div class="form-group">

            <table class="borderless">
                <tbody>
                    <tr>
                        <td width="60%">
                            <h5>最終的に輸出となる物品の消費税免税購入についての購入者誓約書</h5>
                            <h5>Covenant of Purchaser of Consumption Tax-Exempt of Ultimate Export</h5>
                        </td>
                        <td width="30%">
                            <div style="text-align: center"><img src="<?= $qr_image ?>" /></div>
                            <br/>
                            <div style="text-align: center">Ref.No.: <?= $docId ?></div>
                            <!--<span style="border-bottom: 1px solid black; padding-left: 60%;"></span>-->
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="print-bordered">
                <tbody>
                    <tr>
                        <td width="70%" rowspan="2" style="padding: 0px;">
                            <table class="borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <ul>
                                                <li>当該一般物品を、日本から最終的には輸出されるものとして購入し、日本で処分しないことを誓約します。</li>
                                                <li>当該消耗品を、購入した日から30日以内に輸出されるものとして購入し、日本で処分しないことを誓約します。</li>
                                                <li>- I certify that the goods listed as "commodities except consumables"  on this card were purchased by me </li>
                                                for ultimate export from Japan and will not be disposed of within Japan.
                                                <li>- I certify that the goods listed as "consumable commodities" on this card were purchased by me for export from </li>
                                                Japan within 30days from the purchase the purchase date and will not be disposed of within Japan.
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            署 名<br/>
                                            Signature
                                        </td>
                                        <td style="background-color: #cccccc; border-left:1px; text-align: center">
                                            <img src="<?=  $ticket->etrs->signature_url?>" style="height: 50px" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="text-align: center;">
                            販 売 者 氏 名<br/>
                            Seller's Name
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <br/><br/><?= $ticket->outlet->name ?>
                            <br/><br/><br/>
                        </td>
                    </tr>
                </tbody>
            </table>


            <table class="print-bordered">
                <tbody>
                    <tr>
                        <td width="15%">
                            購 入 年 月 日<br/>
                            Date of Purchase
                        </td>
                        <td width="35%" style="padding: 0px;">
                            <table class="borderless" width="100%" style="text-align: right">
                                <tbody>
                                    <tr>
                                        <td width="33%">
                                            <?= date('Y', $ticket->etrs->date_of_purchase) ?> 年<br/>
                                            Year
                                        </td>
                                        <td width="33%">
                                            <?= date('m', $ticket->etrs->date_of_purchase) ?> 月<br/>
                                            Month
                                        </td>
                                        <td width="34%">
                                            <?= date('d', $ticket->etrs->date_of_purchase) ?> 日<br/>
                                            Day
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="20%">
                            在　留　資　格<br/>
                            Status of Residence
                        </td>
                        <td width="30%">
                            <?= $resident_status[$ticket->etrs->resident_status] ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="2" style="padding: 0px; vertical-align: top;">
                            <table class="print-bordered" style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td width="60%" colspan="2">
                                            旅券等の種類 Passport etc.
                                        </td>
                                        <td width="40%">
                                            番号 №
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" colspan="2">
                                            旅　券 PASSPORT
                                        </td>
                                        <td width="40%">
                                            <?= !empty($ticket->etrs->member->passport_number) ? $ticket->etrs->member->passport_number : '' ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            その他<br/>Other
                                        </td>
                                        <td width="40%">

                                        </td>
                                        <td width="40%">

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            国 　籍<br/>
                            Nationality
                        </td>
                        <td>
                            <?= !empty($ticket->etrs->member->nationality->nicename) ? strtoupper($ticket->etrs->member->nationality->nicename) : ''?>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px">
                            購入者氏名<br/>
                            Purchaser Name
                        </td>
                        <td>
                            <?= strtoupper(!empty($ticket->etrs->member->first_name) ? $ticket->etrs->member->first_name : '' )?> <?= strtoupper(!empty($ticket->etrs->member->last_name) ? $ticket->etrs->member->last_name : '')?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            上 陸 年 月 日<br/>
                            Date of Landing
                        </td>
                        <td style="padding: 0px;">
                            <table class="borderless" width="100%" style="text-align: right">
                                <tbody>
                                    <tr>
                                        <td width="33%">
                                            <?= $date_of_landing->format('Y') ?> 年<br/>
                                            Year
                                        </td>
                                        <td width="33%">
                                            <?= $date_of_landing->format('m') ?> 月<br/>
                                            Month
                                        </td>
                                        <td width="34%">
                                            <?= $date_of_landing->format('d') ?> 日<br/>
                                            Day
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="15%">
                            生 年 月 日<br/>
                            Date of Birth of Purchaser
                        </td>
                        <td style="padding: 0px;">
                            <?php
                            $date = DateTime::createFromFormat('Y-m-d H:s:i', $ticket->etrs->member->dob);
                            ?>
                            <table class="borderless" width="100%" style="text-align: right">
                                <tbody>
                                    <tr>
                                        <td width="33%">
                                            <?= $date->format('Y') ?> 年<br/>
                                            Year
                                        </td>
                                        <td width="33%">
                                            <?= $date->format('m') ?> 月<br/>
                                            Month
                                        </td>
                                        <td width="34%">
                                            <?= $date->format('d') ?> 日<br/>
                                            Day
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
<!--            start of non and consumable-->
            <div class="col-md-12" style="padding: 0;margin-bottom: 2%;">
            <div class="col-md-6" style="padding: 0">
                一般物品   Non Consumable
                <table class="print-bordered" style="border:3px solid gray">
                    <tbody>
                    <tr style="text-align: center">
                        <td>
                            品名/ Name of Commodity
                        </td>
                        <td>
                            数量/ Quantity
                        </td>
                        <td>
                            単価/ Unit Price
                        </td>
                        <td>
                            販売価格/ Price
                        </td>
                    </tr>
                    <?php if(!empty($commodity)):?>
                    <tr>
                        <td></td>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"><?= number_format($commodity->amount, 0, '.', ',')?></td>
                    </tr>
                        <tr height="27">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2" style="text-align: center">合計 Total Amount</td>
                            <td style="text-align: right"><?= number_format($commodity->amount, 0, '.', ',')?></td>
                        </tr>
                    <?php else:?>
                        <tr>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                        </tr>
                        <tr>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2" style="text-align: center">合計 Total Amount</td>
                            <td style="text-align: right"></td>
                        </tr>
                    <?php endif?>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6" style="padding: 0">
                消耗品   Consumables
                <table class="print-bordered" style="border:3px solid gray">
                    <tbody>
                    <tr style="text-align: center">
                        <td>品名/ Name of commodity</td>
                        <td>数量/ Quantity</td>
                        <td>単価/ Unit Price</td>
                        <td>販売価格/ Price</td>
                    </tr>
                    <?php if(!empty($consumable)) :?>
                    <tr>
                        <td></td>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"></td>
                        <td style="text-align: right"><?= number_format($consumable->amount, 0, '.', ',')?></td>
                    </tr>
                        <tr height="27">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2" style="text-align: center">合計 Total Amount</td>
                            <td style="text-align: right"><?= number_format($consumable->amount, 0, '.', ',')?></td>
                        </tr>
                    <?php else:?>
                        <tr>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                        </tr>
                        <tr>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                            <td>&nbsp</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2" style="text-align: center">合計 Total Amount</td>
                            <td style="text-align: right"></td>
                        </tr>
                    <?php endif?>
                    </tbody>
                </table>
            </div>
            </div>
            <div>
                <div style="margin-top: 2%">レシートNo.等 : <?=$ticket->etrs->receipt_no?></div>
                <div> 購入記録票発行日時 : <?= $ticket->etrs->created_at?></div>
            </div>
<!--            end of non and consumables-->
        </div>
    </div>
</div>