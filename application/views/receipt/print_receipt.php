<section style="display: block; margin-top: 20px; margin-bottom: 10px" class="col-md-12">
    <div class="col-md-6 col-md-offset-3" style="background-color: white;">
        <div class="panel" id="printableArea" style="word-wrap: break-word">
            <header class="panel-heading">
                <p class="text-center"><img style="width: 50%" src="<?= base_url(); ?>asset/img/tourego_logo_on_etrs_ticket.png"></p>
                <p class="text-center"><img style="width: 20%" src="<?= base_url(); ?>asset/img/eTRS_logo.png"></p>
                <h3 class="text-center specified_red_font_color"><?= $this->lang->line('print_eTRS_title'); ?></h3>
                <h4 class="text-center  specified_red_font_color mb10 m120"><?= $this->lang->line('ticket_detail_note'); ?></h4>
                <?php if ($etrs_status == ISSUED || $etrs_status == PROCESSED): ?>
                    <div id="bcTarget" style="margin: auto"></div>   
                <?php else: ?>
                    <h1 class="text-center mt10"><?= $etrs_status ? $etrs_status : 'ABNORMAL' ?></h1>
                <?php endif; ?>

                <p style="display: none" id="doc_id"><?= $barcode_number ?></p>
                <h4 class="text-center mb10"><?= $this->lang->line('doc_id'); ?>: <?= $Doc_ID ?></h4>
                <p style="text-align: left; margin-top: 10px"><?= $this->lang->line('date_of_issue'); ?>
                    <span style="float: right"><?= $date_of_issue ?></span>
                </p>

                <p style="text-align: left"><?= $this->lang->line('time_of_issue'); ?>
                    <span style="float: right"><?= $time_of_issue ?></span>
                </p>
            </header>
            <div class="panel-body pt5">

                <div>
                    <p class="black_background_white_font mt0"><?= $this->lang->line('central_refund_agency'); ?></p>
                    <div class="text-center">
                        <p><?= TOUREGO ?></p>
                        <p><?= TOUREGO_ADDRESS ?></p>
                        <p><?= TOUREGO_GST_REG_NO ?></p>
                        <p><?= $this->lang->line('tourego_hotline'); ?></p>
                    </div>
                </div>

                <div>
                    <p class="black_background_white_font">
                        <?= $this->lang->line('tourist_details'); ?>
                    </p>

                    <p style="text-align: left"><?= $this->lang->line('passport_no'); ?>
                        <span style="float: right"><?= $passport_no ?></span>
                    </p>
                    <p style="text-align: left"><?= $this->lang->line('eTRS_nationality'); ?>
                        <span style="float: right"><?= $country_numeric_code ?> - <?= $nationality ?></span>
                    </p>
                </div>

                <div>
                    <p class="black_background_white_font">
                        <?= $this->lang->line('retailer_details'); ?>
                    </p>

                    <p style="text-align: left; display: inline-block"><?= $this->lang->line('eTRS_retailer_name'); ?>
                    </p>
                    <p style="float: right; display: inline-block; width: 60%; text-align: right"><?= $merchant_name ?></p>

                    <p style="text-align: left; clear: both"><?= $this->lang->line('eTRS_gst_reg_no'); ?>
                        <span style="float: right"><?= $eTRS_gst_reg_no ?></span>
                    </p>
                </div>

                <div>
                    <p class="black_background_white_font"><?= $this->lang->line('item_summary'); ?></p>
                    <p style="text-align: left"><?= $this->lang->line('receipt_no'); ?>
                        <span style="float: right"><?= $this->lang->line('eTRS_amount'); ?></span>
                    <p>
                        <?php foreach ($packages as $package): ?>
                            <?php foreach ($package->receipts as $receipt): ?>
                                <?php if ($receipt->status != array_search(RECEIPT_STATUS_VOIDED, $receipt_status_array)) : ?>
                                <p style="text-align: left"><?= $receipt->receiptNumber ?>
                                    <span style="float: right">SGD <?= number_format($receipt->amount, 2) ?></span>
                                <p>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>

                    <hr style="border: 1px dashed black;" />

                    <p style="text-align: left"><?= $this->lang->line('eTRS_sales_amount'); ?>
                        <span style="float: right">SGD <?= number_format($eTRS_sales_amount, 2) ?></span>
                    </p>

                    <p style="text-align: left"><?= $this->lang->line('eTRS_gst_amount'); ?>
                        <span style="float: right">SGD <?= number_format($eTRS_sales_amount * $tax_rate / (1 + $tax_rate), 2) ?></span>
                    </p>

                    <p style="text-align: left"><?= $this->lang->line('eTRS_service_fee'); ?>
                        <span style="float: right">SGD <?= number_format($eTRS_sales_amount * $tax_rate / (1 + $tax_rate) - $eTRS_provisional_refund_amount, 2) ?></span>
                    </p>

                    <p style="text-align: left; margin-top: 20px"><?= $this->lang->line('eTRS_provisional_refund_amount'); ?>
                        <span style="float: right">SGD <?= number_format($eTRS_provisional_refund_amount, 2) ?></span>
                    </p>

                    <p class="text-center mt20">
                        <?= $this->lang->line('ticket_detail_refund_note'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="h4" style="text-align: center">
    <!--<span><?= $this->lang->line('print_eTRS_ticket'); ?></span><a style="color: red" href="" target="_top" onclick="printDiv('printableArea')"><?= $this->lang->line('here'); ?></a>-->
</div>
<!--<div id="editor"></div>-->
<!--<button id="cmd">generate PDF</button>-->
</div>
<!-- /inner content wrapper -->
</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>
</section>
<!-- /main content -->
</section>

</div>


<!-- page script -->
<script type="text/javascript" src="<?= base_url(); ?>asset/barcode/jquery-barcode.js"></script>
<script>
        var doc_id = $("#doc_id").html();
        $("#bcTarget").barcode(doc_id, "int25", {barWidth: 2, barHeight: 60, fontSize: 20, showHRI: false});
</script>

<script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
<script src="<?= base_url(); ?>asset/js/jspdf.debug.js"></script>
<script src="<?= base_url(); ?>asset/js/xepOnline.jqPlugin.js"></script>
<script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            var printPage = window.open(document.URL, '_blank');
            setTimeout(printPage.print(), 1);

            document.body.innerHTML = originalContents;
        }
</script>

<script>
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    $('#cmd').click(function () {
        doc.fromHTML($('#printableArea').html(), 15, 15, {
            'width': 230,
            'elementHandlers': specialElementHandlers
        });
        doc.save('sample-file.pdf');
    });
</script>
<!-- /page script -->