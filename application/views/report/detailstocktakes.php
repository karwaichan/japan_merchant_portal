                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">Detail Stock Take Report</h4>
                            </header>
<!--                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stocktype/createstocktype"><i class="fa fa-plus"></i> Create Stock Type</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stock/stocklist"><i class="ti-menu"></i> Stock </a>
                            </header>
                            <div class="panel-body">
                                Coming soon
                            </div>-->
                        </section>
                        <section class="panel">
                            <div class="panel-body">
                                <form id="create-summarysales" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row panel-heading">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Date Range</label>
                                                <div>
                                                    <select name="date_range" data-placeholder="Date Range" style="width:100%;" class="chosen">
                                                        <option value="0" selected="selected">Select</option>
                                                        <option value="today">Today</option>
                                                        <option value="yesterday">Yesterday</option>
                                                        <option value="last7days">Last 7 days</option>
                                                        <option value="thismonth">This Month</option>
                                                        <option value="lastmonth">Last Month</option>
                                                        <option value="thisyear">This Year</option>
                                                        <option value="lastyear">Last Year</option>
                                                        <option value="all">All time</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Or From</label>
                                                <div>
                                                    <input name="from_date" class="form-control datepicker" type="text" placeholder="02/16/2012">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>To</label>
                                                <div>
                                                    <input name="to_date" class="form-control datepicker" type="text" placeholder="02/16/2012">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Export To Excel</label>
                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <span><input type="radio" name="toexcel" value="yes">Yes</span>
                                                        <span><input type="radio" name="toexcel" value="no" checked="checked">No</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    $("#create-summarysales select[name='date_range']").on('change', function(e) {
        $("#create-summarysales input[name='from_date'],  input[name='to_date']").val('');

    });
        
    $("#create-summarysales input[name='from_date'],  input[name='to_date']").on('click', function(e) {
        $("#create-summarysales select[name='date_range'] option").prop('selected', false).trigger('chosen:updated');
    });
    </script>
    <!-- /page script -->