                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">Detail Stock Take Report <?= $from_date ?> - <?= $to_date ?></h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" id="createsaleorder" href="<?= base_url(); ?>report/detailstocktakes"><i class="ti-arrow-left"></i> Back To Detail Stock Take</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>ID</th>
                                                <th>Date</th>
                                                <th>Category</th>
                                                <th>Product Type</th>
                                                <th>Employee</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($stocktakes) && !empty($stocktakes)): ?>
                                                <?php foreach($stocktakes as $stocktake): ?>
                                                    <tr class="row-sale-order">
                                                        <td><a value="<?= $stocktake->id ?>" href="javascript:;" class="expand-sale"><i class="fa fa-arrows"></i></a></td>
                                                        <td><?= $stocktake->id ?></td>
                                                        <td><?= $stocktake->insertiondate ?></td>
                                                        <td><?= $stocktake->category ?></td>
                                                        <td><?= $stocktake->producttype ?></td>
                                                        <td><?= $stocktake->employee ?></td>
                                                    </tr>
                                                    <tr class="row-detail-order-<?= $stocktake->id ?>" id="<?= $stocktake->id ?>" style="display: none">
                                                        <td colspan="12">
                                                            
                                                            <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Stock ID</th>
                                                                        <th>Category</th>
                                                                        <th>Product Type</th>
                                                                        <th>Brand</th>
                                                                        <th>Model</th>
                                                                        <th>Color</th>
                                                                        <th>Barcode</th>
                                                                        <th>Supplier</th>
                                                                        <th>Previous Quantity</th>
                                                                        <th>Check Quantity</th>
                                                                        <th>Remark</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if(isset($stocktake->stocktakeitems) && !empty($stocktake->stocktakeitems)): ?>
                                                                        <?php foreach($stocktake->stocktakeitems as $stocktakeitem): ?>
                                                                            <tr class="row-sale-order">
                                                                                <td><?= $stocktakeitem->stock_id ?></td>
                                                                                <td><?= $stocktakeitem->category ?></td>
                                                                                <td><?= $stocktakeitem->producttype ?></td>
                                                                                <td><?= $stocktakeitem->brand ?></td>
                                                                                <td><?= $stocktakeitem->model ?></td>
                                                                                <td><?= $stocktakeitem->color ?></td>
                                                                                <td><?= $stocktakeitem->barcode ?></td>
                                                                                <td><?= $stocktakeitem->supplier ?></td>
                                                                                <td><?= $stocktakeitem->currentqty ?></td>
                                                                                <td><?= $stocktakeitem->countqty ?></td>
                                                                                <td><?= $stocktakeitem->remarks ?></td>
                                                                            </tr>
                                                                        <?php endforeach;?>
                                                                    <?php endif; ?>
                                                                </tbody>
                                                            </table>
                                                        
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    
    <script>
        $(".expand-sale").on('click', function(e) {
            var id = $(this).attr('value');
            $(".row-detail-order-"+id).toggle();
        });
    </script>
    <!-- /page script -->