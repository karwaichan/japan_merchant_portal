                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">Item Summary Report</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" id="createsaleorder" href="<?= base_url(); ?>report/summaryitems"><i class="ti-arrow-left"></i> Back To Summary Item</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Category</th>
                                                <th>Product Type</th>
                                                <th>Brand</th>
                                                <th>Model</th>
                                                <th>Color</th>
                                                <th>Quantity</th>
                                                <th>Cost Price</th>
                                                <th>Selling Price</th>
                                                <th>Profit</th>
                                                <th>Profit Percentage (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($stocks) && !empty($stocks)): ?>
                                                <?php foreach($stocks as $stock): ?>
                                                    <tr class="list-sale-order">
                                                        <td><?= $stock->stock_id ?></td>
                                                        <td><?= $stock->category_name ?></td>
                                                        <td><?= $stock->producttype_name ?></td>
                                                        <td><?= $stock->brand ?></td>
                                                        <td><?= $stock->model ?></td>
                                                        <td><?= $stock->color ?></td>
                                                        <td><?= $stock->quantity_purchased ?></td>
                                                        <td><?= $stock->cost_price ?></td>
                                                        <td><?= $stock->unit_price ?></td>
                                                        <td><?= number_format($stock->profit, NUMBER_FLOATING_POINT) ?></td>
                                                        <td><?= number_format($stock->profit_percentage, NUMBER_FLOATING_POINT) ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Summary</h3>
                                    </div>
                                </div>

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped reportTable">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Quantity</th>
                                                <th>Total Sold (<?= basecurrency() ?>)</th>
                                                <th>Profit</th>
                                                <th>Profit Percentage (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($categoryquantity) && !empty($categoryquantity)): ?>
                                                <?php foreach($categoryquantity as $key => $value): ?>
                                                    <tr class="list-sale-order">
                                                        <td><?= $categorieArray[$key] ?></td>
                                                        <td><?= $value ?></td>
                                                        <td><?= $category_sellprice[$key] ?></td>
                                                        <td><?= $category_profit[$key] ?></td>
                                                        <td><?= number_format($category_profitpercentage[$key], NUMBER_FLOATING_POINT) ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped reportTable">
                                        <thead>
                                            <tr>
                                                <th>Product Type</th>
                                                <th>Quantity</th>
                                                <th>Total Sold (<?= basecurrency() ?>)</th>
                                                <th>Profit</th>
                                                <th>Profit Percentage (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($producttypequantity) && !empty($producttypequantity)): ?>
                                                <?php foreach($producttypequantity as $key => $value): ?>
                                                    <tr class="list-sale-order">
                                                        <td><?= $producttypeArray[$key] ?></td>
                                                        <td><?= $value ?></td>
                                                        <td><?= $producttype_sellprice[$key] ?></td>
                                                        <td><?= $producttype_profit[$key] ?></td>
                                                        <td><?= number_format($producttype_profitpercentage[$key], NUMBER_FLOATING_POINT) ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <!-- /page script -->