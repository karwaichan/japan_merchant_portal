                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">Employee Summary Report <?= $from_date ?> - <?= $to_date ?></h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" id="createsaleorder" href="<?= base_url(); ?>report/summaryemployees"><i class="ti-arrow-left"></i> Back To Employee Summary</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Transaction Count</th>
                                                <th>Total Cost</th>
                                                <th>Total Amount</th>
                                                <th>Profit</th>
                                                <th>Profit Percentage (%)</th>
                                                <th>Paid Amount</th>
                                                <th>Total Discount</th>
                                                <th>Due Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($saleorders) && !empty($saleorders)): ?>
                                                <?php foreach($saleorders as $saleorder): ?>
                                                    <tr class="list-sale-order">
                                                        <td><?= $employeeArray[$saleorder->employee_id] ?></td>
                                                        <td><?= $saleorder->transaction_count ?></td>
                                                        <td><?= $saleorder->stock_cost ?></td>
                                                        <td><?= $saleorder->total_cost ?></td>
                                                        <td><?= $saleorder->profit ?></td>
                                                        <td><?= $saleorder->profit_percentage ?></td>
                                                        <td><?= $saleorder->payment_amount ?></td>
                                                        <td><?= $saleorder->total_discount ?></td>
                                                        <td><?= $saleorder->total_due ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped reportTable">
                                        <thead>
                                            <tr>
                                                <th>Data</th>
                                                <th>Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="list-sale-order">
                                                <td>Transaction Count</td>
                                                <td><?= $summary['transaction_count'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Cost</td>
                                                <td><?= $summary['stock_cost'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Amount</td>
                                                <td><?= $summary['total_cost'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Profit</td>
                                                <td><?= $summary['profit'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Profit Percentage</td>
                                                <td><?= $summary['profit_percentage'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Due</td>
                                                <td><?= $summary['total_due'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Discount</td>
                                                <td><?= $summary['total_discount'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Payment Amount</td>
                                                <td><?= $summary['payment_amount'] ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <!-- /page script -->