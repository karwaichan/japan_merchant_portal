                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">Summary Stock Holding Report</h4>
                            </header>
<!--                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stocktype/createstocktype"><i class="fa fa-plus"></i> Create Stock Type</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stock/stocklist"><i class="ti-menu"></i> Stock </a>
                            </header>
                            <div class="panel-body">
                                Coming soon
                            </div>-->
                        </section>
                        <section class="panel">
                            <div class="panel-body">
                                <form id="create-summarysales" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row panel-heading">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Age of Stock (Days)</label>
                                                <div>
                                                    <select name="date_range" data-placeholder="Date Range" style="width:100%;" class="chosen">
                                                        <option value="0" selected="selected">All</option>
                                                        <option value="15"> > 15 days</option>
                                                        <option value="20"> > 20 days</option>
                                                        <option value="25"> > 25 days</option>
                                                        <option value="30"> > 30 days</option>
                                                        <option value="35"> > 35 days</option>
                                                        <option value="40"> > 40 days</option>
                                                        <option value="45"> > 45 days</option>
                                                        <option value="50"> > 50 days</option>
                                                        <option value="55"> > 55 days</option>
                                                        <option value="60"> > 60 days</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Select Category</label>
                                                <div>
                                                    <select name="category_id" id="employee-select" data-placeholder="Select Employee" style="width:100%;" class="chosen">
                                                        <?php foreach($categorieArray as $key => $value): ?>
                                                            <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Select Product Type</label>
                                                <div>
                                                    <select name="producttype_id" id="employee-select" data-placeholder="Select Employee" style="width:100%;" class="chosen">
                                                        <?php foreach($producttypeArray as $key => $value): ?>
                                                            <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Select Supplier</label>
                                                <div>
                                                    <select name="supplier_id" id="employee-select" data-placeholder="Select Employee" style="width:100%;" class="chosen">
                                                        <?php foreach($supplierArray as $key => $value): ?>
                                                            <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Export To Excel</label>
                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <span><input type="radio" name="toexcel" value="yes">Yes</span>
                                                        <span><input type="radio" name="toexcel" value="no" checked="checked">No</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    $("#create-summarysales select[name='date_range']").on('change', function(e) {
        $("#create-summarysales input[name='from_date'],  input[name='to_date']").val('');

    });
        
    $("#create-summarysales input[name='from_date'],  input[name='to_date']").on('click', function(e) {
        $("#create-summarysales select[name='date_range'] option").prop('selected', false).trigger('chosen:updated');
    });
    </script>
    <!-- /page script -->