                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">Detail Employee Report <?= $from_date ?> - <?= $to_date ?></h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" id="createsaleorder" href="<?= base_url(); ?>report/detailemployees"><i class="ti-arrow-left"></i> Back To Detail Employee</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Transaction ID</th>
                                                <th>Transaction Date</th>
                                                <th>Sold To</th>
                                                <th>Total Cost</th>
                                                <th>Total Amount</th>
                                                <th>Profit</th>
                                                <th>Profit Percentage (%)</th>
                                                <th>Paid Amount</th>
                                                <th>Total Discount</th>
                                                <th>Due Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($saleorders) && !empty($saleorders)): ?>
                                                <?php foreach($saleorders as $saleorder): ?>
                                                    <tr class="row-sale-order">
                                                        <td><a value="<?= $saleorder->id ?>" href="javascript:;" class="expand-sale"><i class="fa fa-arrows"></i></a></td>
                                                        <td><?= $saleorder->id ?></td>
                                                        <td><?= $saleorder->transaction_date ?></td>
                                                        <td><?= $saleorder->sold_to ?></td>
                                                        <td><?= $saleorder->stock_cost ?></td>
                                                        <td><?= $saleorder->total_cost ?></td>
                                                        <td><?= $saleorder->profit ?></td>
                                                        <td><?= $saleorder->profit_percentage ?></td>
                                                        <td><?= $saleorder->payment_amount ?></td>
                                                        <td><?= $saleorder->total_discount ?></td>
                                                        <td><?= $saleorder->total_due ?></td>
                                                    </tr>
                                                    <tr class="row-detail-order-<?= $saleorder->id ?>" id="<?= $saleorder->id ?>" style="display: none">
                                                        <td colspan="12">
                                                            
                                                            <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Category</th>
                                                                        <th>Product Type</th>
                                                                        <th>Brand</th>
                                                                        <th>Model</th>
                                                                        <th>Color</th>
                                                                        <th>Barcode</th>
                                                                        <th>Supplier</th>
                                                                        <th>Quantity</th>
                                                                        <th>Total Cost</th>
                                                                        <th>Sale Price</th>
                                                                        <th>Discount</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if(isset($saleorder->saleitems) && !empty($saleorder->saleitems)): ?>
                                                                        <?php foreach($saleorder->saleitems as $saleitem): ?>
                                                                            <tr class="row-sale-order">
                                                                                <td><?= $saleitem->id ?></td>
                                                                                <td><?= $saleitem->category ?></td>
                                                                                <td><?= $saleitem->producttype ?></td>
                                                                                <td><?= $saleitem->brand ?></td>
                                                                                <td><?= $saleitem->model ?></td>
                                                                                <td><?= $saleitem->color ?></td>
                                                                                <td><?= $saleitem->barcode ?></td>
                                                                                <td><?= $saleitem->supplier ?></td>
                                                                                <td><?= $saleitem->quantity_purchased ?></td>
                                                                                <td><?= $saleitem->quantity_purchased * $saleitem->cost_price ?></td>
                                                                                <td><?= $saleitem->quantity_purchased * $saleitem->unit_price ?></td>
                                                                                <td><?= $saleitem->discount ?></td>
                                                                            </tr>
                                                                        <?php endforeach;?>
                                                                    <?php endif; ?>
                                                                </tbody>
                                                            </table>
                                                        
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped reportTable">
                                        <thead>
                                            <tr>
                                                <th>Data</th>
                                                <th>Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="list-sale-order">
                                                <td>Transaction Count</td>
                                                <td><?= $summary['transaction_count'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Amount</td>
                                                <td><?= $summary['total_cost'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Stock Cost</td>
                                                <td><?= $summary['stock_cost'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Due</td>
                                                <td><?= $summary['total_due'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Total Discount</td>
                                                <td><?= $summary['total_discount'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Payment Amount</td>
                                                <td><?= $summary['payment_amount'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Profit</td>
                                                <td><?= $summary['profit'] ?></td>
                                            </tr>
                                            <tr class="list-sale-order">
                                                <td>Profit Percentage</td>
                                                <td><?= $summary['profit_percentage'] ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    
    <script>
        $(".expand-sale").on('click', function(e) {
            var id = $(this).attr('value');
            $(".row-detail-order-"+id).toggle();
        });
    </script>
    <!-- /page script -->