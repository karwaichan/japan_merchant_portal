                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Report</h4>
                            </header>
<!--                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stocktype/createstocktype"><i class="fa fa-plus"></i> Create Stock Type</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>stock/stocklist"><i class="ti-menu"></i> Stock </a>
                            </header>
                            <div class="panel-body">
                                Coming soon
                            </div>-->
                        </section>

                        <div class="row">
                            <div class="col-md-4">
                                <section class="panel panel-color no-b">
                                    <header class="panel-heading clearfix brtl brtr">
                                        <a href="javascript:;" class="pull-left mr15">
                                            <img src="<?= base_url(); ?>asset/img/summery.png" class="avatar avatar-md img-circle bordered" alt="">
                                        </a>

                                        <div class="overflow-hidden">
                                            <a href="javascript:;" class="h4 show no-m pt10">Summary Reports</a>
                                            <!--<small>Human Resources Manager</small>-->
                                        </div>
                                    </header>

                                    <div class="list-group">
                                        <a href="<?= base_url(); ?>report/summarysales" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Sales
                                        </a>
                                        <a href="<?= base_url(); ?>report/summaryretailers" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Retailers
                                        </a>
                                        <a href="<?= base_url(); ?>report/summaryitems" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Items
                                        </a>
                                        <a href="<?= base_url(); ?>report/summaryemployees" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Employees
                                        </a>
                                        <a href="<?= base_url(); ?>report/summarystockholdings" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Stock Holding
                                        </a>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-4">
                                <section class="panel panel-color no-b">
                                    <header class="panel-heading clearfix brtl brtr">
                                        <a href="javascript:;" class="pull-left mr15">
                                            <img src="<?= base_url(); ?>asset/img/detailed.png" class="avatar avatar-md img-circle bordered" alt="">
                                        </a>

                                        <div class="overflow-hidden">
                                            <a href="javascript:;" class="h4 show no-m pt10">Detailed Reports</a>
                                        </div>
                                    </header>

                                    <div class="list-group">
                                        <a href="<?= base_url(); ?>report/detailsales" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Sales
                                        </a>
                                        <a href="<?= base_url(); ?>report/detailstocktakes" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Stock Take
                                        </a>
                                        <a href="<?= base_url(); ?>report/detailretailers" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Retailer
                                        </a>
                                        <a href="<?= base_url(); ?>report/detailemployees" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Employee
                                        </a>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-4">
                                <section class="panel panel-color no-b">
                                    <header class="panel-heading clearfix brtl brtr">
                                        <a href="javascript:;" class="pull-left mr15">
                                            <img src="<?= base_url(); ?>asset/img/log.png" class="avatar avatar-md img-circle bordered" alt="">
                                        </a>

                                        <div class="overflow-hidden">
                                            <a href="javascript:;" class="h4 show no-m pt10">Audit Logs</a>
                                        </div>
                                    </header>

                                    <div class="list-group">
                                        <a href="javascript:;" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>History
                                        </a>
                                        <a href="javascript:;" class="list-group-item">
                                            <i class="ti-arrow-right mr10"></i>Stock Import
                                        </a>
                                    </div>
                                </section>
                            </div>
                        </div>

                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-stocktype .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete user id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>stocktype/deletestocktype',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('User id = '+id+' has been deleted successfully');
                        } else {
                            bootbox.alert('User id = '+id+' can\'t deleted');
                            location.reload();
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->