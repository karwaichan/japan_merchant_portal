                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Payment Method</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>payment/createpaymentmethod"><i class="fa fa-plus"></i> Create New Payment Method</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($paymentmethods) && !empty($paymentmethods)): ?>
                                                <?php foreach($paymentmethods as $paymentmethod): ?>
                                                    <tr class="paymentmethod-list">
                                                        <td><?= $paymentmethod->id ?></td>
                                                        <td><?= $paymentmethod->name ?></td>
                                                        <?php if(isset($paymentmethod->status) && $paymentmethod->status): ?>
                                                            <td>Active</td>
                                                        <?php else: ?>
                                                            <td>Inactive</td>
                                                        <?php endif; ?>
                                                        <td>
                                                            <a href="<?= base_url(); ?>payment/updatepaymentmethod/<?= $paymentmethod->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="#" class="updatestatus" id="<?= $paymentmethod->id ?>"><i class="fa fa-minus"> Change Status </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".paymentmethod-list .updatestatus").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to change Payment Method status id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>payment/changepaymentmethodstatus',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('Payment Method id = '+id+' has been changed successfully');
                            location.reload();
                        } else {
                            bootbox.alert('Payment Method id = '+id+' can\'t be changed');
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->