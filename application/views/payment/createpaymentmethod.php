                        <section class="panel">
                            <header class="panel-heading text-center">
                                <h4>Create New Payment Method</h4>
                            </header>
                            <div class="panel-body">
                                <form id="create-payment-method" role="form" method="post" class="parsley-form form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Payment Name</label>

                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="name" data-parsley-required="true" data-parsley-trigger="change" placeholder="Payment Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Status</label>

                                                <div class="col-sm-10">
                                                    <select name="status" style="width:100%;" class="chosen">
                                                        <option value="0" selected="selected">Inactive</option>
                                                        <option value="1">Active</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#create-payment-method #signupForm").click(function(e) {
        e.preventDefault();
        var name = $( "#create-payment-method input[name='name']" ).val();
            
        if(!signupClicked){
            signupClicked = true;
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>payment/checkexistingpaymentmethod',
                    data: 'name='+name,
                    dataType: 'json',
                    success: function(data){
                        signupClicked = false;
                        if (data.status == "true"){
                            bootbox.alert('Payment name is existing, please select another!');
//                            alert(JSON.stringify(data));
                        } else {
                            $("#create-payment-method").submit();
                        }
                            
                    },
                    error: function(data){  
                        signupClicked = false;
                    }
            });
        }
    });
    </script>
    <!-- /page script -->