                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Pass Transactions for Retailer <span class="text-info"><?= $retailer->company_name ?></span></h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" id="createsaleorder" href="<?= base_url(); ?>retailer/retailerlist"><i class="ti-menu"></i> Back To Retailer List</a>
                            </header>
                            <div class="panel-body">
                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Customer</th>
                                                <th>Transaction Date</th>
                                                <th>Order Type</th>
                                                <th>Delivery Status</th>
                                                <th>Payment Status</th>
                                                <th>Order Status</th>
                                                <th>Total Amount</th>
                                                <th>Paid Amount</th>
                                                <th>Due Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($saleorders) && !empty($saleorders)): ?>
                                                <?php foreach($saleorders as $saleorder): ?>
                                                    <tr class="list-sale-order">
                                                        <td><?= $saleorder->id ?></td>
                                                        <td><?= $retailer->company_name ?></td>
                                                        <td><?= gmdate("m/d/Y", $saleorder->transaction_date) ?></td>
                                                        <td><?= $saleorder->order_type ?></td>
                                                        <td><?= $saleorder->delivery_status ?></td>
                                                        <td><?= $saleorder->payment_status ?></td>
                                                        <td><?= $saleorder->order_status ?></td>
                                                        <td><?= $saleorder->total_amount ?></td>
                                                        <td><?= $saleorder->payment_amount ?></td>
                                                        <td><?= $saleorder->total_due ?></td>
                                                        <td>
                                                            <a data-toggle="modal" class='viewsaleorder' href="<?= base_url(); ?>saleorder/stockeditiframe/<?= $saleorder->id.'/'.md5(time()) ?>" data-target="#passTransactionModal"><i class="fa fa-edit"> Edit </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    
    <!-- Modal -->
    <div class="modal fade" id="passTransactionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">Modal title</h4>

                </div>
                <div class="modal-body"><div class="te"></div></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
        $(".viewsaleorder").click(function(e) {
            createsaleorder = $(this).attr('href');
            $.ajax({
                type: 'POST',
                url: '<?= base_url(); ?>saleorder/clearSaleorderSession',
                data: 'createsaleorder='+createsaleorder,
                dataType: 'json',
                success: function(){},
                error: function(){  }
            });

        });
    </script>
    <!-- /page script -->