                        <section class="panel">
                            <header class="panel-heading">
                                <h4>Export Retailers</h4>
                            </header>
                            <div class="panel-body">
                                <form id="export-retailer-form" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label>Export File Type</label>
                                                <div>
                                                    <select name="file_type" data-placeholder="Export File Type" style="width:50%;" class="chosen">
                                                        <option value="csv" selected="selected">CSV</option>
                                                        <!--<option value="excel">Excel</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="download-export-dev" style="display:none">
                                            <div class="form-group text-center">
                                                <label>Download Link</label>
                                                <span id="retailer_export_file">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label><img src="<?= base_url(); ?>/asset/img/ajax-loader.gif" id="loading-indicator" style="display:none" /></label>
                                                <div>
                                                    <input id="export-retailer-submit" name="importStockForm" class="btn btn-primary" type="submit" value="Start Exporting" />
                                                    <a id="back-retailertypelist" class="btn btn-primary btn-heading" href="<?= base_url(); ?>retailertype/retailertypelist" style="display:none"><i class="ti-menu"></i> Back To Stock Type</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
        var signupClicked = false;
        $("#export-retailer-form #export-retailer-submit").click(function(e) {
            e.preventDefault();
            var file_type = $( "#export-retailer-form select[name='file_type']" ).val();
            if(!signupClicked){
                signupClicked = true;
                $('#loading-indicator').show();
                $.ajax({
                        type: 'POST',
                        url: '<?= base_url(); ?>retailer/retailerexportfile',
                        data: 'file_type='+file_type,
                        dataType: 'json',
                        success: function(data){
                            signupClicked = false;
                            if (data.status == "true"){
                                bootbox.alert('Stock export: Done!');
                                $('#export-retailer-submit').hide();
                                $('#back-retailertypelist').show();
                                $('#download-export-dev').show();
                                link = base_url+'uploads/retailers/'+data.message;
                                $( "#export-retailer-form #retailer_export_file").html('<a href="'+link+'">'+link+'</a>');
                            } else {}
                            $('#loading-indicator').hide();

                        },
                        error: function(data){
                            $('#loading-indicator').hide();
                            signupClicked = false;
                        }
                    });
            }

        });
    </script>
    <!-- /page script -->