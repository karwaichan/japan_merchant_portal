                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Retailer</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>retailer/createretailer"><i class="fa fa-plus"></i> Create New Retailer</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>retailer/importretailer"><i class="fa fa-cloud-upload"></i> Import Retailer</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>retailer/exportretailer"><i class="fa fa-cloud-download"></i> Export Retailer</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Fule Name</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Currency</th>
                                                <th>Website</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($retailers) && !empty($retailers)): ?>
                                                <?php foreach($retailers as $retailer): ?>
                                                    <tr class="delete-retailer">
                                                        <td><?= $retailer->id ?></td>
                                                        <td><?= $retailer->first_name.' '.$retailer->last_name ?></td>
                                                        <td><?= $retailer->company_name ?></td>
                                                        <td><?= $retailer->email ?></td>
                                                        <td><?= $retailer->country ?></td>
                                                        <td><?= $retailer->currency ?></td>
                                                        <td><?= $retailer->website_url ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>retailer/updateretailer/<?= $retailer->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="javascript:;" class="delete" id="<?= $retailer->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                            <a href="<?= base_url(); ?>retailer/passtransaction/<?= $retailer->id ?>" class="passtransaction" id="<?= $retailer->id ?>"><i class="ti-menu"> Pass Transactions </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-retailer .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete retailer id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>retailer/deleteretailer',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('Retailer id = '+id+' has been deleted successfully');
                        } else {
                            bootbox.alert('Retailer id = '+id+' can\'t deleted');
                            location.reload();
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->