                        <section class="panel">
                            <header class="panel-heading text-center">
                                <h4>Base Currency Setting</h4>
                            </header>
                            <header class="panel-heading">
                                Go to
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>currency/currencylist"><i class="ti-menu"></i> Currency Rates</a>
                            </header>
                            <div class="panel-body">
                                <form id="create-employee-group" role="form" method="post" class="parsley-form form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Change Base Currency To</label>

                                                <div class="col-sm-10">
                                                    <select name="base_currency" style="width:100%;" class="chosen">
                                                        <?php foreach($currency_types as $key => $value): ?>
                                                            <?php if($key == $base_currency[0]->value): ?>
                                                                <option value="<?= $key ?>" selected="selected"><?= $currency_base_currency = $value ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $key ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 text-right">Current base: </label>
                                                <label class="col-sm-10 text-left"><?= strtoupper($currency_base_currency); ?></label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    </script>
    <!-- /page script -->