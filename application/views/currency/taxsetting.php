                        <section class="panel">
                            <header class="panel-heading text-center">
                                <h4>Tax Setting</h4>
                            </header>
                            <div class="panel-body">
                                <form id="create-rolemanagement" role="form" method="post" class="parsley-form form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Tax (%)</label>

                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="tax" data-parsley-type="number" data-parsley-required="true" data-parsley-trigger="change" placeholder="Role name" value="<?= $setting[0]->value ?>">
                                                </div>
                                                <div class="col-sm-4"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#create-rolemanagement #signupForm").click(function(e) {
        e.preventDefault();
        var role = $( "#create-rolemanagement input[name='role']" ).val();
            
        if(!signupClicked){
            signupClicked = true;
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>rolemanagement/checkexistingrole',
                    data: 'role='+role,
                    dataType: 'json',
                    success: function(data){
                        signupClicked = false;
                        if (data.status == "true"){
                            bootbox.alert('Role is existing, please select another!');
//                            alert(JSON.stringify(data));
                        } else {
                            $("#create-rolemanagement").submit();
                        }
                            
                    },
                    error: function(data){  
                        signupClicked = false;
                    }
            });
        }
    });
    $("#create-rolemanagement input[id='stocktype']:radio").change(function() {
        stocktype_radio_value = this.value;
        if (stocktype_radio_value == 'modify'){
            $("#create-rolemanagement #view-stock-type").show();
        } else {
            $("#create-rolemanagement #view-stock-type").hide();
            $("#create-rolemanagement input[name='rolepermission[stocktypelist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createstocktype]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updatestocktype]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deletestocktype]']:radio").filter("[value='none']").prop("checked",true);
        }
    });
    $("#create-rolemanagement input[id='rolemanagement']:radio").change(function() {
        rolemanagement_radio_value = this.value;
        if (rolemanagement_radio_value == 'modify'){
            $("#create-rolemanagement #view-role-management").show();
        } else {
            $("#create-rolemanagement #view-role-management").hide();
            $("#create-rolemanagement input[name='rolepermission[rolemanagementlist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createrolemanagement]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updaterolemanagement]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deleterolemanagement]']:radio").filter("[value='none']").prop("checked",true);
        }
    });
    $("#create-rolemanagement input[id='employee']:radio").change(function() {
        employee_radio_value = this.value;
        if (employee_radio_value == 'modify'){
            $("#create-rolemanagement #view-employee").show();
        } else {
            $("#create-rolemanagement #view-employee").hide();
            $("#create-rolemanagement input[name='rolepermission[employeelist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updateemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deleteemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[employeegrouplist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createemployeegroup]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updateemployeegroup]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deleteemployeetype]']:radio").filter("[value='none']").prop("checked",true);
        }
    });
    </script>
    <!-- /page script -->