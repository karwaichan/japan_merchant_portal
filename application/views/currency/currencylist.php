                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Currency Rate</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>currency/createcurrency"><i class="fa fa-plus"></i> Create New Currency Pair</a>
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>currency/currencybase"><i class="ti-settings"></i> Change Base Currency</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Rate</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($currencies) && !empty($currencies)): ?>
                                                <?php foreach($currencies as $currency): ?>
                                                    <tr class="delete-currency">
                                                        <td><?= $currency->id ?></td>
                                                        <td><?= $currency_types[$currency->from] ?></td>
                                                        <td><?= $currency_types[$currency->to] ?></td>
                                                        <td><?= $currency->rate ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>currency/updatecurrency/<?= $currency->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="javascript:;" class="delete" id="<?= $currency->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-currency .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete user id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>currency/deletecurrency',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('User id = '+id+' has been deleted successfully');
                        } else {
                            bootbox.alert('User id = '+id+' can\'t deleted');
                            location.reload();
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->