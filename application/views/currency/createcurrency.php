                        <section class="panel">
                            <header class="panel-heading text-center">
                                <h4>Create New Currency Pair</h4>
                            </header>
                            <div class="panel-body">
                                <form id="create-currency" role="form" method="post" class="parsley-form form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">From Base Currency</label>

                                                <div class="col-sm-10">
                                                    <select name="from" style="width:100%;" class="chosen">
                                                        <option value="<?= $base_currency[0]->value ?>"><?= $currency_types[$base_currency[0]->value] ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">To Currency</label>

                                                <div class="col-sm-10">
                                                    <select name="to" style="width:100%;" class="chosen">
                                                        <?php foreach($currency_types as $key => $value): ?>
                                                            <?php if($key != $base_currency[0]->value): ?>
                                                                <option value="<?= $key ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Rate</label>

                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="rate" data-parsley-required="true" data-parsley-trigger="change" data-parsley-type="number" placeholder="Rate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#create-currency #signupForm").click(function(e) {
        e.preventDefault();
        var from = $( "#create-currency select[name='from']" ).val();
        var to = $( "#create-currency select[name='to']" ).val();
            
        if(!signupClicked){
            signupClicked = true;
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>currency/checkexistingcurrencypair',
                    data: 'from='+from+'&to='+to,
                    dataType: 'json',
                    success: function(data){
                        signupClicked = false;
                        if (data.status == "true"){
                            bootbox.alert('Currency To is existing, please select another!');
//                            alert(JSON.stringify(data));
                        } else {
                            $("#create-currency").submit();
                        }
                            
                    },
                    error: function(data){  
                        signupClicked = false;
                    }
            });
        }
    });
    </script>
    <!-- /page script -->