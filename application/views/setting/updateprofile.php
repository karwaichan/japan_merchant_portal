                        <section class="panel">
                            <header class="panel-heading no-b">
                                <h4><?= $this->lang->line('update_profile_title'); ?> - <?= $user_profile->user->first_name.' '.$user_profile->user->last_name ?></h4>
                            </header>
                            <div class="panel-body">
                                <form id="update-profile" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('username'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="username" data-parsley-required="true" data-parsley-trigger="change" placeholder="<?= $this->lang->line('username'); ?>" value="<?= $user_profile->user->username ?>" disabled>
                                                    <input type="hidden" name="id" value="<?= $user_profile->user->id ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?= $this->lang->line('password'); ?></label>
                                                <div>
                                                    <input type="password" class="form-control" name="password" placeholder="<?= $this->lang->line('password'); ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?= $this->lang->line('retype_password'); ?></label>
                                                <div>
                                                    <input type="password" class="form-control" name="retype_password" placeholder="<?= $this->lang->line('retype_password'); ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?= $this->lang->line('retailer_name'); ?></label>
                                                <div>
                                                    <input disabled type="text" class="form-control" name="company_name" placeholder="<?= $this->lang->line('retailer_name'); ?>" value="<?= $user_profile->merchant->retailer_name ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?= $this->lang->line('role'); ?></label>
                                                <div>
                                                    <select name="role_id" data-placeholder="role_id" style="width:100%;" class="chosen">
                                                        <?php foreach($roles as $key => $value): ?>
                                                            <?php if ($key == $user_profile->role): ?>
                                                                <option value="<?= $key ?>" selected="selected"><?= $value ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $key ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?= $this->lang->line('outlet'); ?></label>
                                                <div>
                                                    <select name="outlet_id" data-placeholder="outlet_id" style="width:100%;" class="chosen">
                                                        <?php foreach($outlets as $key => $value): ?>
                                                            <?php if ($key == $user_profile->outlet->id): ?>
                                                                <option value="<?= $key ?>" selected="selected"><?= $value ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $key ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('email'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="email" data-parsley-type="email" data-parsley-required="true" data-parsley-trigger="change" placeholder="retailer@tourego.com.sg" value="<?= $user_profile->user->email ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?= $this->lang->line('last_name'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="last_name" placeholder="<?= $this->lang->line('last_name'); ?>" value="<?= $user_profile->user->last_name ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?= $this->lang->line('first_name'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="first_name" data-parsley-required="true" data-parsley-trigger="change" placeholder="<?= $this->lang->line('first_name'); ?>" value="<?= $user_profile->user->first_name ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?= $this->lang->line('contact_number'); ?></label>
                                                <div>
                                                    <input type="text" class="form-control" name="phone_number" data-parsley-type="digits" data-rangelength="[11,20]" placeholder="18005551234" value="<?= $user_profile->user->phone_number ?>">
                                                </div>
                                            </div>
                                            
<!--                                            <div class="form-group">
                                                <label>Photo Upload</label>
                                                <div>
                                                    <input type="file" name="reseller_photo"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div>
                                                    <div class="thumb text-center">
                                                        <?php if(isset($user_profile->user->reseller_photo)): ?>
                                                            <img class="img-thumbnail stocktype-thump" src="<?= base_url(); ?>uploads/resellers/<?= $reseller->id.'/'.$reseller->reseller_photo ?>" alt="Responsive image">
                                                        <?php else: ?>
                                                            <img class="img-thumbnail stocktype-thump" src="<?= base_url(); ?>asset/img/faceless.jpg" alt="Responsive image">
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>-->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="<?= $this->lang->line('reset'); ?>" />
                                                    <input id="updateProfile" class="btn btn-primary btn-lg btn-parsley" type="submit" value="<?= $this->lang->line('submit'); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <!-- /page script -->
    <script>
    
        $("#updateProfile").on('click', function(e) {
            e.preventDefault();
            var password = $( "input[name='password']").val().trim();
            var retype_password = $( "input[name='retype_password']").val().trim();
            
            if(password.length > 0 && password !== retype_password){
                alert('Password and Retype Password are not matched');
            } else {
                $("#update-profile").submit();
            }
        });
    </script>