    <!-- core scripts -->
    <script src="<?= base_url(); ?>asset/plugins/jquery.slimscroll.min.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/appear/jquery.appear.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/jquery.placeholder.js"></script>
    <!-- /core scripts -->

    <!-- page level scripts -->
    <script src="<?= base_url(); ?>asset/plugins/parsley.min.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?= base_url(); ?>asset/plugins/switchery/switchery.js"></script>
    <!-- /page level scripts -->

    <!-- template scripts -->
    <script src="<?= base_url(); ?>asset/js/offscreen.js"></script>
    <script src="<?= base_url(); ?>asset/js/main.js"></script>
    <!-- /template scripts -->

    <!-- page script -->
    <!-- /page script -->

</body>
<!-- /body -->

</html>