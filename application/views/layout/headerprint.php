<?php
@header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
@header("Expires: Wed, 4 Jul 2012 05:00:00 GMT"); // Date in the past
?>
<!doctype html>
<html class="signin no-js" lang="">

<head>
    <link rel="shortcut icon" href="<?= base_url(); ?>asset/favicon.png" type="img/x-icon">
    <!-- meta -->
    <meta charset="utf-8">
    <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <!-- /meta -->

    <title><?= SITE_NAME ?></title>

    <!-- page level plugin styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>asset/plugins/chosen/chosen.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/plugins/datatables/jquery.dataTables.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/plugins/datepicker/datepicker.css">
    <!-- /page level plugin styles -->

    <!-- core styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/font-awesome.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/animate.min.css">
    <!-- /core styles -->

    <!-- template styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/skins/palette.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/fonts/font.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/main.css">
    <!-- template styles -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- load modernizer -->
    <script src="<?= base_url(); ?>asset/plugins/modernizr.js"></script>
    <script type="text/javascript">
        var base_url = '<?php echo base_url()?>';
        function close_window() {
            close();
        }
    </script>

    <!-- core scripts -->
    <script src="<?= base_url(); ?>asset/plugins/jquery-1.11.1.min.js"></script>
    <!-- core scripts -->

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/bootstrap/js/bootstrap.js"></script>
    <script src="<?= base_url(); ?>asset/js/bootbox.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/bootstrap-datatables.js"></script>
    <!-- /page script -->

    
<style type="text/css">
    body{
        background-color: #ffffff;
    }
    .table{
        margin-bottom: 0px;
    }
    table.borderless, table.print-bordered{
        width: 100%;
        /*height: 100%;*/
    }
    table.borderless td,table.borderless th{
        border: none !important;
        padding: 5px;
    }
    table.print-bordered td, th {
        border: 1px solid black;
        padding: 5px;
    }
</style>
</head>

<!-- body -->

<body>