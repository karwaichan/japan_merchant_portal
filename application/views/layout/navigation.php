﻿<?php $CI =& get_instance(); ?>
    <div class="app">
        <!-- top header -->
        <header class="header header-fixed navbar">

            <div class="brand">
                <!-- toggle offscreen menu -->
                <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
                <!-- /toggle offscreen menu -->

                <!-- logo -->
                <a href="<?= base_url(); ?>" class="navbar-brand">
                    <img src="<?= base_url(); ?>asset/img/logo.png" alt="">
                    <span class="heading-font">
                        <?= $this->lang->line('merchant_portal'); ?>
                    </span>
                </a>
                <!-- /logo -->
            </div>

            <ul class="nav navbar-nav">
                <li class="hidden-xs">
                    <!-- toggle small menu -->
                    <a href="javascript:;" class="toggle-sidebar">
                        <i class="ti-menu"></i>
                    </a>
                    <!-- /toggle small menu -->
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="notifications dropdown">
                    <a href="javascript:;" data-toggle="dropdown">
                        <i class="ti-settings"></i>
                    </a>
                    <div class="dropdown-menu animated fadeInLeft">
                        <div class="panel panel-default no-m">
                            <div class="panel-heading small"><b><?= $this->lang->line('language'); ?></b>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item <?= ($this->session->userdata('site_lang') == 'japanese') ? 'active' : '' ?>">
                                    <a href="<?= base_url(); ?>languageswitcher/switchlang/japanese">
                                        <span class="pull-left mb5 mr15">
                                            <img src="<?= base_url(); ?>asset/img/faceless.jpg" class="avatar avatar-sm img-circle" alt="">
                                        </span>
                                        <div class="m-body">
                                            <span>日本語</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item <?= ($this->session->userdata('site_lang') == 'english') ? 'active' : '' ?>">
                                    <a href="<?= base_url(); ?>languageswitcher/switchlang/english">
                                        <span class="pull-left mb5 mr15">
                                            <img src="<?= base_url(); ?>asset/img/faceless.jpg" class="avatar avatar-sm img-circle" alt="">
                                        </span>
                                        <div class="m-body">
                                            <span>English</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item <?= ($this->session->userdata('site_lang') == 'chinese') ? 'active' : '' ?>">
                                    <a href="<?= base_url(); ?>languageswitcher/switchlang/english">
                                        <span class="pull-left mb5 mr15">
                                            <img src="<?= base_url(); ?>asset/img/faceless.jpg" class="avatar avatar-sm img-circle" alt="">
                                        </span>
                                        <div class="m-body">
                                            <span>中文</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="off-right">
                    <a href="javascript:;" data-toggle="dropdown">
                        <img src="<?= base_url(); ?>asset/img/faceless.jpg" class="header-avatar img-circle" alt="user" title="user">
                        <span class="hidden-xs ml10"><?= $CI->session->userdata('accountName'); ?></span>
                        <i class="ti-angle-down ti-caret hidden-xs"></i>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight">
                        <?php if($CI->session->userdata('accountParentId') == 0): ?>
                        <li>
                            <a href="<?= base_url(); ?>profile/updateprofile"><?= $this->lang->line('update_profile'); ?></a>
                        </li>
                        <?php endif; ?>
<!--                        <li>
                            <a href="javascript:;"><?= $this->lang->line('help'); ?></a>
                        </li>-->
                        <li>
                            <a href="<?= base_url(); ?>home/logout"><?= $this->lang->line('logout'); ?></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>
        <!-- /top header -->

        <section class="layout">
            <!-- sidebar menu -->
            <aside class="sidebar offscreen-left">
                <!-- main navigation -->
                <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
                    <p class="nav-title"><?= $this->lang->line('menu'); ?></p>
                    <ul class="nav">
                        <!-- dashboard -->
                        <li>
                            <a href="<?= base_url(); ?>">
                                <i class="ti-home"></i>
                                <span><?= $this->lang->line('dashboard'); ?></span>
                            </a>
                        </li>
                        <!-- /dashboard -->

                        <!-- ui -->
<!--                        <li>
                            <a href="<?= base_url(); ?>newsevent/newseventlist">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('newseventlist'); ?></span>
                            </a>
                        </li>-->
                        <!-- /ui -->
                        

                        <!-- ui -->
                        <li>
                            <a href="<?= base_url(); ?>pos/viewqrcode">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('qr_code'); ?></span>
                            </a>
                        </li>
                        <!-- /ui -->
                        
                        <!-- ui -->
                        <li>
                            <a href="<?= base_url(); ?>receipt/receiptlist">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('transaction'); ?></span>
                            </a>
                        </li>
                        <!-- /ui -->
                        
                        <!-- ui -->
<!--                        <li>
                            <a href="<?= base_url(); ?>employee/employeelist">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('employeelist'); ?></span>
                            </a>
                        </li>-->
                        <!-- /ui -->
                        
                        <!-- ui -->
<!--                        <li>
                            <a href="<?= base_url(); ?>outlet/updateoutlet">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('update_outlet'); ?></span>
                            </a>
                        </li>-->
                        <!-- /ui -->
                        
                        <!-- ui -->
<!--                        <li>
                            <a href="<?= base_url(); ?>outlet/updateoutletcategory">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('update_outlet_category'); ?></span>
                            </a>
                        </li>-->
                        <!-- /ui -->
                        
                        <!-- ui -->
<!--                        <li>
                            <a href="<?= base_url(); ?>tracking/trackinglist">
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span><?= $this->lang->line('tracking_list'); ?></span>
                            </a>
                        </li>-->
                        <!-- /ui -->

                        <!-- ui -->
<!--                        <li>
                            <a href="javascript:;">
                                <i class="toggle-accordion"></i>
                                <i class="ti-layout-media-overlay-alt-2"></i>
                                <span>Transaction</span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?= base_url(); ?>receipt/receiptlist">
                                        <span>Receipt</span>
                                    </a>
                                </li>
                            </ul>
                        </li>-->
                        <!-- /ui -->

                    </ul>
                </nav>
            </aside>
            <!-- /sidebar menu -->
            

            <!-- main content -->
            <section class="main-content">
                
                <!-- content wrapper -->
                <div class="content-wrap">

                    <!-- inner content wrapper -->
                    <div class="wrapper">
                        <?php if($this->session->flashdata('message')): ?>
                            <div class="alert alert-success text-center alert-dismissable">
                                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                <?= $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger text-center alert-dismissable">
                                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                <?= $this->session->flashdata('error'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('warning')): ?>
                            <div class="alert alert-warning text-center alert-dismissable">
                                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                <?= $this->session->flashdata('warning'); ?>
                            </div>
                        <?php endif; ?>