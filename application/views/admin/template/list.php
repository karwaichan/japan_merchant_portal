                        <section class="panel panel-default">
                            <header class="panel-heading">
                                Data Tables<a class="btn btn-primary" href="/admin/reseller/createreseller">New Reseller</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Office</th>
                                                <th>Age</th>
                                                <th>Start Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Airi Satou</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                <td>5407</td>
                                                <td>2008/11/28</td>
                                                <td>
                                                    <a href="/admin/reseller/createreseller" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                    <a href="javascript:;" class="delete"><i class="fa fa-eraser"> Delete </i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Angelica Ramos</td>
                                                <td>Chief Executive Officer (CEO)</td>
                                                <td>London</td>
                                                <td>5797</td>
                                                <td>2009/10/09</td>
                                                <td>
                                                    <a href="/admin/reseller/createreseller" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                    <a href="javascript:;" class="delete"><i class="fa fa-eraser"> Delete </i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Ashton Cox</td>
                                                <td>Junior Technical Author</td>
                                                <td>San Francisco</td>
                                                <td>1562</td>
                                                <td>2009/01/12</td>
                                                <td>
                                                    <a href="/admin/reseller/createreseller" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                    <a href="javascript:;" class="delete"><i class="fa fa-eraser"> Delete </i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Bradley Greer</td>
                                                <td>Software Engineer</td>
                                                <td>London</td>
                                                <td>2558</td>
                                                <td>2012/10/13</td>
                                                <td>
                                                    <a href="/admin/reseller/createreseller" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                    <a href="javascript:;" class="delete"><i class="fa fa-eraser"> Delete </i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Brenden Wagner</td>
                                                <td>Software Engineer</td>
                                                <td>San Francisco</td>
                                                <td>1314</td>
                                                <td>2011/06/07</td>
                                                <td>
                                                    <a href="/admin/reseller/createreseller" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                    <a href="javascript:;" class="delete"><i class="fa fa-eraser"> Delete </i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <!-- /page script -->