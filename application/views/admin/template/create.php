                        <section class="panel">
                            <header class="panel-heading no-b">
                                <h4>Create New Reseller</h4>
                            </header>
                            <div class="panel-body">
                                <form role="form" class="parsley-form" data-parsley-validate>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="firstname" data-parsley-required="true" data-parsley-trigger="change" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Last name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="lastname" data-parsley-required="true" data-parsley-trigger="change" placeholder="Last Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Country</label>
                                                <div>
                                                    <input type="text" class="form-control" name="country" data-parsley-required="true" data-parsley-trigger="change" placeholder="Country">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>State/Province</label>
                                                <div>
                                                    <input type="text" class="form-control" name="state" data-parsley-required="true" data-parsley-trigger="change" placeholder="State/Province">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>City</label>
                                                <div>
                                                    <input type="text" class="form-control" name="city" data-parsley-required="true" data-parsley-trigger="change" placeholder="City">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Enable</label>
                                                <div>
                                                    <input type="checkbox" class="js-switch-blue-1" />
                                                    <input type="checkbox" class="js-switch-blue-2" checked />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Zipcode</label>
                                                <div>
                                                    <input type="text" class="form-control" name="zipcode" data-parsley-type="alphanum" data-parsley-required="true" data-parsley-trigger="change" placeholder="P6B6L4">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Date of Birth</label>
                                                <div>
                                                    <input class="form-control datepicker" data-parsley-required="true" type="text" value="02/16/2012">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Mobile phone</label>
                                                <div>
                                                    <input type="text" class="form-control" name="mobilephone" data-parsley-type="digits" data-rangelength="[11,20]" data-parsley-required="true" data-parsley-trigger="change" placeholder="18005551234">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Email</label>
                                                <div>
                                                    <input type="text" class="form-control" name="email" data-parsley-type="email" data-parsley-required="true" data-parsley-trigger="change" placeholder="hello@nyasha.me">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Website</label>
                                                <div>
                                                    <input type="text" class="form-control" name="website" data-parsley-type="url" data-parsley-required="true" data-parsley-trigger="change" placeholder="http://nyasha.me">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <!-- /page script -->