                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Resellers</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>admin/reseller/createreseller"><i class="fa fa-plus"></i> Create New Reseller</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Account Type</th>
                                                <th>Trial</th>
                                                <th>Renewal Date</th>
                                                <th>Account Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($resellers) && !empty($resellers)): ?>
                                                <?php foreach($resellers as $reseller): ?>
                                                    <tr class="delete-reseller">
                                                        <td><?= $reseller->id ?></td>
                                                        <td><?= $reseller->username ?></td>
                                                        <td><?= $reseller->email ?></td>
                                                        <td><?= ($reseller->account_type == USERTYPE_RESELLER) ? "Reseller" : "Other" ?></td>
                                                        <td><?= $reseller->trial ?></td>
                                                        <td><?= gmdate("m/d/Y", $reseller->renewal_date) ?></td>
                                                        <td><?= $reseller->account_status ?></td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>admin/reseller/updatereseller/<?= $reseller->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <?php if($reseller->account_status == 'active'): ?>
                                                                <a href="javascript:;" class="flag" id="<?= $reseller->id ?>" value="deactive"><i class="fa fa-frown-o"> Flag </i></a>
                                                            <?php elseif($reseller->account_status == 'deactive'): ?>
                                                                <a href="javascript:;" class="flag" id="<?= $reseller->id ?>" value="active"><i class="fa fa-check"> Unflag </i></a>
                                                            <?php endif; ?>
                                                            <a href="javascript:;" class="delete" id="<?= $reseller->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-reseller .flag").click(function(e) {
        var id = $(this).attr('id');
        var flag_value = $(this).attr('value');
        bootbox.confirm("Are you sure to flag user id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>admin/reseller/flagreseller',
                    data: 'id='+id+'&account_status='+flag_value,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('User id = '+id+' has been '+data.message+' successfully');
                            location.reload();
                        } else {
                            bootbox.alert('User id = '+id+' can\'t be flagged');
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    $(".delete-reseller .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete user id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>admin/reseller/deletereseller',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('User id = '+id+' has been deleted successfully');
                        } else {
                            bootbox.alert('User id = '+id+' can\'t be deleted');
                            location.reload();
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->