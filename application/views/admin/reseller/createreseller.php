                        <section class="panel">
                            <header class="panel-heading no-b">
                                <h4>Create New Reseller</h4>
                            </header>
                            <div class="panel-body">
                                <form id="create-reseller" role="form" method="post" class="parsley-form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div>
                                                    <input type="text" class="form-control" name="email" data-parsley-type="email" data-parsley-required="true" data-parsley-trigger="change" placeholder="reseller@inspirepos.com.sg">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Username</label>
                                                <div>
                                                    <input type="text" class="form-control" name="username" data-parsley-required="true" data-parsley-trigger="change" placeholder="Username">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <div>
                                                    <input type="password" class="form-control" name="password" data-parsley-required="true" data-parsley-trigger="change" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Account Type</label>
                                                <div>
                                                    <select name="account_type" data-placeholder="Account type" style="width:100%;" class="chosen">
                                                        <option value="<?= USERTYPE_RESELLER ?>">Reseller</option>
                                                        <option value="<?= USERTYPE_OTHER ?>">Other</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Trial</label>
                                                <div>
                                                    <input name="trial" type="checkbox" class="js-switch-blue-1" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Renewal Date</label>
                                                <div>
                                                    <input name="renewal_date" class="form-control datepicker" data-parsley-required="true" type="text" placeholder="02/16/2012">
                                                </div>
                                            </div>

<!--                                            <div class="form-group">
                                                <label>License Period</label>
                                                <div>
                                                    <input name="license_period" class="form-control datepicker" data-parsley-required="true" type="text" placeholder="02/16/2012">
                                                </div>
                                            </div>-->
                                            
                                            <div class="form-group">
                                                <label>Account Status</label>
                                                <div>
                                                    <select name="account_status" data-placeholder="Account Status" style="width:100%;" class="chosen">
                                                        <option value="active">Active</option>
                                                        <option value="deactive" selected="selected">Deactive</option>
                                                        <option value="deleted">Deleted</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="first_name" data-parsley-required="true" data-parsley-trigger="change" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Last name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <div>
                                                    <input type="text" class="form-control" name="company_name" placeholder="Company Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Account Number</label>
                                                <div>
                                                    <input type="text" class="form-control" name="account_no" placeholder="Account Number">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address</label>
                                                <div>
                                                    <input type="text" class="form-control" name="address" placeholder="Address">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Country</label>
                                                <div>
                                                    <select name="country" data-placeholder="Country" style="width:100%;" class="chosen">
                                                        <?php foreach($countries as $key => $value): ?>
                                                            <?php if ($value == 'Singapore'): ?>
                                                                <option value="<?= $value ?>" selected="selected"><?= $value ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $value ?>"><?= $value ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone</label>
                                                <div>
                                                    <input type="text" class="form-control" name="phone" data-parsley-type="digits" data-rangelength="[11,20]" placeholder="18005551234">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Login Page</label>
                                                <div>
                                                    <input type="text" class="form-control" name="login_page" placeholder="Login Page">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#create-reseller #signupForm").click(function(e) {
        e.preventDefault();
        var username = $( "#create-reseller input[name*='username']" ).val();
            
        if(!signupClicked){
            signupClicked = true;
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>admin/reseller/checkreseller',
                    data: 'username='+username,
                    dataType: 'json',
                    success: function(data){
                        signupClicked = false;
                        if (data.status == "true"){
                            bootbox.alert('Username is existing, please select another!');
//                            alert(JSON.stringify(data));
                        } else {
                            $("#create-reseller").submit();
                        }
                            
                    },
                    error: function(data){  
                        signupClicked = false;
                    }
                });
        }

    });
    </script>
    <!-- /page script -->