<!--<body class="bg-primary">-->

<!--    <div class="cover" style="background-image: url(<?= base_url(); ?>asset/img/cover1.jpg)"></div>-->

    <div class="overlay bg-primary"></div>

    <div class="center-wrapper">
        <div class="center-content">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <section class="panel bg-white no-b text-center">
                        <header class="panel-heading">
                            <h4>Welcome to Tourego - Merchant <Portal></Portal></h4>
                        </header>
                        <p class="h4 show no-m pt10">
                            Sign in
                        </p>
                        <div class="p15">
                            <?php if($this->session->flashdata('error')): ?>
                                <div class="alert alert-danger text-center alert-dismissable">
                                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                    <?= $this->session->flashdata('error'); ?>
                                </div> 
                            <?php endif; ?>
                            <form role="form" action="#" method="post" class="parsley-form" data-parsley-validate>
                                <input name="username" data-parsley-minlength="3" data-parsley-required="true" data-parsley-trigger="change" type="text" class="form-control input-lg mb25" placeholder="Username" autofocus>
                                <input name="password" data-parsley-minlength="6" data-parsley-required="true" data-parsley-trigger="change" type="password" class="form-control input-lg mb25" placeholder="Password">
                                <div class="show">
                                    <label class="pull-right">
                                        <a href="/">Forgot password?</a>
                                    </label>
                                </div>
                                
                                <input class="btn btn-primary btn-lg btn-block" type="submit" value="Sign in" />
                            </form>
                        </div>
                    </section>
                    <p class="text-center">
                        Copyright &copy;
                        <span id="year" class="mr5"></span>
                        <span>Tourego</span>
                    </p>
                </div>
            </div>
        
        </div>
    </div>
    <script type="text/javascript">
        var el = document.getElementById("year"),
            year = (new Date().getFullYear());
        el.innerHTML = year;
    </script>