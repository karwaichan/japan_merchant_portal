                        <section class="panel">
                            <header class="panel-heading text-center">
                                <h4>Create New Role</h4>
                            </header>
                            <div class="panel-body">
                                <form id="create-rolemanagement" role="form" method="post" class="parsley-form form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Role name</label>

                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="role" data-parsley-required="true" data-parsley-trigger="change" placeholder="Role name">
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <label>Select the options below to grant access to the required modules</label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Employee</label>

                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[employee]" id="employee" checked value="none"/> No Access
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[employee]" id="employee" value="read"/> Read Only
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[employee]" id="employee" value="modify"/> Modify
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-bordered" id="view-employee" style="display: none" >
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Employee List</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeelist]" id="employeelist" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeelist]" id="employeelist" value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Employee</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployee]" id="createemployee" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployee]" id="createemployee" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Employee</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployee]" id="updateemployee" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployee]" id="updateemployee" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Employee</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployee]" id="deleteemployee" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployee]" id="deleteemployee" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Employee Type List</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeegrouplist]" id="employeegrouplist" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeegrouplist]" id="employeegrouplist" value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Employee Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployeegroup]" id="createemployeegroup" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployeegroup]" id="createemployeegroup" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Employee Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployeegroup]" id="updateemployeegroup" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployeegroup]" id="updateemployeegroup" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Employee Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployeetype]" id="deleteemployeetype" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployeetype]" id="deleteemployeetype" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Stock Type</label>

                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[stocktype]" id="stocktype" checked value="none"/> No Access
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[stocktype]" id="stocktype" value="read"/> Read Only
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[stocktype]" id="stocktype" value="modify"/> Modify
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-bordered" id="view-stock-type" style="display: none" >
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[stocktypelist]" id="stocktypelist" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[stocktypelist]" id="stocktypelist" value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createstocktype]" id="createstocktype" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createstocktype]" id="createstocktype" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updatestocktype]" id="updatestocktype" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updatestocktype]" id="updatestocktype" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deletestocktype]" id="deletestocktype" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deletestocktype]" id="deletestocktype" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Role Management</label>

                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[rolemanagement]" id="rolemanagement" checked value="none"/> No Access
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[rolemanagement]" id="rolemanagement" value="read"/> Read Only
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[rolemanagement]" id="rolemanagement" value="modify"/> Modify
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-bordered" id="view-role-management" style="display: none" >
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Role Management List</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[rolemanagementlist]" id="rolemanagementlist" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[rolemanagementlist]" id="rolemanagementlist" value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Role Management</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createrolemanagement]" id="createrolemanagement" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createrolemanagement]" id="createrolemanagement" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Role Management</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updaterolemanagement]" id="updaterolemanagement" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updaterolemanagement]" id="updaterolemanagement" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Role Management</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleterolemanagement]" id="deleterolemanagement" checked value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleterolemanagement]" id="deleterolemanagement" value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#create-rolemanagement #signupForm").click(function(e) {
        e.preventDefault();
        var role = $( "#create-rolemanagement input[name='role']" ).val();
            
        if(!signupClicked){
            signupClicked = true;
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>rolemanagement/checkexistingrole',
                    data: 'role='+role,
                    dataType: 'json',
                    success: function(data){
                        signupClicked = false;
                        if (data.status == "true"){
                            bootbox.alert('Role is existing, please select another!');
//                            alert(JSON.stringify(data));
                        } else {
                            $("#create-rolemanagement").submit();
                        }
                            
                    },
                    error: function(data){  
                        signupClicked = false;
                    }
            });
        }
    });
    $("#create-rolemanagement input[id='stocktype']:radio").change(function() {
        stocktype_radio_value = this.value;
        if (stocktype_radio_value == 'modify'){
            $("#create-rolemanagement #view-stock-type").show();
        } else {
            $("#create-rolemanagement #view-stock-type").hide();
            $("#create-rolemanagement input[name='rolepermission[stocktypelist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createstocktype]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updatestocktype]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deletestocktype]']:radio").filter("[value='none']").prop("checked",true);
        }
    });
    $("#create-rolemanagement input[id='rolemanagement']:radio").change(function() {
        rolemanagement_radio_value = this.value;
        if (rolemanagement_radio_value == 'modify'){
            $("#create-rolemanagement #view-role-management").show();
        } else {
            $("#create-rolemanagement #view-role-management").hide();
            $("#create-rolemanagement input[name='rolepermission[rolemanagementlist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createrolemanagement]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updaterolemanagement]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deleterolemanagement]']:radio").filter("[value='none']").prop("checked",true);
        }
    });
    $("#create-rolemanagement input[id='employee']:radio").change(function() {
        employee_radio_value = this.value;
        if (employee_radio_value == 'modify'){
            $("#create-rolemanagement #view-employee").show();
        } else {
            $("#create-rolemanagement #view-employee").hide();
            $("#create-rolemanagement input[name='rolepermission[employeelist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updateemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deleteemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[employeegrouplist]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[createemployeegroup]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[updateemployeegroup]']:radio").filter("[value='none']").prop("checked",true);
            $("#create-rolemanagement input[name='rolepermission[deleteemployeetype]']:radio").filter("[value='none']").prop("checked",true);
        }
    });
    
    employee
    </script>
    <!-- /page script -->