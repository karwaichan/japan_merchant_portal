                        <section class="panel">
                            <header class="panel-heading text-center">
                                <h4>Update Role Id = <?= $role->id ?></h4>
                            </header>
                            <div class="panel-body">
                                <form id="update-rolemanagement" role="form" method="post" class="parsley-form form-horizontal" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Role name</label>

                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="role" data-parsley-required="true" data-parsley-trigger="change" placeholder="Role name" value="<?= $role->role ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <label>Select the options below to grant access to the required modules</label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Employee</label>

                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[employee]" id="employee" <?= ($rolepermission['employee'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[employee]" id="employee" <?= ($rolepermission['employee'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[employee]" id="employee" <?= ($rolepermission['employee'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-bordered" id="view-employee" style="display: <?= ($rolepermission['employee'] == "modify") ? '' : 'none' ?>" >
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Employee List</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeelist]" id="employeelist" <?= ($rolepermission['employeelist'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeelist]" id="employeelist" <?= ($rolepermission['employeelist'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Employee</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployee]" id="createemployee" <?= ($rolepermission['createemployee'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployee]" id="createemployee" <?= ($rolepermission['createemployee'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Employee</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployee]" id="updateemployee" <?= ($rolepermission['updateemployee'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployee]" id="updateemployee" <?= ($rolepermission['updateemployee'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Employee</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployee]" id="deleteemployee" <?= ($rolepermission['deleteemployee'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployee]" id="deleteemployee" <?= ($rolepermission['deleteemployee'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Employee Type List</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeegrouplist]" id="employeegrouplist" <?= ($rolepermission['employeegrouplist'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[employeegrouplist]" id="employeegrouplist" <?= ($rolepermission['employeegrouplist'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Employee Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployeegroup]" id="createemployeegroup" <?= ($rolepermission['createemployeegroup'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createemployeegroup]" id="createemployeegroup" <?= ($rolepermission['createemployeegroup'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Employee Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployeegroup]" id="updateemployeegroup" <?= ($rolepermission['updateemployeegroup'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updateemployeegroup]" id="updateemployeegroup" <?= ($rolepermission['updateemployeegroup'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Employee Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployeetype]" id="deleteemployeetype" <?= ($rolepermission['deleteemployeetype'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleteemployeetype]" id="deleteemployeetype" <?= ($rolepermission['deleteemployeetype'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Stock Type</label>

                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[stocktype]" id="stocktype" <?= ($rolepermission['stocktype'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[stocktype]" id="stocktype" <?= ($rolepermission['stocktype'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[stocktype]" id="stocktype" <?= ($rolepermission['stocktype'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-bordered" id="view-stock-type" style="display: <?= ($rolepermission['stocktype'] == "modify") ? '' : 'none' ?>" >
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[stocktypelist]" id="stocktypelist" <?= ($rolepermission['stocktypelist'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[stocktypelist]" id="stocktypelist" <?= ($rolepermission['stocktypelist'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createstocktype]" id="createstocktype" <?= ($rolepermission['createstocktype'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createstocktype]" id="createstocktype" <?= ($rolepermission['createstocktype'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updatestocktype]" id="updatestocktype" <?= ($rolepermission['updatestocktype'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updatestocktype]" id="updatestocktype" <?= ($rolepermission['updatestocktype'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Stock Type</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deletestocktype]" id="deletestocktype" <?= ($rolepermission['deletestocktype'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deletestocktype]" id="deletestocktype" <?= ($rolepermission['deletestocktype'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Role Management</label>

                                                <div class="col-sm-10">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[rolemanagement]" id="rolemanagement" <?= ($rolepermission['rolemanagement'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[rolemanagement]" id="rolemanagement" <?= ($rolepermission['rolemanagement'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="rolepermission[rolemanagement]" id="rolemanagement" <?= ($rolepermission['rolemanagement'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-bordered" id="view-role-management" style="display: <?= ($rolepermission['rolemanagement'] == "modify") ? '' : 'none' ?>" >
                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">View Role Management List</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[rolemanagementlist]" id="rolemanagementlist" <?= ($rolepermission['rolemanagementlist'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[rolemanagementlist]" id="rolemanagementlist" <?= ($rolepermission['rolemanagementlist'] == "read") ? 'checked' : '' ?> value="read"/> Read Only
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Create Role Management</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createrolemanagement]" id="createrolemanagement" <?= ($rolepermission['createrolemanagement'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[createrolemanagement]" id="createrolemanagement" <?= ($rolepermission['createrolemanagement'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Update Role Management</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updaterolemanagement]" id="updaterolemanagement" <?= ($rolepermission['updaterolemanagement'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[updaterolemanagement]" id="updaterolemanagement" <?= ($rolepermission['updaterolemanagement'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-2 control-label">Delete Role Management</label>
                                                    <div class="col-sm-8 ">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleterolemanagement]" id="deleterolemanagement" <?= ($rolepermission['deleterolemanagement'] == "none") ? 'checked' : '' ?> value="none"/> No Access
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="rolepermission[deleterolemanagement]" id="deleterolemanagement" <?= ($rolepermission['deleterolemanagement'] == "modify") ? 'checked' : '' ?> value="modify"/> Modify
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label></label>
                                                <div>
                                                    <input class="btn btn-primary btn-lg btn-parsley" type="reset" value="Reset" />
                                                    <input id="signupForm" class="btn btn-primary btn-lg btn-parsley" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->

                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>
    </div>

    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/pickers_date.js"></script>
    <script src="<?= base_url(); ?>asset/js/form-custom_blue.js"></script>
    <script>
    var signupClicked = false;
    $("#update-rolemanagement #signupForm").click(function(e) {
        e.preventDefault();
        var employee_permission = $('#update-rolemanagement input[name="rolepermission[employee]"]:checked').val();
        if (employee_permission != 'modify'){
            $("#update-rolemanagement input[name='rolepermission[employeelist]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updateemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deleteemployee]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[employeegrouplist]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createemployeegroup]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updateemployeegroup]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deleteemployeetype]']:radio").filter("[value='none']").prop("checked",true);
        }
        var stocktype_permission = $('#update-rolemanagement input[name="rolepermission[stocktype]"]:checked').val();
        if (stocktype_permission != 'modify'){
            $("#update-rolemanagement input[name='rolepermission[stocktypelist]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createstocktype]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updatestocktype]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deletestocktype]']:radio").filter("[value='none']").prop("checked",true);
        }
        var rolemanagement_permission = $('#update-rolemanagement input[name="rolepermission[rolemanagement]"]:checked').val();
        if (rolemanagement_permission != 'modify'){
            $("#update-rolemanagement input[name='rolepermission[rolemanagementlist]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createrolemanagement]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updaterolemanagement]']:radio").filter("[value='none']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deleterolemanagement]']:radio").filter("[value='none']").prop("checked",true);
        }
        $("#update-rolemanagement").submit();
    });
    $("#update-rolemanagement input[id='employee']:radio").change(function() {
        stocktype_radio_value = this.value;
        if (stocktype_radio_value == 'modify'){
            $("#update-rolemanagement #view-employee").show();
        } else {
            $("#update-rolemanagement #view-employee").hide();
            $("#update-rolemanagement input[name='rolepermission[employeelist]']:radio").filter("[value='<?= $rolepermission['employeelist'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createemployee]']:radio").filter("[value='<?= $rolepermission['createemployee'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updateemployee]']:radio").filter("[value='<?= $rolepermission['updateemployee'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deleteemployee]']:radio").filter("[value='<?= $rolepermission['deleteemployee'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[employeegrouplist]']:radio").filter("[value='<?= $rolepermission['employeegrouplist'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createemployeegroup]']:radio").filter("[value='<?= $rolepermission['createemployeegroup'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updateemployeegroup]']:radio").filter("[value='<?= $rolepermission['updateemployeegroup'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deleteemployeetype]']:radio").filter("[value='<?= $rolepermission['deleteemployeetype'] ?>']").prop("checked",true);
        }
    });
    $("#update-rolemanagement input[id='stocktype']:radio").change(function() {
        stocktype_radio_value = this.value;
        if (stocktype_radio_value == 'modify'){
            $("#update-rolemanagement #view-stock-type").show();
        } else {
            $("#update-rolemanagement #view-stock-type").hide();
            $("#update-rolemanagement input[name='rolepermission[stocktypelist]']:radio").filter("[value='<?= $rolepermission['stocktypelist'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createstocktype]']:radio").filter("[value='<?= $rolepermission['createstocktype'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updatestocktype]']:radio").filter("[value='<?= $rolepermission['updatestocktype'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deletestocktype]']:radio").filter("[value='<?= $rolepermission['deletestocktype'] ?>']").prop("checked",true);
        }
    });
    $("#update-rolemanagement input[id='rolemanagement']:radio").change(function() {
        rolemanagement_radio_value = this.value;
        if (rolemanagement_radio_value == 'modify'){
            $("#update-rolemanagement #view-role-management").show();
        } else {
            $("#update-rolemanagement #view-role-management").hide();
            $("#update-rolemanagement input[name='rolepermission[rolemanagementlist]']:radio").filter("[value='<?= $rolepermission['rolemanagementlist'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[createrolemanagement]']:radio").filter("[value='<?= $rolepermission['createrolemanagement'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[updaterolemanagement]']:radio").filter("[value='<?= $rolepermission['updaterolemanagement'] ?>']").prop("checked",true);
            $("#update-rolemanagement input[name='rolepermission[deleterolemanagement]']:radio").filter("[value='<?= $rolepermission['deleterolemanagement'] ?>']").prop("checked",true);
        }
    });
    </script>
    <!-- /page script -->