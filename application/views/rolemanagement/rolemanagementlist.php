                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <h4 class="text-center">List of Roles</h4>
                            </header>
                            <header class="panel-heading">
                                <a class="btn btn-primary btn-heading" href="<?= base_url(); ?>rolemanagement/createrolemanagement"><i class="fa fa-plus"></i> Create New Role</a>
                            </header>
                            <div class="panel-body">

                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped mg-t datatable editable-datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Role</th>
                                                <th>Role Permission</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($roles) && !empty($roles)): ?>
                                                <?php foreach($roles as $role): ?>
                                                    <tr class="delete-role">
                                                        <td><?= $role->id ?></td>
                                                        <td><?= $role->role ?></td>
                                                        <td>
                                                            <?php foreach($role->rolepermissions as $key => $value): ?>
                                                                <span class="text-primary"><?= $key ?></span>: <?= $value ?><br/>
                                                            <?php endforeach;?>
                                                        </td>
                                                        <td>
                                                            <a href="<?= base_url(); ?>rolemanagement/updaterolemanagement/<?= $role->id ?>" class="edit"><i class="fa fa-edit"> Edit </i></a>
                                                            <a href="javascript:;" class="delete" id="<?= $role->id ?>"><i class="fa fa-minus"> Delete </i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /inner content wrapper -->
                </div>
                <!-- /content wrapper -->
                <a class="exit-offscreen"></a>
            </section>
            <!-- /main content -->
        </section>

    </div>
    
    
    <!-- page script -->
    <script src="<?= base_url(); ?>asset/js/table-edit.js"></script>
    <script>
    $(".delete-role .delete").click(function(e) {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure to delete user id = "+id+"?", function(result) {
            if (result == true){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url(); ?>rolemanagement/deleterolemanagement',
                    data: 'id='+id,
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "true"){
                            bootbox.alert('User id = '+id+' has been deleted successfully');
                        } else {
                            bootbox.alert('User id = '+id+' can\'t deleted');
                            location.reload();
                        }
                    },
                    error: function(data){}
                });
            } else {
                location.reload();
            }
        });
    });
    </script>
    <!-- /page script -->