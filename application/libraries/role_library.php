<?php

class Role_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('role_model');
    }

    function getRoleArray($list = array()){
        $categories = $this->CI->role_model->getAll();
        if ($categories){
            foreach ($categories as $category){
                $list[$category->id] = $category->name;
            }
        }
        return $list;
    }

    function getCategoryidArray($list = array()){
        $categories = $this->CI->role_model->getAll();
        if (!$categories){
            $data_category['name'] = USERTYPE_TOURIST;
            $result = $this->CI->category_model->create($data_category);
            $categories = $this->CI->category_model->getAll();
        }
        foreach ($categories as $category){
            $list[$category->id] = 0;
        }
        return $list;
    }
  
}
