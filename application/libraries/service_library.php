<?php

class Service_library extends CI_Controller {
  
    public function __construct() {
        parent::__construct();
        $this->load->library('ws_curl_library');
    }

    function login($username, $password){
        $url = WEBSERVICE_URL.'member/merchant-login';
        $data_input['username'] = $username;
        $data_input['password'] = $password;
        $data_input['version'] = 999;
        $data_input['device'] = '1';
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function testsoap(){
        $url = WEBSERVICE_URL.'system/demo';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function getUserProfile(){
        $url = WEBSERVICE_URL.'merchant/checkMerchantByAuth';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        if ($user_result)
            return $user_result;
        redirect(base_url()."home/logout");
    }
    
    function getCashierById($data_input){
        $url = WEBSERVICE_URL.'merchant/account/getCashierById';
        $data_input['user_token'] = $this->session->userdata('user_token');
        return $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
    }
    
    function deleteCashierById($data_input){
        $url = WEBSERVICE_URL.'cashier/deleteCashier';
        $data_input['user_token'] = $this->session->userdata('user_token');
        return $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
    }
    
    function getQrcode($data_input){
        $url = WEBSERVICE_URL.'merchant/getQrcode';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function getReceiptList($input = []){
        $url = WEBSERVICE_URL.'etrs/getListEtrsForMerchantJPN';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $sentData = array_merge($data_input, $input);
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $sentData);
        return $user_result;
    }
    
    function getReceiptById($data_input){
        $url = WEBSERVICE_URL.'etrs/getEtrsTicketByDocIdJPN';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function updateMerchantProfileById($id, $data_input){
        $url = WEBSERVICE_URL.'cashier/updateMerchantProfileById';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function createCashier($data_input){
        $url = WEBSERVICE_URL.'merchant/account/createCashier';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function updateOutletById($id, $data_input){
        $url = WEBSERVICE_URL.'outlet/saveOutletById';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function updateOutletCategoryById($id, $data_input){
        $url = WEBSERVICE_URL.'outlet/updateOutletCategoryById';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }
    
    function getOulets($merchant_id){
        $url = WEBSERVICE_URL.'apioutlet/searchOutlet';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $data_input['merchant_id'] = $merchant_id;
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        $outlets = array();
        if ($result){
            foreach ($result as $outlet){
                $outlets[$outlet->id] = $outlet->name;
            }
        }
        return $outlets;
    }
    
    function getTrackingDeviceList($outlet_id){
        $url = WEBSERVICE_URL2.'member/getCustomersByOutletid';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $data_input['outlet_id'] = $outlet_id;
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $result;
    }
    
    function getTripsByPeriod($data_input){
        $url = WEBSERVICE_URL2.'tracking/getTripsByPeriod';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $result;
    }
    
    function getCategories(){
        $url = WEBSERVICE_URL.'category/categories';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $header['country'] = $this->session->userdata('country');
        $result = $this->ws_curl_library->wsDecodedGetCall($url, $data_input, $header);
        $categories = array();
        if ($result){
            foreach ($result as $outlet){
                $categories[$outlet->id] = $outlet->name;
            }
        }
        return $categories;
    }
    
    function getCategoryByOutletId($outlet_id){
        $url = WEBSERVICE_URL.'outlet/getCategoryByOutletId';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $data_input['outlet_id'] = $outlet_id;
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $result;
    }
    
    function getCategoryByMerchantId($merchant_id){
        $url = WEBSERVICE_URL.'outlet/getCategoryByMerchantId';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $data_input['merchant_id'] = $merchant_id;
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $result;
    }
    
    function getNewsEventByCountry($country, $per_page = 0){
        $url = WEBSERVICE_URL.'article/getByCountry';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $data_input['country'] = $country;
        if ($per_page){
            $data_input['per_page'] = $per_page;
        }
            
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $result;
    }
    
    function getNewsEventById($id){
        $url = WEBSERVICE_URL.'article/getById';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $data_input['id'] = $id;
            
        $result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $result;
    }
    
    function getEmployeeList(){
        $url = WEBSERVICE_URL.'merchant/account/getCashierByOutletid';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        return $user_result;
    }

	public function getSalesAmountForMerchant() {
		$url = 'etrs/getListEtrsSummaryForMerchant';
		$input['user_token'] = $this->session->userdata('user_token');
		return $this->ws_curl_library->wsDecodedPostCall(WEBSERVICE_URL . $url, $input);
	}
}
