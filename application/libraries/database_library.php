<?php

class Database_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->dbforge();
    }

    function createResellerDB($database_name){
        if ($this->CI->dbforge->create_database($database_name)){
            return true;
        }
        return false;
    }
    
    function createResellertables($database_name){
        if (!$this->createStocktypetable($database_name)) return false;
        if (!$this->createRolestable($database_name)) return false;
        if (!$this->createRolepermissionstable($database_name)) return false;
        if (!$this->createEmployeegroupstable($database_name)) return false;
        if (!$this->createEmployeestable($database_name)) return false;
        if (!$this->createCurrenciestable($database_name)) return false;
        if (!$this->createPaymentmethodstable($database_name)) return false;
        if (!$this->createSettingstable($database_name)) return false;
        if (!$this->createSupplierstable($database_name)) return false;
        if (!$this->createRetailerstable($database_name)) return false;
        if (!$this->createCategoriestable($database_name)) return false;
        if (!$this->createProducttypestable($database_name)) return false;
        if (!$this->createSaleitemstable($database_name)) return false;
        if (!$this->createSaleorderstable($database_name)) return false;
        if (!$this->createSalepaymentstable($database_name)) return false;
        if (!$this->createStockstable($database_name)) return false;
        if (!$this->createStocktakeitemstable($database_name)) return false;
        if (!$this->createStocktakestable($database_name)) return false;
        if (!$this->createStocktake_temptable($database_name)) return false;
        if (!$this->createReturnorderstable($database_name)) return false;
        if (!$this->createReturnitemstable($database_name)) return false;
        if (!$this->createReturnpaymentstable($database_name)) return false;
        if (!$this->createRepairorderstable($database_name)) return false;
        if (!$this->createRepairitemstable($database_name)) return false;
        if (!$this->createRepairpaymentstable($database_name)) return false;
        if (!$this->createPurchasereturnstable($database_name)) return false;
        if (!$this->createPurchasereturnitemstable($database_name)) return false;
        
        return true;
    }
    
    function createStocktypetable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `stocktypes` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `category` varchar(20) DEFAULT NULL,
            `producttype` varchar(30) DEFAULT NULL,
            `brand` varchar(50) DEFAULT NULL,
            `model` varchar(255) DEFAULT NULL,
            `color` varchar(20) DEFAULT NULL,
            `low_stock_levels` int(20) NOT NULL,
            `cost_price` float(9,2) DEFAULT NULL,
            `transfer_price` float(9,2) DEFAULT NULL,
            `del` enum('0','1') DEFAULT '0',
            `insertiondate` int(16) DEFAULT NULL,
            `updationdate` int(16) DEFAULT NULL,
            `bonus` float(9,2) DEFAULT '0.00',
            `stock_type_image` varchar(100) DEFAULT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createRolestable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `roles` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `role` varchar(30) DEFAULT NULL,
            `del` enum('0','1') DEFAULT '0',
            `insertiondate` int(16) DEFAULT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createRolepermissionstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `rolepermissions` (
            `id` int(16) NOT NULL AUTO_INCREMENT,
            `role_id` int(11) DEFAULT NULL,
            `module` varchar(20) DEFAULT NULL,
            `permission` enum('read','modify','none') DEFAULT NULL,
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createEmployeegroupstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `employeegroups` (
            `id` int(5) NOT NULL AUTO_INCREMENT,
            `employeetype` varchar(25) NOT NULL,
            `point` int(5) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createEmployeestable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `employees` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `first_name` varchar(255) NOT NULL,
            `last_name` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `username` varchar(32) NOT NULL,
            `del` enum('0','1') DEFAULT '0',
            `dob` int(12) DEFAULT '0',
            `employeegroup_id` int(4) NOT NULL DEFAULT '0',
            `role_id` int(4) NOT NULL DEFAULT '0',
            `address_1` varchar(255) DEFAULT NULL,
            `address_2` varchar(255) DEFAULT NULL,
            `city` varchar(255) DEFAULT NULL,
            `state` varchar(255) DEFAULT NULL,
            `zip` varchar(255) DEFAULT NULL,
            `country` varchar(255) DEFAULT NULL,
            `phone` varchar(32) NOT NULL,
            `user_id` int(16) NOT NULL DEFAULT '0',
            `point` int(11) DEFAULT '0',
            `gender` varchar(20) DEFAULT NULL,
            `nationality` varchar(100) DEFAULT NULL,
            `employee_photo` varchar(128) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createCurrenciestable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `currencies` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `from` varchar(16) NOT NULL,
            `to` varchar(16) NOT NULL,
            `rate` decimal(24,6) NOT NULL DEFAULT '0.000000',
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createPaymentmethodstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `paymentmethods` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `name` varchar(100) NOT NULL,
            `status` enum('0','1') DEFAULT '0',
            `insertiondate` int(16) NOT NULL,
            `updationdate` int(16) NOT NULL,
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createSettingstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `settings` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `name` varchar(16) NOT NULL,
            `type` varchar(16) NOT NULL,
            `value` varchar(128) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createSupplierstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `suppliers` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `first_name` varchar(255) NOT NULL,
            `last_name` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `del` enum('0','1') DEFAULT '0',
            `address_1` varchar(255) DEFAULT NULL,
            `address_2` varchar(255) DEFAULT NULL,
            `city` varchar(255) DEFAULT NULL,
            `state` varchar(255) DEFAULT NULL,
            `zip` varchar(255) DEFAULT NULL,
            `country` varchar(255) DEFAULT NULL,
            `currency` varchar(16) NOT NULL DEFAULT 'SGD',
            `phone` varchar(32) NOT NULL,
            `photo` varchar(128) NOT NULL,
            `company_name` varchar(128) NOT NULL,
            `account_number` varchar(32) NOT NULL,
            `bank_name` varchar(128) NOT NULL,
            `website_url` varchar(128) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createRetailerstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `retailers` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `defined_id` varchar(16) DEFAULT NULL,
            `usertype_id` varchar(16) DEFAULT NULL,
            `outlet_name` varchar(16) DEFAULT NULL,
            `first_name` varchar(255) NOT NULL,
            `last_name` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `del` enum('0','1') DEFAULT '0',
            `address_1` varchar(255) DEFAULT NULL,
            `address_2` varchar(255) DEFAULT NULL,
            `city` varchar(255) DEFAULT NULL,
            `state` varchar(255) DEFAULT NULL,
            `zip` varchar(255) DEFAULT NULL,
            `country` varchar(255) DEFAULT NULL,
            `currency` varchar(16) NOT NULL DEFAULT 'SGD',
            `phone` varchar(32) NOT NULL,
            `photo` varchar(128) NOT NULL,
            `company_name` varchar(128) NOT NULL,
            `account_number` varchar(32) NOT NULL,
            `bank_name` varchar(128) NOT NULL,
            `website_url` varchar(128) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    //26/09/2014
    function createCategoriestable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `categories` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createProducttypestable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `producttypes` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createSaleitemstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `saleitems` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `stock_id` int(11) NOT NULL DEFAULT '0',
            `saleorder_id` int(11) NOT NULL DEFAULT '0',
            `retailer_id` int(11) NOT NULL DEFAULT '0',
            `quantity_purchased` int(11) DEFAULT '0',
            `cost_price` double(15,2) DEFAULT '0.00',
            `unit_price` double(15,2) DEFAULT '0.00',
            `discount` varchar(11) DEFAULT '0',
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createSaleorderstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `saleorders` (
            `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
            `employee_id` int(16) NOT NULL DEFAULT '0',
            `retailer_id` int(10) NOT NULL DEFAULT '0',
            `transaction_date` int(16) NOT NULL DEFAULT '0',
            `total_amount` double(15,2) DEFAULT '0.00',
            `total_discount` double(15,2) DEFAULT '0.00',
            `total_tax` double(15,2) DEFAULT '0.00',
            `total_cost` double(15,2) DEFAULT '0.00',
            `stock_cost` double(15,2) DEFAULT '0.00',
            `payment_amount` double(15,2) DEFAULT '0.00',
            `total_due` double(15,2) DEFAULT '0.00',
            `comment` varchar(500) DEFAULT NULL,
            `order_type` enum('sell','exchange','return') NOT NULL DEFAULT 'sell',
            `delivery_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `payment_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `order_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) DEFAULT NULL,
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createSalepaymentstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `salepayments` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `saleorder_id` int(11) NOT NULL,
            `paymentmethod_id` int(11) DEFAULT NULL,
            `payment_amount` double(15,2) DEFAULT '0.00',
            `comment` varchar(500) DEFAULT NULL,
            `transaction_date` int(16) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    function createStockstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `stocks` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `stocktype_id` int(11) NOT NULL,
            `cost_price` double NOT NULL,
            `selling_price` float NOT NULL,
            `transfer_price` float NOT NULL,
            `bonus` int(11) NOT NULL DEFAULT '0',
            `shortcut` varchar(1) NOT NULL DEFAULT 'n',
            `name` varchar(255) DEFAULT '',
            `supplier_id` int(11) DEFAULT '0',
            `description` varchar(255) NOT NULL,
            `unit_price` double NOT NULL,
            `quantity` double NOT NULL DEFAULT '0',
            `del` int(1) DEFAULT '0',
            `barcode` varchar(50) NOT NULL DEFAULT '0',
            `featuredproduct` enum('0','1') DEFAULT '0',
            `invoice_id` varchar(30) NOT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    
    //17/10/2014
    function createStocktakeitemstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `stocktakeitems` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `stocktake_id` int(11) DEFAULT NULL,
            `stock_id` int(11) DEFAULT NULL,
            `currentqty` int(11) DEFAULT NULL,
            `countqty` int(11) DEFAULT NULL,
            `remarks` varchar(255) DEFAULT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createStocktakestable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `stocktakes` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `category` varchar(20) NOT NULL,
            `producttype` varchar(50) NOT NULL,
            `employee_id` int(20) DEFAULT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createStocktake_temptable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `stocktake_temp` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `category` varchar(20) NOT NULL,
            `producttype` varchar(50) NOT NULL,
            `employee_id` int(20) DEFAULT NULL,
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) NOT NULL DEFAULT '0',
            `del` enum('0','1') DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createReturnorderstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `returnorders` (
            `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
            `employee_id` int(16) NOT NULL DEFAULT '0',
            `retailer_id` int(10) NOT NULL DEFAULT '0',
            `transaction_date` int(16) NOT NULL DEFAULT '0',
            `total_amount` double(15,2) DEFAULT '0.00',
            `total_discount` double(15,2) DEFAULT '0.00',
            `total_tax` double(15,2) DEFAULT '0.00',
            `service_charge` double DEFAULT '0',
            `total_cost` double(15,2) DEFAULT '0.00',
            `payment_amount` double(15,2) DEFAULT '0.00',
            `total_due` double(15,2) DEFAULT '0.00',
            `comment` varchar(500) DEFAULT NULL,
            `order_type` enum('repair','exchange','return') NOT NULL DEFAULT 'return',
            `payment_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `order_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) DEFAULT NULL,
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createReturnitemstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `returnitems` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `stock_id` int(11) NOT NULL DEFAULT '0',
            `returnorder_id` int(11) NOT NULL DEFAULT '0',
            `quantity_purchased` int(11) DEFAULT '0',
            `cost_price` double(15,2) DEFAULT '0.00',
            `unit_price` double(15,2) DEFAULT '0.00',
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createReturnpaymentstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `returnpayments` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `returnorder_id` int(11) NOT NULL,
            `paymentmethod_id` int(11) DEFAULT NULL,
            `payment_amount` double(15,2) DEFAULT '0.00',
            `comment` varchar(500) DEFAULT NULL,
            `transaction_date` int(16) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createRepairorderstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `repairorders` (
            `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
            `employee_id` int(16) NOT NULL DEFAULT '0',
            `retailer_id` int(10) NOT NULL DEFAULT '0',
            `transaction_date` int(16) NOT NULL DEFAULT '0',
            `total_amount` double(15,2) DEFAULT '0.00',
            `total_discount` double(15,2) DEFAULT '0.00',
            `total_tax` double(15,2) DEFAULT '0.00',
            `service_charge` double NOT NULL DEFAULT '0',
            `total_cost` double(15,2) DEFAULT '0.00',
            `payment_amount` double(15,2) DEFAULT '0.00',
            `total_due` double(15,2) DEFAULT '0.00',
            `comment` varchar(500) DEFAULT NULL,
            `order_type` enum('repair','exchange','return') NOT NULL DEFAULT 'repair',
            `payment_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `order_status` enum('pending','completed','cancelled','voided') DEFAULT 'pending',
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) DEFAULT NULL,
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createRepairitemstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `repairitems` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `stock_id` int(11) NOT NULL DEFAULT '0',
            `repairorder_id` int(11) NOT NULL DEFAULT '0',
            `quantity_purchased` int(11) DEFAULT '0',
            `cost_price` double(15,2) DEFAULT '0.00',
            `unit_price` double(15,2) DEFAULT '0.00',
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createRepairpaymentstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `repairpayments` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `repairorder_id` int(11) NOT NULL,
            `paymentmethod_id` int(11) DEFAULT NULL,
            `payment_amount` double(15,2) DEFAULT '0.00',
            `comment` varchar(500) DEFAULT NULL,
            `transaction_date` int(16) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createPurchasereturnstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `purchasereturns` (
            `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
            `employee_id` int(16) NOT NULL DEFAULT '0',
            `comment` varchar(500) DEFAULT NULL,
            `status` enum('pending','completed') DEFAULT 'pending',
            `insertiondate` int(16) NOT NULL DEFAULT '0',
            `updationdate` int(16) DEFAULT NULL,
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
    function createPurchasereturnitemstable($database_name){
        $this->db = $this->CI->user_model->db_changer($database_name);
        $query = "CREATE TABLE `purchasereturnitems` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `stock_id` int(11) NOT NULL DEFAULT '0',
            `purchasereturn_id` int(11) NOT NULL DEFAULT '0',
            `supplier_id` int(11) NOT NULL DEFAULT '0',
            `quantity_purchased` int(11) NOT NULL DEFAULT '0',
            `cost_price` double NOT NULL DEFAULT '0',
            `del` enum('0','1') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        return $this->db->query ($query);
    }
  
}
