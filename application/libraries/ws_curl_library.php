<?php
class Ws_curl_library{
  private $CI; 
  
  public function __construct() {
    $this->CI = & get_instance();
  }
  
  public function wsPostCall($url, $data_string){
//    $data_string = json_encode($data);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//       'Content-Type: application/json',
//       'Content-Length: ' . strlen($data_string))
//    );

    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $contenttype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
    curl_close($ch);
//    $result = json_decode($result);
    return $result;
  }
  
  public function wsDecodedPostCall($url, $data_string){
    $ch = curl_init($url);
    if (isset($data_string['user_token'])){
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'user-token: '.$data_string['user_token'],
            'Accept: application/json',
            'country: JPN'
        ));
    }
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $contenttype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
    curl_close($ch);
    $result = json_decode($result);
    if ($result && $result->error){
        return false;
    }
    return $result->data;
  }
  
  public function wsDecodedGetCall($url, $data_string, $header = null){
    $ch = curl_init($url);
    $header_request = array('Accept: application/json');
    if ($header){
        foreach ($header as $key => $value){
            $header_request[] = $key.': '.$value;
        }
    }
    if (isset($data_string['user_token'])){
        $header_request[] = 'user-token: '.$data_string['user_token'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header_request);
    }
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $contenttype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
    curl_close($ch);
    $result = json_decode($result);
    if ($result && isset($result->error) && $result->error){
        return false;
    }
    return $result->data;
  }
}
?>
