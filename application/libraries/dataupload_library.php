<?php

class Dataupload_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
    }

    function uploadImage($server_folder, $input_variable){
        //upload file
        $config['upload_path'] = $server_folder;
        $config['allowed_types'] = '*';
        $config['max_size']  = '5000000';
        if(!is_dir($config['upload_path'])){
          if(!mkdir($config['upload_path'], 0757, TRUE)){
            $this->CI->session->set_flashdata('error', 'Error mkdir. Check folder permission.');
            die();
          }
        }
        $this->CI->load->library('upload', $config);
        if ($this->CI->upload->do_upload($input_variable)){
          return $this->CI->upload->data();
        }
        return false;
    }
  
}
