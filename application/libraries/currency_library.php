<?php

class Currency_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('country_model');
    }

    function getCurrencyTypeArray(){
        $list = array();
        $currency_types = $this->CI->country_model->getAll();
        foreach ($currency_types as $currency_type){
            $list[$currency_type->currency_code] = $currency_type->currency_code;
        }
        ksort($list);
        return $list;
    }

    function validateExistingCurrencyPair($currency){
        $base_currency = $this->CI->setting_model->getByType(SETTING_BASE_CURRENCY);
        if (!$base_currency){
            $this->CI->session->set_flashdata('error', 'Please set base currency before continue!');
            redirect('currency/currencybase', 'refresh');
        }
        if ($currency == $base_currency[0]->value){
            return true;
        }
        $check = $this->CI->currency_model->getByFromTo($base_currency[0]->value, $currency);
        if(!$check){
            $data['from'] = $base_currency[0]->value;
            $data['to'] = $currency;
            $data['rate'] = 1.00;
            return $this->CI->currency_model->create($data);
        }
        return $check;
    }
  
}
