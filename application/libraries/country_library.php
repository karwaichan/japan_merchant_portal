<?php

class Country_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('country_model');
    }

    function getCountryArray(){
        $list = array();
        $countries = $this->CI->country_model->getAll();
        foreach ($countries as $country){
            $list[$country->id] = $country->country_name;
        }
        return $list;
    }
  
}
