<?php

class Importexport_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('category_model');
    }

    function convertToStock($stock_import_file){
        $this->CI->load->library('parseCSV');
        $csv = new parseCSV($stock_import_file);
        $stock_array = $csv->data;
        $result_stock = false;
        foreach ($stock_array as $key => $value){
            if (sizeof($value) == 15 && $value[1] != "Category" && $value[1]){
                $brand = $value[3];
                $model = $value[4];
                $color = $value[5];
                
                //get category
                $category = $this->CI->category_model->getByName($value[1]);
                if ($category){
                    $category_id = $category->id;
                } else {
                    //create new category
                    $data_category['name'] = $value[1];
                    $category_id = $this->CI->category_model->create($data_category);
                }
                
                //get producttype
                $producttype = $this->CI->producttype_model->getByName($value[2]);
                if ($producttype){
                    $producttype_id = $producttype->id;
                } else {
                    //create new producttype
                    $data_producttype['name'] = $value[2];
                    $producttype_id = $this->CI->producttype_model->create($data_producttype);
                }
                
                //get stocktype
                if ($category_id && $producttype_id){
                    $check_stocktype = $this->CI->stocktype_library->getByFields($category_id, $producttype_id, $brand, $model, $color);
                    if ($check_stocktype){
                        $stocktype_id = $check_stocktype->id;
                    } else {
                        //create new stocktype
                        $data_stocktype['category'] = $category_id;
                        $data_stocktype['producttype'] = $producttype_id;
                        $data_stocktype['brand'] = $brand;
                        $data_stocktype['model'] = $model;
                        $data_stocktype['color'] = $color;
                        $data_stocktype['low_stock_levels'] = 0;
                        $stocktype_id = $this->CI->stocktype_model->create($data_stocktype);
                    }
                } else return false;
                
                $cost_price = $value[8];
                $selling_price = $value[9];
                $transfer_price = $value[10];
                $quantity = $value[11];
                $barcode = $value[7];
                $bonus = $value[6];
                $shortcut = $value[12];
                
                //get supplier
                $supplier = $this->CI->supplier_model->getByCompanyname(STANDARD_SUPPLIER);
                if ($supplier){
                    $supplier_id = $supplier->id;
                } else {
                    //create new supplier
                    $data_supplier['company_name'] = STANDARD_SUPPLIER;
                    $supplier_id = $this->CI->supplier_model->create($data_supplier);
                }
                
                //get stock
                $check_stock = $this->CI->stock_model->getByFields($stocktype_id, $supplier_id, $cost_price, $selling_price, $transfer_price, $barcode);
                if ($check_stock){
                    $data_stock_update['quantity'] = $check_stock->quantity+$quantity;
                    $result_stock = $this->CI->stock_model->updateById($check_stock->id, $data_stock_update);
                } else {
                    //create new stock
                    $data_stock = array();
                    $data_stock['cost_price'] = $cost_price;
                    $data_stock['selling_price'] = $selling_price;
                    $data_stock['transfer_price'] = $transfer_price;
                    $data_stock['unit_price'] = $selling_price;
                    $data_stock['stocktype_id'] = $stocktype_id;
                    $data_stock['quantity'] = $quantity;
                    $data_stock['barcode'] = $barcode;
                    $data_stock['supplier_id'] = $supplier_id;
                    $data_stock['bonus'] = $bonus;
                    $data_stock['shortcut'] = $shortcut;
                    $result_stock = $this->CI->stock_model->create($data_stock);
                }
            }
        }
        return ($result_stock) ? true : false;
    }

    function convertStockToFile($file_name, $file_type){
        $stocktypes = $this->CI->stocktype_library->getStocktypeArrayObject();
        $stocks = $this->CI->stock_model->getAllValid();
        $data = array();
        
		$data[] = array('Stock Listing');
		$data[] = array();
		$data[] = array('All Outlet');
		$data[] = array();
		$data[] = array('Stock type id','Category','Product Type','Brand','Model','Color','Bonus','IMEI/Barcode','Cost Price','Selling Price','Transfer Price','Quantity','Shortcut','Remaining','Update_outlet');
        foreach ($stocks as $stock){
            $row = array();
            $row['id'] = $stock->stocktype_id;
            $row['category'] = $stocktypes[$stock->stocktype_id]->category;
            $row['producttype'] = $stocktypes[$stock->stocktype_id]->producttype;
            $row['brand'] = $stocktypes[$stock->stocktype_id]->brand;
            $row['model'] = $stocktypes[$stock->stocktype_id]->model;
            $row['color'] = $stocktypes[$stock->stocktype_id]->color;
            $row['bonus'] = $stock->bonus;
            $row['barcode'] = $stock->barcode;
            $row['cost_price'] = $stock->cost_price;
            $row['selling_price'] = $stock->selling_price;
            $row['transfer_price'] = $stock->transfer_price;
            $row['quantity'] = $stock->quantity;
            $row['shortcut'] = $stock->shortcut;
            $row['remaining'] = $stock->quantity;
            $row['update_outlet'] = 'false';
            $data[] = $row;
        }
        
        //file configuration
        $date_conversion = date('jS_F_Y-h_i_s_A_T', time());
		$file_name = $file_name.'_'.$date_conversion.'.'.$file_type;
        $config['upload_path'] = './uploads/reports/';
        $config['allowed_types'] = 'csv';
        $config['max_size']  = '1000';

        if(!is_dir($config['upload_path'])){
          if(!mkdir($config['upload_path'], 0755, TRUE)){
            $this->session->set_flashdata('error', 'Error mkdir. Check folder permission.');
            redirect('stock/stocklist', 'refresh');
          }
        }
        $file_path = $config['upload_path'].$file_name; 
        
        //creating csv
        $this->CI->load->library('parseCSV');
		$csv = new parseCSV();
//		$csvHeader = array('Stock type id','Category','Product Type','Brand','Model','Color','Bonus','IMEI/Barcode','Cost Price','Selling Price','Transfer Price','Quantity','Shortcut','Remaining','Update_outlet');
		$csvHeader = array();
        $result = $csv->save($file_path, $data, false, $csvHeader);
        if ($result) return $file_name;
		return false;
    }

    function convertToRetailer($retailer_import_file){
        $this->CI->load->library('parseCSV');
        $csv = new parseCSV($retailer_import_file);
        $retailer_array = $csv->data;
        $result_retailer = false;
        foreach ($retailer_array as $key => $value){
            if (sizeof($value) == 9 && $value[1] != "first_name" && $value[1]){
                $first_name = $value[1];
                $last_name = $value[2];
                $phone = $value[3];
                $email = $value[4];
                $usertype_id = $value[5];
                $defined_id = $value[6];
                $outlet_name = $value[7];
                
                //get stock
                $check_stock = $this->CI->retailer_model->getByFields($first_name, $last_name, $phone, $email, $defined_id);
                if (!$check_stock){
                    //create new stock
                    $data_stock = array();
                    $data_stock['first_name'] = $first_name;
                    $data_stock['last_name'] = $last_name;
                    $data_stock['phone'] = $phone;
                    $data_stock['email'] = $email;
                    $data_stock['defined_id'] = $defined_id;
                    $data_stock['usertype_id'] = $usertype_id;
                    $data_stock['outlet_name'] = $outlet_name;
                    $result_retailer = $this->CI->retailer_model->create($data_stock);
                }
            }
        }
        return ($result_retailer) ? true : false;
    }

    function convertRetailerToFile($file_type){
        $retailers = $this->CI->retailer_model->getAllValid();
        $data = array();
        
		$data[] = array('Customer Listing');
		$data[] = array('#','first_name','last_name','phone_number','email','usert_ype_id','defined_id','outlet_name');
        foreach ($retailers as $retailer){
            $row = array();
            $row['id'] = $retailer->id;
            $row['first_name'] = $retailer->first_name;
            $row['last_name'] = $retailer->last_name;
            $row['phone_number'] = $retailer->phone;
            $row['email'] = $retailer->email;
            $row['usert_ype_id'] = $retailer->usertype_id;
            $row['defined_id'] = $retailer->defined_id;
            $row['outlet_name'] = $retailer->outlet_name;
            $data[] = $row;
        }
        
        //file configuration
        $date_conversion = date('M-d-Y', strtotime("-1 day", time()));
		$file_name = 'retailer_export_'.$date_conversion.'.'.$file_type;
        $config['upload_path'] = './uploads/retailers/';
        $config['allowed_types'] = 'csv';
        $config['max_size']  = '1000';

        if(!is_dir($config['upload_path'])){
          if(!mkdir($config['upload_path'], 0755, TRUE)){
            $this->session->set_flashdata('error', 'Error mkdir. Check folder permission.');
            redirect('retailer/retailerlist', 'refresh');
          }
        }
        $file_path = $config['upload_path'].$file_name; 
        
        //creating csv
        $this->CI->load->library('parseCSV');
		$csv = new parseCSV();
//		$csvHeader = array('Stock type id','Category','Product Type','Brand','Model','Color','Bonus','IMEI/Barcode','Cost Price','Selling Price','Transfer Price','Quantity','Shortcut','Remaining','Update_outlet');
		$csvHeader = array();
        $result = $csv->save($file_path, $data, false, $csvHeader);
        if ($result) return $file_name;
		return false;
    }

    function convertSummarySaleToExcel($saleorderSummary, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Sale Summary Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array('Transaction Date', 'Count', 'Total Cost', 'Total Amount', 'Profit', 'Profit Percentage (%)', 'Paid Amount', 'Total Discount', 'Due Amount', 'styles62');
        foreach ($saleorderSummary['saleorders'] as $saleorder){
            $exportArray[] = array(
                $saleorder->transaction_date,
                $saleorder->transaction_count,
                $saleorder->stock_cost,
                $saleorder->total_cost,
                $saleorder->profit,
                $saleorder->profit_percentage,
                $saleorder->payment_amount,
                $saleorder->total_discount,
                $saleorder->total_due);
        }
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Transaction Count', $saleorderSummary['summary']['transaction_count']);
        $exportArray[] = array('Total Amount', $saleorderSummary['summary']['total_cost']);
        $exportArray[] = array('Stock Cost', $saleorderSummary['summary']['stock_cost']);
        $exportArray[] = array('Total Due', $saleorderSummary['summary']['total_due']);
        $exportArray[] = array('Total Discount', $saleorderSummary['summary']['total_discount']);
        $exportArray[] = array('Payment Amount', $saleorderSummary['summary']['payment_amount']);
        $exportArray[] = array('Profit', $saleorderSummary['summary']['profit']);
        $exportArray[] = array('Profit Percentage', $saleorderSummary['summary']['profit_percentage']);
        return $exportArray;
    }

    function convertDetailSalesToExcel($saleorderSummary, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Detail Sale Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array('Transaction ID', 'Transaction Date', 'Sold By', 'Sold To', 'Total Cost', 'Total Amount', 'Profit', 'Profit Percentage (%)', 'Paid Amount', 'Total Discount', 'Due Amount', 'styles62');
        foreach ($saleorderSummary['saleorders'] as $saleorder){
            $exportArray[] = array(
                $saleorder->id,
                $saleorder->transaction_date,
                $saleorder->sold_by,
                $saleorder->sold_to,
                $saleorder->total_cost,
                $saleorder->total_cost,
                $saleorder->profit,
                $saleorder->profit_percentage,
                $saleorder->payment_amount,
                $saleorder->total_discount,
                $saleorder->total_due);
            $exportArray[] = array();
            $exportArray[] = array('ID', 'Category', 'Product Type', 'Brand', 'Model', 'Color', 'Barcode', 'Supplier', 'Quantity', 'Total Cost', 'Sale Price', 'Discount', 'styles62');
            foreach ($saleorder->saleitems as $saleitem){
                $exportArray[] = array(
                    $saleitem->id,
                    $saleitem->category,
                    $saleitem->producttype,
                    $saleitem->brand,
                    $saleitem->model,
                    $saleitem->color,
                    $saleitem->barcode,
                    $saleitem->supplier,
                    $saleitem->quantity_purchased,
                    $saleitem->quantity_purchased * $saleitem->cost_price,
                    $saleitem->quantity_purchased * $saleitem->unit_price,
                    $saleitem->discount);
            }
            $exportArray[] = array();
        }
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Transaction Count', $saleorderSummary['summary']['transaction_count']);
        $exportArray[] = array('Total Amount', $saleorderSummary['summary']['total_cost']);
        $exportArray[] = array('Stock Cost', $saleorderSummary['summary']['stock_cost']);
        $exportArray[] = array('Total Due', $saleorderSummary['summary']['total_due']);
        $exportArray[] = array('Total Discount', $saleorderSummary['summary']['total_discount']);
        $exportArray[] = array('Payment Amount', $saleorderSummary['summary']['payment_amount']);
        $exportArray[] = array('Profit', $saleorderSummary['summary']['profit']);
        $exportArray[] = array('Profit Percentage', $saleorderSummary['summary']['profit_percentage']);
        return $exportArray;
    }

    function convertSummaryRetailerToExcel($saleorderSummary, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Retailer Summary Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array();
        $exportArray[] = array('Retailer', 'Transaction Count', 'Total Amount', 'Paid Amount', 'Total Discount', 'Due Amount', 'styles62');
        $retailerArray = $this->CI->retailer_library->getRetailerArray();
        foreach ($saleorderSummary['saleorders'] as $saleorder){
            $exportArray[] = array(
                $retailerArray[$saleorder->retailer_id],
                $saleorder->transaction_count,
                $saleorder->total_cost,
                $saleorder->payment_amount,
                $saleorder->total_discount,
                $saleorder->total_due);
        }
        $exportArray[] = array();
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Transaction Count', $saleorderSummary['summary']['transaction_count']);
        $exportArray[] = array('Total Amount', $saleorderSummary['summary']['total_cost']);
        $exportArray[] = array('Total Due', $saleorderSummary['summary']['total_due']);
        $exportArray[] = array('Total Discount', $saleorderSummary['summary']['total_discount']);
        $exportArray[] = array('Payment Amount', $saleorderSummary['summary']['payment_amount']);
        return $exportArray;
    }

    function convertDetailRetailerToExcel($saleorderSummary, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Detail Retailer Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array('Transaction ID', 'Transaction Date', 'Sold By', 'Total Cost', 'Total Amount', 'Profit', 'Profit Percentage (%)', 'Paid Amount', 'Total Discount', 'Due Amount', 'styles62');
        foreach ($saleorderSummary['saleorders'] as $saleorder){
            $exportArray[] = array(
                $saleorder->id,
                $saleorder->transaction_date,
                $saleorder->sold_by,
                $saleorder->total_cost,
                $saleorder->total_cost,
                $saleorder->profit,
                $saleorder->profit_percentage,
                $saleorder->payment_amount,
                $saleorder->total_discount,
                $saleorder->total_due);
            $exportArray[] = array();
            $exportArray[] = array('ID', 'Category', 'Product Type', 'Brand', 'Model', 'Color', 'Barcode', 'Supplier', 'Quantity', 'Total Cost', 'Sale Price', 'Discount', 'styles62');
            foreach ($saleorder->saleitems as $saleitem){
                $exportArray[] = array(
                    $saleitem->id,
                    $saleitem->category,
                    $saleitem->producttype,
                    $saleitem->brand,
                    $saleitem->model,
                    $saleitem->color,
                    $saleitem->barcode,
                    $saleitem->supplier,
                    $saleitem->quantity_purchased,
                    $saleitem->quantity_purchased * $saleitem->cost_price,
                    $saleitem->quantity_purchased * $saleitem->unit_price,
                    $saleitem->discount);
            }
            $exportArray[] = array();
        }
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Transaction Count', $saleorderSummary['summary']['transaction_count']);
        $exportArray[] = array('Total Amount', $saleorderSummary['summary']['total_cost']);
        $exportArray[] = array('Stock Cost', $saleorderSummary['summary']['stock_cost']);
        $exportArray[] = array('Total Due', $saleorderSummary['summary']['total_due']);
        $exportArray[] = array('Total Discount', $saleorderSummary['summary']['total_discount']);
        $exportArray[] = array('Payment Amount', $saleorderSummary['summary']['payment_amount']);
        $exportArray[] = array('Profit', $saleorderSummary['summary']['profit']);
        $exportArray[] = array('Profit Percentage', $saleorderSummary['summary']['profit_percentage']);
        return $exportArray;
    }

    function convertSummaryEmployeeToExcel($saleorderSummary, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Employee Summary Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array();
        $exportArray[] = array('Employee', 'Transaction Count', 'Total Cost', 'Total Amount', 'Profit', 'Profit Percentage (%)', 'Paid Amount', 'Total Discount', 'Due Amount', 'styles62');
        
        $employeeArray = $this->CI->employee_library->getEmployeeArray();
        foreach ($saleorderSummary['saleorders'] as $saleorder){
            $exportArray[] = array(
                $employeeArray[$saleorder->employee_id],
                $saleorder->transaction_count,
                $saleorder->stock_cost,
                $saleorder->total_cost,
                $saleorder->profit,
                $saleorder->profit_percentage,
                $saleorder->payment_amount,
                $saleorder->total_discount,
                $saleorder->total_due);
        }
        $exportArray[] = array();
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Transaction Count', $saleorderSummary['summary']['transaction_count']);
        $exportArray[] = array('Total Cost', $saleorderSummary['summary']['stock_cost']);
        $exportArray[] = array('Total Amount', $saleorderSummary['summary']['total_cost']);
        $exportArray[] = array('Profit', $saleorderSummary['summary']['profit']);
        $exportArray[] = array('Profit Percentage', $saleorderSummary['summary']['profit_percentage']);
        $exportArray[] = array('Total Discount', $saleorderSummary['summary']['total_discount']);
        $exportArray[] = array('Payment Amount', $saleorderSummary['summary']['payment_amount']);
        $exportArray[] = array('Total Due', $saleorderSummary['summary']['total_due']);
        return $exportArray;
    }

    function convertDetailEmployeeToExcel($saleorderSummary, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Detail Employee Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array();
        $exportArray[] = array('Transaction ID', 'Transaction Date', 'Sold To', 'Total Cost', 'Total Amount', 'Profit', 'Profit Percentage (%)', 'Paid Amount', 'Total Discount', 'Due Amount', 'styles62');
        foreach ($saleorderSummary['saleorders'] as $saleorder){
            $exportArray[] = array(
                $saleorder->id,
                $saleorder->transaction_date,
                $saleorder->sold_to,
                $saleorder->total_cost,
                $saleorder->total_cost,
                $saleorder->profit,
                $saleorder->profit_percentage,
                $saleorder->payment_amount,
                $saleorder->total_discount,
                $saleorder->total_due);
            $exportArray[] = array();
            $exportArray[] = array('ID', 'Category', 'Product Type', 'Brand', 'Model', 'Color', 'Barcode', 'Supplier', 'Quantity', 'Total Cost', 'Sale Price', 'Discount', 'styles62');
            foreach ($saleorder->saleitems as $saleitem){
                $exportArray[] = array(
                    $saleitem->id,
                    $saleitem->category,
                    $saleitem->producttype,
                    $saleitem->brand,
                    $saleitem->model,
                    $saleitem->color,
                    $saleitem->barcode,
                    $saleitem->supplier,
                    $saleitem->quantity_purchased,
                    $saleitem->quantity_purchased * $saleitem->cost_price,
                    $saleitem->quantity_purchased * $saleitem->unit_price,
                    $saleitem->discount);
            }
            $exportArray[] = array();
        }
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Transaction Count', $saleorderSummary['summary']['transaction_count']);
        $exportArray[] = array('Total Amount', $saleorderSummary['summary']['total_cost']);
        $exportArray[] = array('Stock Cost', $saleorderSummary['summary']['stock_cost']);
        $exportArray[] = array('Total Due', $saleorderSummary['summary']['total_due']);
        $exportArray[] = array('Total Discount', $saleorderSummary['summary']['total_discount']);
        $exportArray[] = array('Payment Amount', $saleorderSummary['summary']['payment_amount']);
        $exportArray[] = array('Profit', $saleorderSummary['summary']['profit']);
        $exportArray[] = array('Profit Percentage', $saleorderSummary['summary']['profit_percentage']);
        return $exportArray;
    }

    function convertDetailStocktakeToExcel($detailstocktakes, $from_date, $to_date){
        $exportArray = array();
        $exportArray[] = array('Detail Stock Take Report '.$from_date.' - '.$to_date, 'styles62');
        $exportArray[] = array();
        $exportArray[] = array('ID', 'Date', 'Category', 'Product Type', 'Employee', 'styles62');
        foreach ($detailstocktakes['stocktakes'] as $stocktake){
            $exportArray[] = array(
                $stocktake->id,
                $stocktake->insertiondate,
                $stocktake->category,
                $stocktake->producttype,
                $stocktake->employee);
            $exportArray[] = array();
            $exportArray[] = array('Stock ID', 'Category', 'Product Type', 'Brand', 'Model', 'Color', 'Barcode', 'Supplier', 'Previous Quantity', 'Check Quantity', 'Remark', 'styles62');
            foreach ($stocktake->stocktakeitems as $stocktakeitem){
                $exportArray[] = array(
                    $stocktakeitem->id,
                    $stocktakeitem->category,
                    $stocktakeitem->producttype,
                    $stocktakeitem->brand,
                    $stocktakeitem->model,
                    $stocktakeitem->color,
                    $stocktakeitem->barcode,
                    $stocktakeitem->supplier,
                    $stocktakeitem->currentqty,
                    $stocktakeitem->countqty,
                    $stocktakeitem->remarks);
            }
            $exportArray[] = array();
        }
        return $exportArray;
    }

    function convertSummaryStockholdingsToExcel($summarystockholdings){
        $exportArray = array();
        $exportArray[] = array('Stock Holding Summary Report', 'styles62');
        $exportArray[] = array();
        $exportArray[] = array('ID', 'Category', 'Product Type', 'Brand', 'Model', 'Color', 'Bar Code', 'Cost Price', 'Selling Price', 'Supplier', 'styles62');
        
        foreach ($summarystockholdings['stocks'] as $stock){
            $exportArray[] = array(
                $stock->id,
                $stock->category_name,
                $stock->producttype_name,
                $stock->brand,
                $stock->model,
                $stock->color,
                $stock->barcode,
                $stock->cost_price,
                $stock->selling_price,
                $stock->supplier);
        }
        $producttypeArray = $this->CI->producttype_library->getProducttypeArray();
        $categoryArray = $this->CI->category_library->getCategoryArray();
        
        $exportArray[] = array();
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Category', 'Quantity', 'Total Cost ('.basecurrency().')', 'styles62');
        if (isset($summarystockholdings['categoryquantity']) && !empty($summarystockholdings['categoryquantity'])){
            foreach($summarystockholdings['categoryquantity'] as $key => $value){
                $exportArray[] = array(
                    $categoryArray[$key],
                    $value,
                    $summarystockholdings['categorytotal_cost'][$key]);
            }
        }
        
        $exportArray[] = array();
        $exportArray[] = array('Product Type', 'Quantity', 'Total Cost ('.basecurrency().')', 'styles62');
        if (isset($summarystockholdings['producttypequantity']) && !empty($summarystockholdings['producttypequantity'])){
            foreach($summarystockholdings['producttypequantity'] as $key => $value){
                $exportArray[] = array(
                    $producttypeArray[$key],
                    $value,
                    $summarystockholdings['producttypetotal_cost'][$key]);
            }
        }
        return $exportArray;
    }

    function convertSummaryItemsToExcel($summaryitems){
        $exportArray = array();
        $exportArray[] = array('Item Summary Report', 'styles62');
        $exportArray[] = array();
        $exportArray[] = array('ID', 'Category', 'Product Type', 'Brand', 'Model', 'Color', 'Quantity', 'Cost Price', 'Selling Price', 'Profit', 'Profit Percentage (%)', 'styles62');
        
        foreach ($summaryitems['stocks'] as $stock){
            $exportArray[] = array(
                $stock->stock_id,
                $stock->category_name,
                $stock->producttype_name,
                $stock->brand,
                $stock->model,
                $stock->color,
                $stock->quantity_purchased,
                $stock->cost_price,
                $stock->unit_price,
                number_format($stock->profit, NUMBER_FLOATING_POINT),
                number_format($stock->profit_percentage, NUMBER_FLOATING_POINT));
        }
        $producttypeArray = $this->CI->producttype_library->getProducttypeArray();
        $categoryArray = $this->CI->category_library->getCategoryArray();
        
        $exportArray[] = array();
        $exportArray[] = array('Summary', 'styles62');
        $exportArray[] = array('Category', 'Quantity', 'Total Sold ('.basecurrency().')', 'Profit', 'Profit Percentage (%)', 'styles62');
        if (isset($summaryitems['categoryquantity']) && !empty($summaryitems['categoryquantity'])){
            foreach($summaryitems['categoryquantity'] as $key => $value){
                $exportArray[] = array(
                    $categoryArray[$key],
                    $value,
                    $summaryitems['category_sellprice'][$key],
                    $summaryitems['category_profit'][$key],
                    number_format($summaryitems['category_profitpercentage'][$key], NUMBER_FLOATING_POINT));
            }
        }
        
        $exportArray[] = array();
        $exportArray[] = array('Product Type', 'Quantity', 'Total Sold ('.basecurrency().')', 'Profit', 'Profit Percentage (%)', 'styles62');
        if (isset($summaryitems['producttypequantity']) && !empty($summaryitems['producttypequantity'])){
            foreach($summaryitems['producttypequantity'] as $key => $value){
                $exportArray[] = array(
                    $producttypeArray[$key],
                    $value,
                    $summaryitems['producttype_sellprice'][$key],
                    $summaryitems['producttype_profit'][$key],
                    number_format($summaryitems['producttype_profitpercentage'][$key], NUMBER_FLOATING_POINT));
            }
        }
        return $exportArray;
    }

    function exportExcel($file_name, $data){
        $this->CI->load->library('parsexls');
        $xls = new parsexls('UTF-8', false, $file_name);
        $xls->addArray($data);
                    
		return $xls->generateXML($file_name);
    }
  
}
