<?php

class Mobilecache_library {
	private $CI; 
  
	public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('paymentmethod_model');
    }

	// Add query to server's cache
	function addToCached($id, $query_result){
        return $this->CI->paymentmethod_model->addToCached($id."_".$this->CI->session->userdata('accountId'), $query_result);
	}

	// Delete server's cache
	function deleteFromCached($id){
        return $this->CI->paymentmethod_model->deleteFromCached($id."_".$this->CI->session->userdata('accountId'));
	}

	// Attempt to retrieve from server's cache
	function getFromCached($id){
        return $this->CI->paymentmethod_model->getFromCached($id."_".$this->CI->session->userdata('accountId'));
	}
  
}
