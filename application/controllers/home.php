<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
    }
    
    public function index(){
        $data = array();
        $this->layout->show('home', $data);
    }

    function logout(){
        $this->session->sess_destroy();
        $this->layout->template('logout');
        $data['redirect'] = base_url().'welcome';
        $this->load->view('logout', $data);
    }

    function decode(){
        $data = array();
        $pemcert = "-----BEGIN CERTIFICATE-----
MIIFazCCBFOgAwIBAgIRAN/LZH5Ptwo+bzrx8sRVfTwwDQYJKoZIhvcNAQELBQAw
gZAxCzAJBgNVBAYTAkdCMRswGQYDVQQIExJHcmVhdGVyIE1hbmNoZXN0ZXIxEDAO
BgNVBAcTB1NhbGZvcmQxGjAYBgNVBAoTEUNPTU9ETyBDQSBMaW1pdGVkMTYwNAYD
VQQDEy1DT01PRE8gUlNBIERvbWFpbiBWYWxpZGF0aW9uIFNlY3VyZSBTZXJ2ZXIg
Q0EwHhcNMTYxMDI4MDAwMDAwWhcNMTcxMDI4MjM1OTU5WjBbMSEwHwYDVQQLExhE
b21haW4gQ29udHJvbCBWYWxpZGF0ZWQxFDASBgNVBAsTC1Bvc2l0aXZlU1NMMSAw
HgYDVQQDExdkZXZhZG1pbi50b3VyZWdvLmNvbS5zZzCCASMwDQYJKoZIhvcNAQEB
BQADggEQADCCAQsCggECANgGUnFxQ9HROxKBTVDDwA+mgV6W+LiQiiPjQg+LVnSK
k5mJkKzteqEfuM4vs73yXquBjYG2Tp9QNmQC2HTiZkLNSTrzKtQDLLwg5hK1zYsn
YbxXaJJ9jw01PuJbVAX/uDfPR9Vqfs4MdHNGIGmG9he6mx2AtpeQ7P0JSHK+zuEv
jmGKQ5im9IxdI4Dn9dkvYP+aYb4tub1Yx1sUrQ+FxBpH/jb/6FPowNTovAPfEu1E
mpXm1JAqn9RkiZyZqCTl/Yqr3QtEHATg/0atKfzqj6UgwwtYUXM/oTWJcSReuFib
xC1TisgndH8LZOtmhhamv+MG1xUr06cbpoEmOdM6qQVpAgMBAAGjggHxMIIB7TAf
BgNVHSMEGDAWgBSQr2o6lFoL2JDqElZz30O0Oija5zAdBgNVHQ4EFgQUtmfc25WO
6GYEnNMHD8+KQCMGQRwwDgYDVR0PAQH/BAQDAgWgMAwGA1UdEwEB/wQCMAAwHQYD
VR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCME8GA1UdIARIMEYwOgYLKwYBBAGy
MQECAgcwKzApBggrBgEFBQcCARYdaHR0cHM6Ly9zZWN1cmUuY29tb2RvLmNvbS9D
UFMwCAYGZ4EMAQIBMFQGA1UdHwRNMEswSaBHoEWGQ2h0dHA6Ly9jcmwuY29tb2Rv
Y2EuY29tL0NPTU9ET1JTQURvbWFpblZhbGlkYXRpb25TZWN1cmVTZXJ2ZXJDQS5j
cmwwgYUGCCsGAQUFBwEBBHkwdzBPBggrBgEFBQcwAoZDaHR0cDovL2NydC5jb21v
ZG9jYS5jb20vQ09NT0RPUlNBRG9tYWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNB
LmNydDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuY29tb2RvY2EuY29tMD8GA1Ud
EQQ4MDaCF2RldmFkbWluLnRvdXJlZ28uY29tLnNnght3d3cuZGV2YWRtaW4udG91
cmVnby5jb20uc2cwDQYJKoZIhvcNAQELBQADggEBAH5PEzAsyG+IewW+t95TC0gx
idPi+cRVxnn43Hack1JzJA4aoz0YMPoLkefQfzGIZyEXdM09JsGD/EvMrz64J4ko
CwrwOpQhb5eYKozThdqsiHNsvUMBNeU9KvbMDipW673JPNbFiSs2CBp5rjEZbLK7
37YkVcWlq+CukDGsB3yw9eaQP253tYDwdM+NYi3319nry6k4KN9Ib/rcdCfEhyyD
s81GAp9q5iW562Du1oSYqfGJsVMXlNxvU9b+Rrm0CPRPIuq7/YdtnfWZhDxFjIAR
5WluZ+lfLzsf4XtJIo4IuaYsOnbnuBCTIZM4nKuYzbic70kscGcs1kQMQVw+xN4=
-----END CERTIFICATE-----";
        
        $cert = openssl_x509_parse($pemcert, true);
        $skid = str_replace(":","",$cert['extensions']['subjectKeyIdentifier']);
        var_dump($cert);
        var_dump($skid);
        return $data;
    }

    function image($name = null){
        if ($name){
            $name = 'http://spaceplace.nasa.gov/templates/featured/sun/'.$name;
            header('Content-Type: image/jpeg');
            readfile($name);
            exit;
        } else {
            $data = array();
            $this->layout->show('login', $data);
        }
    }

    function frontendimage($name = null){
        $image = 'http://spaceplace.nasa.gov/templates/featured/sun/'.$name;
        // Read image path, convert to base64 encoding
        $imageData = base64_encode(file_get_contents($image));

        // Format the image SRC:  data:{mime};base64,{data};
        $src = 'data: '.mime_content_type($image).';base64,'.$imageData;

        // Echo out a sample image
        echo '<img src="'.$src.'">';
    }
}