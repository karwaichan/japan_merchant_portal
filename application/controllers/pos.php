<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
//        $this->load->model('saleorder_model');
        $this->load->library('ci_qr_code');
        $this->load->library('service_library');
    }
    
    function index(){
        redirect('home', 'refresh');
    }

    function viewqrcode(){
        $user_profile = $this->service_library->getUserProfile();
        if (!$user_profile){
            $this->session->set_flashdata('error', 'There\' no user with your profile');
            redirect('dashboard', 'refresh');
        }
        $data['cashier_id'] = $user_profile->user->id;
        $result = $this->service_library->getQrcode($data);
        $data['qr_image'] = $this->print_qr($result->qr_content);
        $data['user_profile'] = $user_profile;
        $this->layout->show('pos/viewqrcode', $data);
    }
    
    function print_qr($codeContents){
        $this->config->load("qr_code");
        $qr_code_config = array();
        $qr_code_config['cacheable'] = $this->config->item('cacheable');
        $qr_code_config['cachedir'] = $this->config->item('cachedir');
        $qr_code_config['imagedir'] = $this->config->item('imagedir');
        $qr_code_config['errorlog'] = $this->config->item('errorlog');
        $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
        $qr_code_config['quality'] = $this->config->item('quality');
        $qr_code_config['size'] = $this->config->item('size');
        $qr_code_config['black'] = $this->config->item('black');
        $qr_code_config['white'] = $this->config->item('white');
        $this->ci_qr_code->initialize($qr_code_config);

        $image_name = microtime().".png";
        $params['data'] = base64_encode($codeContents);
        $params['level'] = 'H';
        $params['size'] = 2;

        $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
        $this->ci_qr_code->generate($params);

        $params['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

        $file = $params['savename'];
        if(file_exists($file)){
            return $params['qr_code_image_url'];
//            unlink($file);
        }
    }

}