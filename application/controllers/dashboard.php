<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
        $this->load->library('ws_curl_library');
        $this->load->library('service_library');
    }

    function index(){
        $data = array();
        $country = ($this->session->set_userdata('country')) ? $this->session->set_userdata('country') : 'SGP';
        $per_page = 5;
        $data['newsevents'] = $this->service_library->getNewsEventByCountry($country, $per_page);

		$temp_sales_amount = $this->service_library->getSalesAmountForMerchant()->sales_amount;
		foreach ($temp_sales_amount as $retailer) {
			foreach ($retailer->outlet_sale as $outlet) {
				$data['sales'] = [
					'today_sales_amount' => $outlet->today_sales_amount,
					'month_to_date_sales_amount' => $outlet->month_to_date_sales_amount,
					'rolling_six_month_sales_amount' => $outlet->rolling_six_month_sales_amount
				];
			}
		}

        $this->layout->show('dashboard', $data);
    }

    function test(){
//        $url = WEBSERVICE_URL.'receipt/cancelReceipt';
//        $data_input['user_token'] = $this->session->userdata('user_token');
//        $data_input['receiptNumber'] = 'abc';
//        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
//        var_dump($user_result);die;
    }

    function checkAuth(){
        $url = WEBSERVICE_URL.'merchant/checkMerchantByAuth';
        $data_input['user_token'] = $this->session->userdata('user_token');
        $user_result = $this->ws_curl_library->wsDecodedPostCall($url, $data_input);
        var_dump($user_result);die;
    }
	
	
}