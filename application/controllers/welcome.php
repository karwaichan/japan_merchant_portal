<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
    function __construct(){
        parent::__construct();
//        $this->load->model('user_model');
        $this->load->library('role_library');
        $this->load->library('service_library');
    }

    function index(){
        $data = array();
        if($this->input->post()){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $user = $this->service_library->login($username, $password);
            if($user){
                //success
                $allow_roles = array(USERTYPE_MANAGER, USERTYPE_CASHIER);

                //SGP: only manager can login
//                if ($user->merchant->country == 'SGP' && $user->role == USERTYPE_CASHIER){
//                    $data['error'] = 1;
//                    $this->session->set_flashdata('error', 'Account has no permission!');
//                    redirect('welcome', 'refresh');
//                }

                if (!in_array($user->role_id, $allow_roles)){
                    $data['error'] = 1;
                    $this->session->set_flashdata('error', 'Account has no permission!');
                    redirect('welcome', 'refresh');
                }
                $this->session->set_userdata('username', $user->username);
                $this->session->set_userdata('userid', $user->id);
                $outlet_id = (isset($user->outlet) && $user->outlet->outletID) ? $user->outlet->outletID : 0;
                $this->session->set_userdata('outlet_id', $outlet_id);
                $this->session->set_userdata('userrole', $user->role);
                $this->session->set_userdata('user_token', $user->user_token);
                $this->session->set_userdata('merchantid', $user->merchant->merchantID);
                $this->session->set_userdata('country', $user->merchant->country);
                $this->session->set_userdata('site_lang', 'japanese');
                $this->session->set_flashdata('message', $this->lang->line('welcome_message').' '.$user->username);
                redirect('dashboard', 'redirect');
            }else{
                $data['error'] = 1;
                $this->session->set_flashdata('error', 'Account is not valid!');
                redirect('welcome', 'refresh');
            }
        }

        $this->layout->show('login', $data);
    }
    
//    private function validateEmployeeRole($user, $reseller){
//        $this->load->model('employee_model');
//        $this->load->model('role_model');
//        $this->load->model('rolepermission_model');
//        $employee = $this->employee_model->getByUserid($user->id);
//        if (!$employee){
//            $this->session->unset_userdata('accountName');
//            $this->session->set_flashdata('error', 'Account is not valid!');
//            redirect('welcome', 'refresh');
//        }
//        $role = $this->role_model->getById($employee->role_id);
//        if (!$role || $role->del == '1'){
//            $this->session->unset_userdata('accountName');
//            $this->session->set_flashdata('error', 'This employee has no active role. Please contact reseller '.$reseller->username.'!');
//            redirect('welcome', 'refresh');
//        }
//        $rolepermissions = $this->rolepermission_model->getByRoleid($role->id);
//        if (!$rolepermissions){
//            $this->session->unset_userdata('accountName');
//            $this->session->set_flashdata('error', 'This employee has no role permission. Please contact reseller '.$reseller->username.'!');
//            redirect('welcome', 'refresh');
//        }
//        $employee_permissions = array();
//        foreach ($rolepermissions as $rolepermission){
//            $employee_permissions[$rolepermission->module] = $rolepermission->permission;
//        }
//        
//        $this->session->set_userdata('employee_permissions', $employee_permissions);
//    }
}