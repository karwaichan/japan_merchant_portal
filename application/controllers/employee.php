<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
        $this->load->library('service_library');
        
    }
    
    function index(){
        redirect('home', 'refresh');
    }

    function employeelist(){
        $data = array();
        $data =  (array)$this->service_library->getEmployeeList();
        $data['roles'] = unserialize (ROLES);
        $this->layout->show('employee/employeelist', $data);
    }

    function createemployee(){
        if ($this->input->post()){
            $data['cashierLoginId'] = trim($this->input->post('username'));
            $data['email'] = trim($this->input->post('email'));
            $data['phone_number'] = trim($this->input->post('phone_number'));
            $data['role_id'] = trim($this->input->post('role_id'));
            $data['cashierName'] = trim($this->input->post('first_name'));
            $data['last_name'] = trim($this->input->post('last_name'));
            $data['outletId'] = $this->session->userdata('outlet_id');
            $data['phone_number'] = trim($this->input->post('phone_number'));
            $password = trim($this->input->post('password'));
            $retype_password = trim($this->input->post('retype_password'));
            if (strlen($password) > 0 || strlen($retype_password) > 0){
                if ($password !== $retype_password){
                    $this->session->set_flashdata('error', 'Password and Retype Password are not matched!');
                    redirect('employee/createemployee', 'refresh');
                }
                $data['cashierpassword'] = $password;
            }
            $result = $this->service_library->createCashier($data);
            if ($result){
                $this->session->set_flashdata('message', 'Cashier id = '.$result->cashierId.' has been created successfully');
            } else {
                $this->session->set_flashdata('error', 'Server error, please try again!');
            }
            redirect('employee/employeelist', 'refresh');
        }
        $data['user_profile'] = $this->service_library->getUserProfile();
        $data['roles'] = unserialize (ROLES);
        $this->layout->show('employee/createemployee', $data);
    }

    function updateemployee($id){
        $data['id'] = $id;
        if ($this->input->post()){
            $data['email'] = trim($this->input->post('email'));
            $data['phone_number'] = trim($this->input->post('phone_number'));
            $data['role_id'] = trim($this->input->post('role_id'));
            $data['first_name'] = trim($this->input->post('first_name'));
            $data['last_name'] = trim($this->input->post('last_name'));
            $data['outlet_id'] = $this->session->userdata('outlet_id');
            $data['phone_number'] = trim($this->input->post('phone_number'));
            $password = trim($this->input->post('password'));
            $retype_password = trim($this->input->post('retype_password'));
            if (strlen($password) > 0 || strlen($retype_password) > 0){
                if ($password !== $retype_password){
                    $this->session->set_flashdata('error', 'Password and Retype Password are not matched!');
                    redirect('employee/updateemployee/'.$id, 'refresh');
                }
                $data['password'] = $password;
            }
            $result = $this->service_library->updateMerchantProfileById($data['id'], $data);
            if ($result){
                $this->session->set_flashdata('message', 'Cashier id = '.$id.' has been updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Server error, please try again!');
            }
            redirect('employee/employeelist', 'refresh');
        }
        if (!$id){
            $this->session->set_flashdata('error', 'There is no record found!');
            redirect('employee/employeelist', 'refresh');
        }
        $data['cashier_profile'] = $this->service_library->getCashierById($data);
        if (!$data['cashier_profile']){
            $this->session->set_flashdata('error', 'There\' no user with your profile');
            redirect('dashboard', 'refresh');
        }
        $data['roles'] = unserialize (ROLES);
        $this->layout->show('employee/updateemployee', $data);
    }

    function deleteemployee(){
        $data['cashierId'] = trim($this->input->post('id'));
        $check = $this->service_library->deleteCashierById($data);
        if($check !== false)
          exit(json_encode(array('status' => 'true')));
        else
          exit(json_encode(array('status' => 'false')));
    }
}