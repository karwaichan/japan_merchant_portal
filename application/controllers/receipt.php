<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receipt extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
		$this->load->library('ci_qr_code');
        $this->load->library('service_library');
		$this->load->library('PHPExcel/PHPExcel');
    }
    
    function index(){
        redirect('home', 'refresh');
    }

    function testsoap(){
        $data = array();
        $receipts = $this->service_library->testsoap();
    }

    function receiptlist(){
        $data = array();
        $receipts = $this->service_library->getReceiptList();
        $data['country'] = unserialize (JPN_COUNTRY);
		$data['tickets'] = empty($receipts) ? false : $receipts;
        $this->layout->show('receipt/receiptlist', $data);
    }

    function jpcovenantofpurchase($id){
        $this->layout->template('printing');
        $data = array();
        $data['doc_id'] = $id;
        $tickets = $this->service_library->getReceiptById($data);
        if (!$tickets){
            $this->session->set_flashdata('error', 'There\'s no order with id '.$id.'!');
            redirect('receipt/receiptlist', 'redirect');
        }

		$docId = str_replace('.', '', $tickets->etrs->doc_id);

		#divided in to consumable and commodities
		$receipts = $tickets->receipt;

		#currently 11 = consumable and 12 = commodity
		$consumable = array_filter($receipts, function($value){
			return $value->category_id === 11;
		});

		$commodity = array_filter($receipts, function($value){
			return $value->category_id === 12;
		});

		$totalAmount = array_reduce($receipts, function($carry, $item){
			return $carry +=$item->amount;
		});

		$consumableObject = empty($consumable) ? null : array_shift($consumable);
		$commodityObject = empty($commodity) ? null : array_shift($commodity);

		$residentStatus = unserialize(JPN_NEW_RESIDENT_STATUS);

		$date = $this->changeTimeToJapan($tickets->etrs->date_of_landing);
		$data['date_of_landing'] = $date;
		$data['qr_image'] = $this->print_qr($docId);
		$data['total_amount'] = empty($totalAmount) ? 0 : $totalAmount;
		$data['consumable'] = $consumableObject;
		$data['commodity'] = $commodityObject ;
		$data['docId'] = $docId;
		$data['ticket'] = $tickets;
		$data['resident_status'] = $residentStatus;
        $this->layout->show('receipt/jpcovenantofpurchase', $data);
    }

    function jprecordofpurchase($id){
        $this->layout->template('printing');
        $data = array();
        $data['doc_id'] = $id;
        $tickets = $this->service_library->getReceiptById($data);
        if (!$tickets){
            $this->session->set_flashdata('error', 'There\'s no order with id '.$id.'!');
            redirect('receipt/receiptlist', 'redirect');
        }

        $docId = str_replace('.', '', $tickets->etrs->doc_id);

        #divided in to consumable and commodities
		$receipts = $tickets->receipt;

		#currently 11 = consumable and 12 = commodity
		$consumable = array_filter($receipts, function($value){
			return $value->category_id === 11;
		});

		$commodity = array_filter($receipts, function($value){
			return $value->category_id === 12;
		});

		$totalAmount = array_reduce($receipts, function($carry, $item){
			return $carry +=$item->amount;
		});

		$consumableAmount = array_shift($consumable)->amount;
		$commodityAmount = array_shift($commodity)->amount;
		$date = $this->changeTimeToJapan($tickets->etrs->date_of_landing);
		$data['date_of_landing'] = $date;
        $data['qr_image'] = $this->print_qr($docId);
		$data['consumable_amount'] =  empty($consumableAmount) ? 0 : $consumableAmount;
		$data['commodity_amount'] = empty($commodityAmount) ? 0 : $commodityAmount;
		$data['total_amount'] = empty($totalAmount) ? 0 : $totalAmount;
        $data['docId'] = $docId;
        $data['ticket'] = $tickets;
        $this->layout->show('receipt/jprecordofpurchase', $data);
    }

    private function changeTimeToJapan($time){
		$date = new DateTime(date('Y-m-d H:i:s', $time));
		$date->setTimezone(new DateTimeZone('Asia/Tokyo'));

		return $date;
	}

    function print_receipt($ticket_Id) {
        $data = array();

        $input['ticket_id'] = $ticket_Id;
        $receiptdetail = $this->service_library->getReceiptById($input);

        if ($receiptdetail) {
            $tickets = array_shift($receiptdetail->tickets);
                $etrs_status = $tickets->etrs_status;
                $Doc_ID = $tickets->DOC_ID;
                $barcode_number = str_replace('.', '', $tickets->DOC_ID);
                $tax_rate = $tickets->tax_rate;
                $eTRS_sales_amount = $tickets->amount;
                $eTRS_gst_amount = $tickets->gst_amount;
                $eTRS_service_fee = $tickets->refund_fee;
                $eTRS_provisional_refund_amount = $tickets->refund_amount;

                $date = $tickets->created_at / 1000;
                $date_of_issue = date("d/m/Y", $date);
                $time_of_issue = date("H:i:s", $date);

                $ticket_info = array_shift($tickets->ticket_info);
                
                $merchant_name = $ticket_info->merchant->name;
                $eTRS_gst_reg_no = $ticket_info->merchant->gstRegisterNumber;
                $passport_no = $ticket_info->member->passport_number;
                $nationality = $ticket_info->member->nationality;
                $packages = $ticket_info->packages;

                //get country numeric code by reading countrylist json file
                $nationality_iso3 = $ticket_info->member->nationality_iso3;
                $country_list_array = json_decode(json_encode(json_decode(file_get_contents(FCPATH . 'asset/json/nationalitylist.json'))), True);
                $country_numeric_code = $country_list_array[$nationality_iso3]['numcode'];

                $data['Doc_ID'] = $Doc_ID;
                $data['barcode_number'] = $barcode_number;
                $data['tax_rate'] = $tax_rate;
                $data['etrs_status'] = $etrs_status;

                $data['eTRS_sales_amount'] = $eTRS_sales_amount;
                $data['eTRS_gst_amount'] = $eTRS_gst_amount;
                $data['eTRS_service_fee'] = $eTRS_service_fee;
                $data['eTRS_provisional_refund_amount'] = $eTRS_provisional_refund_amount;

                $data['merchant_name'] = $merchant_name;
                $data['eTRS_gst_reg_no'] = $eTRS_gst_reg_no;
                $data['date_of_issue'] = $date_of_issue;
                $data['time_of_issue'] = $time_of_issue;
                $data['passport_no'] = $passport_no;
                $data['nationality'] = $nationality;
                $data['country_numeric_code'] = $country_numeric_code;

                $receipt_status_array = unserialize(RECEIPT_STATUS);
                $data['receipt_status_array'] = $receipt_status_array;

                $data['packages'] = $packages;
                $this->layout->show('receipt/print_receipt', $data);
        } else {
            redirect(base_url().'receipt/receipt_list', '');
        }
    }

	function print_qr($codeContents){
		$this->config->load("qr_code");
		$qr_code_config = array();
		$qr_code_config['cacheable'] = $this->config->item('cacheable');
		$qr_code_config['cachedir'] = $this->config->item('cachedir');
		$qr_code_config['imagedir'] = $this->config->item('imagedir');
		$qr_code_config['errorlog'] = $this->config->item('errorlog');
		$qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
		$qr_code_config['quality'] = $this->config->item('quality');
		$qr_code_config['size'] = $this->config->item('size');
		$qr_code_config['black'] = $this->config->item('black');
		$qr_code_config['white'] = $this->config->item('white');
		$this->ci_qr_code->initialize($qr_code_config);

		$image_name = microtime().".png";
		$params['data'] = $codeContents;
		$params['level'] = 'H';
		$params['size'] = 2;

		$params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
		$this->ci_qr_code->generate($params);

		$params['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

		$file = $params['savename'];
		if(file_exists($file)){
			return $params['qr_code_image_url'];
			//            unlink($file);
		}
	}

	public function exportDataToExcel()
	{
		$input = [];

		$dateFrom = date('Y-m-d', strtotime($_POST['dateFrom']));
		$dateTo = date('Y-m-d', strtotime($_POST['dateTo']));

		$input['dateFrom'] = $dateFrom;
		$input['dateTo'] = $dateTo;

		$receipts = $this->service_library->getReceiptList($input);

		if($receipts !== false || !empty($receipts)) {
			$excel = new PHPExcel();
			$excel->setActiveSheetIndex(0);

			$excelHeader = [
				'A' => '#',
				'B' => $this->lang->line('date_of_purchase'),
				'C' => $this->lang->line('transaction_id'),
				'D' => $this->lang->line('status'),
				'E' => $this->lang->line('customer_name'),
				'F' => $this->lang->line('document_type'),
				'G' => $this->lang->line('number'),
				'H' => $this->lang->line('nationality'),
				'I' => $this->lang->line('amount') . ' (¥)'
			];

			$excel->getActiveSheet()->setTitle('Transaction History');

			foreach ($excelHeader as $key => $value) {
				$excel->getActiveSheet()->setCellValue($key . '1', $value);
				$excel->getActiveSheet()->getStyle($key . '1')->getFont()->setSize(13);
				$excel->getActiveSheet()->getStyle($key . '1')->getFont()->setBold(true);
				$excel->getActiveSheet()->getStyle($key . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}

			foreach ($excelHeader as $key => $value) {
				$excel->getActiveSheet()->getColumnDimension($key)->setAutoSize(true);
			}

			$index     = 1;
			$rowNumber = 2;

			foreach ($receipts->tickets as $result) {
				$excel->getActiveSheet()->setCellValue('A' . $rowNumber, $index);
				$excel->getActiveSheet()->getStyle('A' . $rowNumber)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$excel->getActiveSheet()->setCellValue('B' . $rowNumber, date('Y-m-d', $result->ticket->created_at/ 1000));
				$excel->getActiveSheet()->setCellValue('C' . $rowNumber, $result->ticket->DOC_ID);
				$excel->getActiveSheet()->setCellValue('D' . $rowNumber, $this->lang->line($result->ticket->status));
				$excel->getActiveSheet()->setCellValue('E' . $rowNumber, $result->ticket->user_name);
				$excel->getActiveSheet()->setCellValue('F' . $rowNumber, $this->lang->line('passport'));
				$excel->getActiveSheet()->setCellValue('G' . $rowNumber, $result->ticket->passport_number);
				$excel->getActiveSheet()->setCellValue('H' . $rowNumber, $result->ticket->nationality_iso3);
				$excel->getActiveSheet()->setCellValue('I' . $rowNumber, $result->ticket->amount);

				foreach ($excelHeader as $key => $value) {
					$excel->getActiveSheet()->getStyle($key . $rowNumber)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}

				$index++;
				$rowNumber++;
			}


			$date = new DateTime(date('Y-m-d H:i:s'));
			$date->setTimezone(new DateTimeZone('Asia/Tokyo'));

			$filename = 'Transaction_History_' . $date->format('Y-m-d H:i:s	') . '.xlsx';
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
			$objWriter->save('php://output');
			die();
		}
		else {
			$this->session->set_flashdata('error', 'There are no transaction for the selected date');
			redirect('receipt/receiptlist', 'redirect');
		}
    }

	public function copTest($id)
	{
		$this->layout->template('printing');
        $data = array();
        $data['doc_id'] = $id;
        $tickets = $this->service_library->getReceiptById($data);
        if (!$tickets){
			$this->session->set_flashdata('error', 'There\'s no order with id '.$id.'!');
			redirect('receipt/receiptlist', 'redirect');
		}

		$docId = str_replace('.', '', $tickets->etrs->doc_id);

		#divided in to consumable and commodities
		$receipts = $tickets->receipt;

		#currently 11 = consumable and 12 = commodity
		$consumable = array_filter($receipts, function($value){
			return $value->category_id === 11;
		});

		$commodity = array_filter($receipts, function($value){
			return $value->category_id === 12;
		});

		$totalAmount = array_reduce($receipts, function($carry, $item){
			return $carry +=$item->amount;
		});

		$consumableObject = empty($consumable) ? null : array_shift($consumable);
		$commodityObject = empty($commodity) ? null : array_shift($commodity);

		$date = $this->changeTimeToJapan($tickets->etrs->date_of_landing);
		$data['date_of_landing'] = $date;
		$data['qr_image'] = $this->print_qr($docId);
		$data['total_amount'] = empty($totalAmount) ? 0 : $totalAmount;
		$data['consumable'] = $consumableObject;
		$data['commodity'] = $commodityObject ;
		$data['docId'] = $docId;
		$data['ticket'] = $tickets;
//		$data['resident_status'] =
        $this->layout->show('receipt/coptest', $data);
    }
}