<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
        $this->load->library('service_library');
        
    }
    
    function index(){
        redirect('home', 'refresh');
    }

    function updateprofile(){
        $data = array();
        if ($this->input->post()){
            $password = trim($this->input->post('password'));
            $retype_password = trim($this->input->post('retype_password'));
            if (!empty($password) && $retype_password == $password)
                $data['password'] = $password;
            $data['id'] = trim($this->input->post('id'));
            $data['email'] = trim($this->input->post('email'));
            $data['first_name'] = trim($this->input->post('first_name'));
            $data['last_name'] = trim($this->input->post('last_name'));
            $data['phone_number'] = trim($this->input->post('phone_number'));
            $data['role_id'] = trim($this->input->post('role_id'));
            $data['outlet_id'] = trim($this->input->post('outlet_id'));
            $result = $this->service_library->updateMerchantProfileById($data['id'], $data);
            if ($result){
                $this->session->set_flashdata('message', 'Your profile has been updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Server error, please try again!');
            }
            redirect('/profile/updateprofile', 'refresh');
        }
        $user_profile = $this->service_library->getUserProfile();
        if (!$user_profile){
            $this->session->set_flashdata('error', 'There\' no user with your profile');
            redirect('dashboard', 'refresh');
        }
        $data['roles'] = unserialize (ROLES);
        $data['user_profile'] = $user_profile;
        
        $data['outlets'] = $this->service_library->getOulets($user_profile->merchant->id);
        $this->layout->show('setting/updateprofile', $data);
    }
}