<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outlet extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
        $this->load->library('service_library');
        
    }
    
    function index(){
        redirect('home', 'refresh');
    }

    function updateoutlet(){
        $data = array();
        if ($this->input->post()){
            $data['id'] = trim($this->input->post('id'));
            $data['name'] = trim($this->input->post('name'));
            $data['address'] = trim($this->input->post('address'));
            $data['building_name'] = trim($this->input->post('building_name'));
//            $data['mall_id'] = trim($this->input->post('mall_id'));
            $data['postal_code'] = trim($this->input->post('postal_code'));
            $data['phone_number'] = trim($this->input->post('phone_number'));
            $data['longitude'] = trim($this->input->post('longitude'));
            $data['latitude'] = trim($this->input->post('latitude'));
            $data['tax_office'] = trim($this->input->post('tax_office'));
            $data['tax_place'] = trim($this->input->post('tax_place'));
            $data['outlet_gstn'] = trim($this->input->post('outlet_gstn'));
            $data['share_link'] = trim($this->input->post('share_link'));
            $data['email'] = trim($this->input->post('email'));
            $result = $this->service_library->updateOutletById($data['id'], $data);
            if ($result){
                $this->session->set_flashdata('message', 'Your outlet has been updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Server error, please try again!');
            }
            redirect('outlet/updateoutlet', 'refresh');
        }
        $user_profile = $this->service_library->getUserProfile();
        if (!$user_profile){
            $this->session->set_flashdata('error', 'There\' no user with your profile');
            redirect('dashboard', 'refresh');
        }
        $data['outlet'] = $user_profile->outlet;
        
        $this->layout->show('outlet/updateoutlet', $data);
    }

    function updateoutletcategory(){
        $data = array();
        if ($this->input->post()){
            $data['outlet_category'] = json_encode($this->input->post('outlet_category'));
            $data['outlet_id'] = $this->session->userdata('outlet_id');
            $result = $this->service_library->updateOutletCategoryById($data['outlet_id'], $data);
            if ($result){
                $this->session->set_flashdata('message', 'Your outlet categories have been updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Server error, please try again!');
            }
            redirect('outlet/updateoutletcategory', 'refresh');
        }
        $data['categories'] = $this->service_library->getCategories();
        if (!$data['categories']){
            $this->session->set_flashdata('error', 'There\' no category');
            redirect('dashboard', 'refresh');
        }
        $user_profile = $this->service_library->getUserProfile();
        if (!$user_profile){
            $this->session->set_flashdata('error', 'There\' no user with your profile');
            redirect('dashboard', 'refresh');
        }
        $data['outlet'] = $user_profile->outlet;
        $data['outlet_categories'] = $this->service_library->getCategoryByOutletId($user_profile->outlet->id);
        $data['merchant_categories'] = $this->service_library->getCategoryByMerchantId($this->session->userdata('merchantid'));
        $this->layout->show('outlet/updateoutletcategory', $data);
    }
}