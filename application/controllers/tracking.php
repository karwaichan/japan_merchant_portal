<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tracking extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
        $this->load->library('service_library');
    }
    
    function index(){
        redirect('home', 'refresh');
    }

    function trackinglist(){
        $data = array();
        $user_profile = $this->service_library->getUserProfile();
        if (!$user_profile){
            $this->session->set_flashdata('error', 'There\' no user with your profile');
            redirect('dashboard', 'refresh');
        }
        $devices = $this->service_library->getTrackingDeviceList($user_profile->outlet->id);
        $data['trackingdevices'] = $devices->devices;
        $this->layout->show('tracking/trackinglist', $data);
    }

    function trackingdevice($mobile_id, $user_id = false){
        $data = array();
        if (!$mobile_id || $user_id === false){
            $this->session->set_flashdata('error', 'There\'s no device data');
            redirect('tracking/trackinglist', 'refresh');
        }
        $data['from'] = 0;
        $data['to'] = time();
        $data['mobile_id'] = $mobile_id;
        $data['user_id'] = $user_id;
        $data['device'] = $data['trips'] = false;
        $result = $this->service_library->getTripsByPeriod($data);
        if (!$result){
            $this->session->set_flashdata('error', 'There\'s no trip for device '.$mobile_id);
            redirect('tracking/trackinglist', 'refresh');
        }
        $data['device'] = $result[0]->device;
        $data['trips'] = array_reverse((array)$result[0]->trips, true);
        $this->layout->show('tracking/trackingdevice', $data);
    }
}