<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsevent extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->layout->template('reseller');
        $this->load->library('ws_curl_library');
        $this->load->library('service_library');
    }
    
    public function index(){
        $data = array();
        $this->layout->show('home', $data);
    }

    function newseventlist(){
        $data = array();
        $country = ($this->session->set_userdata('country')) ? $this->session->set_userdata('country') : 'SGP';
        $data['newsevents'] = $this->service_library->getNewsEventByCountry($country);
//        var_dump($data['newsevents']);die;
        $this->layout->show('newsevent/newseventlist', $data);
    }

    function newseventdetail($id){
        $data = array();
        $data['newsevent'] = $this->service_library->getNewsEventById($id);
        $this->layout->show('newsevent/newseventdetail', $data);
    }
	
	
}