<?php

class Roleusers_model extends MY_Model {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->db_name = 'role_users';
    }

    function create($data){
        $data['created_at'] = time();
        $result = $this->db->insert($this->db_name, $data);
        if($result){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedByAll();
            }
            return $this->db->insert_id();
        }
        return false;
    }

    function getByUserid($user_id){
        if(MODEL_CACHE_ENABLED){
            $result = $this->getFromCached('getByUserid_'.$user_id);
            if($result){
                return $result;
            }
        }
        $this->db->where('user_id',$user_id);
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0)
            return false;
        $result = $query->first_row();
        if(MODEL_CACHE_ENABLED){
            $this->addToCached('getByUserid_'.$name, $result);
        }
        return $result;
    }

    function updateById($user_id, $data){
        $role = $this->getById($user_id);
        if (!$role) return false;
        $this->db->where('user_id', $user_id);
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($user_id);
                $this->deleteFromCached('getByUserid_'.$role->user_id);
            }
            return TRUE;
        }
        return FALSE;
    }
  
    public function deleteById($user_id){
        $role = $this->getById($user_id);
        if(!$role)
            return false;

        $this->db->where('$user_id', $user_id);
        if($this->db->delete($this->db_name)){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($user_id);
                $this->deleteFromCached('getByUserid_'.$role->user_id);
            }
            return true;
        }
        return false;
    }
}
