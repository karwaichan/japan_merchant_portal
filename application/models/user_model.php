<?php

class User_model extends MY_Model {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->db_name = 'users';
    }
    
    function login($username, $password){
        var_dump($this->encrypt->sha1($password));die;
        //check reseller
        $this->db->where('username',$username);
        $this->db->where('status', ACCOUNT_STATUS_ACTIVE);
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0){
            return false;
        }
        
        $user = $query->first_row();
        $encrypted_password = $this->encrypt->sha1($password . $user->key);
        if($encrypted_password === $user->password){
            $this->updateLoginById($user->id);
            return $user;
        }
        return false;
    }

	function create($data){
            $data['created_at'] = $data['key'] = time();
            $data['password'] = $this->encrypt->sha1($data['password'] . $data['key']);
            $result = $this->db->insert($this->db_name, $data);
            if($result){
                if(MODEL_CACHE_ENABLED){
                    $this->deleteCachedByAll();
                }
                return $this->db->insert_id();
            }
		return false;
	}

    function updateLoginById($id){
        $data['last_login'] = time();
        $this->db->where('id', $id);
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0){
            return TRUE;
        }
        return FALSE;
    }  

    //no password field
    function updateUserById($id, $data){
        $user = $this->getById($id);
        if(!$user)
            return false;
        $this->db->where('id', $id);
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($id);
                $this->deleteFromCached('getByParentid_'.$user->parent_id);
            }
            return TRUE;
        }
        return FALSE;
    }  

    function updateById($id, $data){
        $user = $this->getById($id);
        if(!$user)
            return false;
        $key = time();
        if (isset($data['password'])){
            $data['password'] = $this->encrypt->sha1($key . $data['password']);
            $data['key'] = $key;
        }
            
        $data['date_updated'] = $key;
        $data['trial'] = ($data['trial']) ? 'yes' : 'no';
        $data['renewal_date'] = strtotime($data['renewal_date'])+3600*24;

        $this->db->where('id', $id);
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($id);
                $this->deleteFromCached('getByParentid_'.$user->parent_id);
            }
            return TRUE;
        }
        return FALSE;
    }

    function getAllValid(){
        if(MODEL_CACHE_ENABLED){
            $result = $this->getFromCached('getAllValid');
            if($result){
                return $result;
            }
        }
        $this->db->where('account_status != "deleted"');
        $this->db->where('parent_id = 0');
        $result = $this->db->get($this->db_name)->result();
        if(MODEL_CACHE_ENABLED){
            $this->addToCached('getAllValid', $result);
        }
        return $result;
    }

    function getByUsername($username){
        if(MODEL_CACHE_ENABLED){
            $result = $this->getFromCached('getByUsername_'.$username);
            if($result){
                return $result;
            }
        }
        $this->db->where('username', $username);
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0)
            return false;
        
        $result = $query->result();
        if(MODEL_CACHE_ENABLED){
            $this->addToCached('getByUsername_'.$username, $result);
        }
        return $result;
    }

    function getByParentid($parent_id){
        if(MODEL_CACHE_ENABLED){
            $result = $this->getFromCached('getByParentid_'.$parent_id);
            if($result){
                return $result;
            }
        }
        $this->db->where('parent_id', $parent_id);
        $this->db->where('account_status != "deleted"');
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0)
            return false;
        
        $result = $query->result();
        if(MODEL_CACHE_ENABLED){
            $this->addToCached('getByParentid_'.$parent_id, $result);
        }
        return $result;
    }
  
    public function deleteById($id){
        $user = $this->getById($id);
        if(!$user)
            return false;

        $this->db->where('id', $id);
        if($this->db->delete($this->db_name)){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($id);
            }
            return true;
        }
        return false;
    }
}
