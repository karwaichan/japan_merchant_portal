<?php

class Admin_model extends MY_Model {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->db_name = 'admins';
    }
    
    function login($username, $password){
        $this->db->where('username', $username);
        $this->db->where('account_status', ACCOUNT_STATUS_ACTIVE);
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0)
            return false;
        
        $admin = $query->first_row();
        
        $encrypted_password = $this->encrypt->sha1($admin->key . $password);
			
        if($encrypted_password === $admin->password){
            $this->updateLoginById($admin->id);
            return $admin;
        }
        return false;
    } 

    function updateLoginById($id){
        $data['last_login'] = time();
        $this->db->where('id', $id);
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0){
            return TRUE;
        }
        return FALSE;
    }  
}
