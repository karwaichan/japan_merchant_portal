<?php

class Country_model extends My_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->db_name = 'countries';
    }

  function getByCountry($country_code){
    if(MODEL_CACHE_ENABLED){
        $result = $this->getFromCached('getByCountry_'.$country_code);
        if($result){
            return $result;
        }
    }
    $this->db->where('country_code', $country_code);
    $query = $this->db->get($this->db_name);
    if ($query->num_rows() == 0){
      return false;
    }
    $result = $query->first_row();
    if(MODEL_CACHE_ENABLED){
        $this->addToCached('getByCountry_'.$country_code, $result);
    }
    return $result;
  }
}
