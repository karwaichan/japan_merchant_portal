<?php

class Role_model extends MY_Model {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->db_name = 'roles';
    }

    function create($data){
        $result = $this->db->insert($this->db_name, $data);
        if($result){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedByAll();
            }
            return $this->db->insert_id();
        }
        return false;
    }

    function getByName($name){
        if(MODEL_CACHE_ENABLED){
            $result = $this->getFromCached('getByName_'.$name);
            if($result){
                return $result;
            }
        }
        $this->db->where('name',$name);
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0)
            return false;
        $result = $query->first_row();
        if(MODEL_CACHE_ENABLED){
            $this->addToCached('getByName_'.$name, $result);
        }
        return $result;
    }

    function getBySlug($slug){
        if(MODEL_CACHE_ENABLED){
            $result = $this->getFromCached('getBySlug_'.$slug);
            if($result){
                return $result;
            }
        }
        $this->db->where('slug',$slug);
        $query = $this->db->get($this->db_name);
        if($query->num_rows() == 0)
            return false;
        $result = $query->first_row();
        if(MODEL_CACHE_ENABLED){
            $this->addToCached('getBySlug_'.$slug, $result);
        }
        return $result;
    }

    function updateById($id, $data){
        $role = $this->getById($id);
        if (!$role) return false;
        $this->db->where('id', $id);
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($id);
                $this->deleteFromCached('getByName_'.$role->name);
                $this->deleteFromCached('getBySlug_'.$role->slug);
            }
            return TRUE;
        }
        return FALSE;
    }
  
    public function deleteById($id){
        $role = $this->getById($id);
        if(!$role)
            return false;

        $this->db->where('id', $id);
        if($this->db->delete($this->db_name)){
            if(MODEL_CACHE_ENABLED){
                $this->deleteCachedById($id);
                $this->deleteFromCached('getByName_'.$role->name);
                $this->deleteFromCached('getBySlug_'.$role->slug);
            }
            return true;
        }
        return false;
    }
}
