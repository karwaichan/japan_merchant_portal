<?php

$lang['welcome_message'] = 'ようこそ！';
$lang['dashboard'] = 'ダッシュボード';
$lang['merchant_portal'] = '販売者ポータル';
$lang['menu'] = 'メニュー';
$lang['qr_code'] = 'QRコード';
$lang['transaction'] = '購入記録';
$lang['language'] = 'ランゲージ';
$lang['update_profile'] = 'プロフィール編集';
$lang['logout'] = 'ログアウト';
$lang['help'] = 'ヘルプ';
$lang['merchant_information'] = '販売者情報';
$lang['merchant'] = '販売者';
$lang['cashier'] = 'キャッシャー';
$lang['list_of_receipt'] = '購入記録の一覧';
$lang['transaction_id'] = '伝票番号';
$lang['customer_name'] = '購入者';
$lang['amount'] = '合計価額 (円)';
$lang['date_of_purchase'] = '購入年月日';
$lang['action'] = 'アクション';
$lang['document_type'] = '旅券等の種類';
$lang['number'] = '番号';
$lang['nationality'] = '国籍';
$lang['rop'] = '記録票';
$lang['cop'] = '誓約書';
$lang['update_outlet'] = 'Update Outlet';
$lang['submit'] = '送信';
$lang['reset'] = 'リセット';
$lang['update_outlet_category'] = 'Outlet Category';
$lang['update_profile_title'] = 'プロフィールを更新';
$lang['username'] = 'ユーザ名';
$lang['password'] = 'パスワード';
$lang['retype_password'] = 'パスワードの再入力';
$lang['retailer_name'] = '会社名';
$lang['role'] = '役割';
$lang['outlet'] = 'アウトレット店';
$lang['email'] = 'メールアドレス';
$lang['last_name'] = '姓';
$lang['first_name'] = '名';
$lang['newseventlist'] = 'News and Events';
$lang['employeelist'] = 'Employee List';
$lang['id'] = 'ID';
$lang['full_name'] = 'Full Name';
$lang['update_employee'] = 'Update Cashier';
$lang['create_employee'] = 'Create New Cashier';
$lang['tracking_list'] = 'Customer Tracking';
$lang['passport'] = '旅券';

$lang['status'] = '状態';
$lang['shoes_and_bags'] = 'カバン・靴';
$lang['accessories_and_handicrafts'] = '宝飾品・民芸品';
$lang['golf_equipments'] = 'ゴルフ用品';
$lang['clothing'] = '洋服・着物';
$lang['health_food_products_commodities'] = '家電製品・薬品類';
$lang['cosmetics'] = '化粧品類';
$lang['beverages'] = '飲料類';
$lang['foods'] = '食品類';
$lang['cigarettes'] = 'たばこ';
$lang['health_food_products_consumables'] = '薬品類(消耗品)';
$lang['country'] = 'Country';

$lang['sales_account_information'] = '販売額情報';
$lang['today_sales'] = '今日の総売上高';
$lang['month_to_date_sale'] = '今月の総売上高';
$lang['rolling_6_month'] = 'ローリング6か月の総販売額';

$lang['export_transaction'] = '取引履歴を出力';
$lang['date'] = '日付';
$lang['submit'] = '提出';
$lang['apply'] ='確定';
$lang['cancel'] = '取消';

$lang['ISSUED'] = '発行済';
	$lang['VOIDED'] = '取消';
	$lang['EXPIRED'] = '期限切れ';
	$lang['APPROVED'] ='承認済';
	$lang['REJECTED'] = '却下済';
$lang['PROVISIONAL'] = '仮';

	$lang{'deleted_account'} = 'Deleted account';

	$lang['general_category'] = '一般物品';
	$lang['consumables_category'] = '消耗品';
	$lang['tax'] = '消費税額';
