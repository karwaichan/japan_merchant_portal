<?php

$lang['welcome_message'] = 'Welcome back';
$lang['dashboard'] = 'Dashboard';
$lang['merchant_portal'] = 'Merchant Portal';
$lang['menu'] = 'MENU';
$lang['qr_code'] = 'QR Code';
$lang['transaction'] = 'Transaction';
$lang['language'] = 'Language';
$lang['update_profile'] = 'Update Profile';
$lang['logout'] = 'Logout';
$lang['help'] = 'Help';
$lang['merchant_information'] = 'Merchant Information';
$lang['merchant'] = 'Merchant';
$lang['cashier'] = 'Cashier';
$lang['list_of_receipt'] = 'List Of Transaction';
$lang['transaction_id'] = 'Transaction Id';
$lang['amount'] = 'Amount (¥)';
$lang['customer_name'] = 'Customer Name';
$lang['date_of_purchase'] = 'Date of Purchase';
$lang['action'] = 'Action';
$lang['document_type'] = 'Document Type';
$lang['number'] = 'Passport No.';
$lang['nationality'] = 'Nationality';
$lang['rop'] = 'RoP';
$lang['cop'] = 'CoP';
$lang['update_outlet'] = 'Outlet Information';
$lang['submit'] = 'Submit';
$lang['reset'] = 'Reset';
$lang['update_outlet_category'] = 'Outlet Category';
$lang['update_profile_title'] = 'Update Profile';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['retype_password'] = 'Retype Password';
$lang['retailer_name'] = 'Retailer Name';
$lang['role'] = 'Role';
$lang['outlet'] = 'Outlet';
$lang['email'] = 'Email';
$lang['last_name'] = 'Last Name';
$lang['first_name'] = 'First Name';
$lang['newseventlist'] = 'News and Events';
$lang['employeelist'] = 'Cashier Management';
$lang['id'] = 'ID';
$lang['full_name'] = 'Full Name';
$lang['update_employee'] = 'Update Cashier';
$lang['create_employee'] = 'Create New Cashier';
$lang['tracking_list'] = 'Customer Tracking';
$lang['user_id'] = 'Customer';
$lang['mobile_id'] = 'Device ID';
$lang['created_date'] = 'Created Date';
$lang['contact_number'] = 'Contact Number';
$lang['outlet_number'] = 'Outlet Number';
$lang['passport'] = 'Passport';

$lang['shoes_and_bags'] = 'Shoes / bags';
$lang['accessories_and_handicrafts'] = 'Accessories / handicrafts';
$lang['golf_equipments'] = 'Golf equipments';
$lang['clothing'] = 'Clothing';
$lang['health_food_products_commodities'] = 'Health food products (commodities)';
$lang['cosmetics'] = 'Cosmetics';
$lang['beverages'] = 'Beverages';
$lang['foods'] = 'Foods';
$lang['cigarettes'] = 'Cigarettes';
$lang['health_food_products_consumables'] = 'Health food products (consumables)';
$lang['country'] = 'Country';
$lang['etrs'] = 'eTRS';



/* RECEIPT PRINT  */
$lang['print_eTRS_title'] = 'ELECTRONIC TOURIST REFUND TICKET';
$lang['central_refund_agency'] = 'CENTRAL REFUND AGENCY';
$lang['retailer_details'] = 'RETAILER DETAILS';
$lang['eTRS_retailer_name'] = 'Retailer Name: ';
$lang['eTRS_gst_reg_no'] = 'GST Reg No: ';
$lang['tourego_hotline'] = 'Hotline: +65 6635 8846';
$lang['date_of_issue'] = 'Date of Issue: ';
$lang['time_of_issue'] = 'Time of Issue: ';
$lang['tourist_details'] = 'TOURIST DETAILS';
$lang['passport_no'] = 'Passport No: ';
$lang['eTRS_nationality'] = 'Nationality: ';
$lang['item_summary'] = 'ITEM SUMMARY';
$lang['receipt_no'] = 'Receipt No';
$lang['eTRS_amount'] = 'Amount';
$lang['eTRS_sales_amount'] = 'Total Sales Amount Inc. GST:';
$lang['eTRS_gst_amount'] = 'GST Amount:';
$lang['eTRS_service_fee'] = 'Service Fee:';
$lang['eTRS_provisional_refund_amount'] = 'Provisional Refund Amount:';
$lang['ticket_detail_refund_note'] = 'Provisional refund amounts are subject to eligibility check and approval';
$lang['identifier'] = 'IDENTIFIER';
$lang['for_more_information'] = 'For More Information';
$lang['ticket_detail_note'] = "Please keep this eTRS Ticket, the receipts and goods for Customs' inspection.";

$lang['print_eTRS_ticket'] = 'Print the ticket? Please click ';
$lang['here'] = 'here';

/* TRANSACTION HISTORY */
$lang['created_at'] = 'Date of Issue';
$lang['etrs_doc_id'] = 'eTRS Doc-ID';
$lang['doc_id'] = 'Doc-ID';
$lang['eTRS'] = 'eTRS';
$lang['sales_amount'] = 'Total Sales Amount Inc. GST (SGD)';
$lang['gst_amount'] = 'GST Amount (SGD)';
$lang['refund_amount'] = 'Refund Amount (SGD)';
$lang['merchant_name'] = 'Merchant Name';
$lang['status'] = 'Status';
$lang['PROVISIONAL'] = 'PROVISIONAL';
$lang['VOIDED'] = 'VOIDED';
$lang['ISSUED'] = 'ISSUED';
$lang['PROCESSED'] = 'PROCESSED';
$lang['EXPIRED'] = 'EXPIRED';
$lang['abnormal_status'] = 'ABNORMAL';


	$lang['sales_account_information'] = 'Sales Account Information';
	$lang['today_sales'] = 'Today Total Sales Amount';
	$lang['month_to_date_sale'] = 'Month to Date Total Sales Amount';
	$lang['rolling_6_month'] = 'Rolling 6 Months Total Sales Amount';

	$lang['export_transaction'] = 'Export Transaction';
	$lang['date'] = 'Date';
	$lang['submit'] = 'Submit';
	$lang['apply'] ='Apply';
	$lang['cancel'] = 'Cancel';

	$lang['VOIDED'] = 'VOIDED';
	$lang['ISSUED'] = 'ISSUED';
	$lang['REJECTED'] = 'REJECTED';
	$lang['PROVISIONAL'] = 'PROVISIONAL';
	$lang['EXPIRED'] = 'EXPIRED';
	$lang['APPROVED'] = 'APPROVED';

	$lang{'deleted_account'} = 'Deleted account';

	$lang['general_category'] = 'General';
	$lang['consumables_category'] = 'Consumable';
	$lang['tax'] = 'Tax';