<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('MODEL_CACHE_ENABLED',                   0);
define('MODEL_CACHE_DEFAULT_TIME',              3600);
//define('WEBSERVICE_URL',                        'http://demoadmin.tourego.com.sg/en/api/');
define('WEBSERVICE_URL2',                        'http://demoservice.tourego.com.sg/api/');
//define('WEBSERVICE_URL',                        'http://admin2.techlogx.info/en/api/');

define('USERTYPE_MANAGER',                      "4");
define('USERTYPE_CASHIER',                      "8");

define('ACCOUNT_STATUS_ACTIVE',                 'activate');
define('ACCOUNT_STATUS_DEACTIVE',		'deactive');

define('BASE_CURRENCY',                         'SGD');
define('SETTING_CURRENCY_TYPE',                 'currency_type');
define('SETTING_BASE_CURRENCY',                 'base_currency');
define('SETTING_BASE_TAX',                      'base_tax');
define('LIMIT_PER_PAGE_10',                     '10');
define('NUMBER_FLOATING_POINT',                 '2');

define("LANGUAGE_CODE", serialize (array ("english" => "en", "chinese" => "zh", "japanese" => "jp")));

define('SITE_NAME',                             'Tourego - 販売者ポータル');
//define('SITE_NAME',                             'Tourego - Merchant Portal');
define ("ROLES", serialize (array (4 => "Manager", 8 => "Cashier")));

#Changed on 20200319, the amount increased to 30 instead of existing 27. DELETED SOON.
define ('JPN_RESIDENT_STATUS', serialize (array (
                                                    1 => "外交", 
                                                    2 => "公用", 
                                                    3 => "教授", 
                                                    4 => "芸術", 
                                                    5 => "宗教", 
                                                    6 => "報道", 
                                                    7 => "投資・経営", 
                                                    8 => "法律・会計業務", 
                                                    9 => "医療", 
                                                    10 => "研究", 
                                                    11 => "教育", 
                                                    12 => "技術", 
                                                    13 => "人文知識・国際業務", 
                                                    14 => "企業内転勤", 
                                                    15 => "興行", 
                                                    16 => "技能", 
                                                    17 => "技能実習", 
                                                    18 => "文化活動", 
                                                    19 => "短期滞在", 
                                                    20 => "留学", 
                                                    21 => "研修", 
                                                    22 => "家族滞在", 
                                                    23 => "特定活動", 
                                                    24 => "永住者", 
                                                    25 => "日本人の配偶者等", 
                                                    26 => "永住者の配偶者等", 
                                                    27 => "定住者"
                                                )));
define('JPN_NEW_RESIDENT_STATUS', serialize([
	19 => '短期滞在',
	1  => '外交',
	2  => '公用',
	3  => '研修',
	4  => '留学',
	5  => '文化活動',
	6  => '家族滞在',
	7  => '特定活動',
	8  => '教授',
	9  => '教育',
	10 => '高度専門職',
	11 => '経営・管理',
	12 => '研究',
	13 => '技術・人文知識・国際業務',
	14 => '技能',
	15 => '企業内転勤',
	16 => '介護',
	17 => '技能実習',
	18 => '特定技能',
	20 => '医療',
	21 => '芸術',
	22 => '宗教',
	23 => '報道',
	24 => '法律・会計業務',
	25 => '興行',
	26 => '永住者',
	27 => '特別永住者',
	28 => '日本人の配偶者等',
	29 => '永住者の配偶者等',
	30 => '定住者'
]));

define ("JPN_COUNTRY", serialize (array (
                                                    "SGP" => "シンガポール", 
                                                    "CHN" => "中国", 
                                                    "MYS" => "マレーシア", 
                                                    "USA" => "米国", 
                                                    "CAN" => "カナダ", 
                                                    "VNM" => "ベトナム",
                                                    "GBR" => "イギリス",
                                                    "IND" => "インド", 
                                                    "AUS" => "オーストラリア", 
                                                    "LVA" => "ラトビア", 
                                                    "UKR" => "ウクライナ",
                                                    "BGD" => "バングラデシュ",
                                                    "POL" => "ポーランド",
                                                )));

define ('CATEGORY', serialize (array (
                                                    1 => "shoes_and_bags", 
                                                    2 => "accessories_and_handicrafts", 
                                                    3 => "golf_equipments", 
                                                    4 => "clothing", 
                                                    5 => "health_food_products_commodities", 
                                                    6 => "cosmetics",
                                                    7 => "beverages",
                                                    8 => "foods", 
                                                    9 => "cigarettes", 
                                                    10 => "health_food_products_consumables",
													11 => 'consumables',
													12 => 'general'
                                                )));


define("TOUREGO", 'TOUREGO');
define("TOUREGO_GST_REG_NO", 'GST No: 201421746H');
define("TOUREGO_ADDRESS", '133 Cecil Street #10-01 Singapore 069535');
define("FOR_MORE_INFORMATION_LINK", 'Website: www.tourego.com');

/* eTRS status */
define('PROVISIONAL', 'PROVISIONAL');
define('VOIDED', 'VOIDED');
define('ISSUED', 'ISSUED');
define('PROCESSED', 'PROCESSED');
define('EXPIRED', 'EXPIRED');

define("RECEIPT_STATUS", serialize (array (2 => "RECEIPT_STATUS_PENDING", 3 => "RECEIPT_STATUS_BEING_PROCESSED", 4 => "RECEIPT_STATUS_VOIDED", 5 => "RECEIPT_STATUS_PROCESSED", 6 => "RECEIPT_STATUS_EXPIRED")));
define('RECEIPT_STATUS_PENDING', 'RECEIPT_STATUS_PENDING');
define('RECEIPT_STATUS_BEING_PROCESSED', 'RECEIPT_STATUS_BEING_PROCESSED');
define('RECEIPT_STATUS_VOIDED', 'RECEIPT_STATUS_VOIDED');
define('RECEIPT_STATUS_PROCESSED', 'RECEIPT_STATUS_PROCESSED');
define('RECEIPT_STATUS_EXPIRED', 'RECEIPT_STATUS_EXPIRED');
/* End of file constants.php */
/* Location: ./application/config/constants.php */