<?php
//    $type = 1 for controller functions
//    $type = 0 for validation
    function validatePermission($permissions, $class = null, $method = null, $type = 0, $isAjax = 0){
        if ($permissions){
            $CI =& get_instance();
            if (isset($permissions[$class]) && ($permissions[$class] == 'read' || $permissions[$class] == 'modify')){
                if (isset($permissions[$method]) && ($permissions[$method] == 'read' || $permissions[$method] == 'modify')){
                    return $permissions[$method];
                }
            }
            if ($type){
                $CI->session->set_flashdata('error', 'No permission!');
                if (!$isAjax){
                    redirect('dashboard', 'refresh');
                }
                    
            }
            return false;
        }
        return true;
    }