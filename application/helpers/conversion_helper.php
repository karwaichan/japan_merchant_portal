<?php
  //convert unixtime to format "year month day hour minute second ago"
    function timeToDayago($time){
        $endDate = time();    // current time
        $diff = $endDate - $time; /// diffrence
        $converted_time = "";
        if ($diff < 0) return $converted_time;
        $years = floor($diff/(86400*365));  ///  number of years 
        $months = floor(($diff-$years*86400*365)/(86400*30));  ///  number of months 
        $days = floor(($diff - $years*(86400*365) - $months*(86400*30))/86400);  ///  number of days 
        $hours = floor(($diff - $years*(86400*365) - $months*(86400*30) - $days*86400)/(60 * 60));  ////  number of hours
        $min = floor(($diff-($years*(86400*365) + $months*(86400*30) + $days*86400+$hours*3600))/60);///// numbers of minute
        $second = $diff - ($years*(86400*365) + $months*(86400*30) + $days*86400+$hours*3600+$min*60); //// secondes
        if($years > 0) $converted_time .= $years." Year(s) ";
        if($months > 0) $converted_time .= $months." Month(s) ";
        if($days > 0) $converted_time .= $days." Day(s)";
        elseif($hours > 0) $converted_time .= $hours." Hour(s)";
        elseif($min > 0) $converted_time .= $min." Minute(s)";
        else echo $converted_time .= "Just second(s)";
        return $converted_time." ago";
    }

    function arraykeycounter($data, $input){
        if (isset($data[key($input)])){
            $data[key($input)] += $input[key($input)];
        } else {
            $data[key($input)] = $input[key($input)];
        }
        return $data;
    }

    function currencyConverter($amount, $to, $from = null){
        if (!$from){
            $from = basecurrency();
        }
        if ($to == $from)
            return number_format($amount, NUMBER_FLOATING_POINT);
        $CI =& get_instance();
        
        $check = $CI->currency_model->getByFromTo($from, $to);
        if (!$check){
            $CI->currency_library->validateExistingCurrencyPair($to);
            $check = $CI->currency_model->getByFromTo($from, $to);
        }
        return number_format($amount * $check->rate, NUMBER_FLOATING_POINT);
    }

    function gettax(){
        $CI =& get_instance();
        $setting = $CI->setting_model->getByType(SETTING_BASE_TAX);
        if (!$setting){
            $CI->session->set_flashdata('error', 'Please set tax before continue!');
            redirect('/currency/taxsetting', 'refresh');
        }
        return $setting[0]->value;
    }

    function basecurrency(){
        $CI =& get_instance();
        $setting = $CI->setting_model->getByType(SETTING_BASE_CURRENCY);
        if (!$setting){
            $CI->session->set_flashdata('error', 'Please set base currency before continue!');
            redirect('/currency/currencybase', 'refresh');
        }
        return $setting[0]->value;
    }

    function currencyConverterSymbol($to, $from = null){
        return $to.' '.currencyConverter($to, $from = null);
    }
    
    function getdaterange($date_select, $from_date = null, $to_date = null){
        switch ($date_select) {
            case "0":
                $from_date = strtotime($from_date);
                $to_date = strtotime($to_date)+3600*24;
              break;
            case "today":
                $from_date = strtotime("today");
                $to_date = strtotime("today + 1 day");
              break;
            case "yesterday":
                $from_date = strtotime("yesterday");
                $to_date = strtotime("today");
              break;
            case "last7days":
                $from_date = strtotime("today - 7 days");
                $to_date = strtotime("today + 1 day");
              break;
            case "thismonth":
                $from_date = strtotime(date('Y-m'));
                $to_date = strtotime("today + 1 day");
              break;
            case "lastmonth":
                $from_date = strtotime(date('Y-m')." -1 month");
                $to_date = strtotime(date('Y-m'));
              break;
            case "thisyear":
                $from_date = strtotime(date('Y-01-01'));
                $to_date = strtotime("today + 1 day");
              break;
            case "lastyear":
                $from_date = strtotime(date('Y-01-01')." -1 year");
                $to_date = strtotime(date('Y-01-01'));
              break;
            case "all":
            default:
                $from_date = 0;
                $to_date = strtotime("today + 1 day");
              break;
        }
        return array('from_date' => $from_date, 'to_date' => $to_date);
    }
    
    function getfromdate($date_select){
        switch ($date_select) {
            case "0":
                $to_date = time();
              break;
            case "15":
                $to_date = strtotime("today - 15 days");
              break;
            case "20":
                $to_date = strtotime("today - 20 day");
              break;
            case "25":
                $to_date = strtotime("today - 25 day");
              break;
            case "30":
                $to_date = strtotime("today - 30 day");
              break;
            case "35":
                $to_date = strtotime("today - 35 day");
              break;
            case "40":
                $to_date = strtotime("today - 40 day");
              break;
            case "45":
                $to_date = strtotime("today - 45 day");
              break;
            case "all":
            default:
                $to_date = strtotime("today - 15 day");
              break;
        }
        return $to_date;
    }