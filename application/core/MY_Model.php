<?php

class MY_Model extends CI_Model{

    var $db_name;

    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->load->driver('cache');
    }

    function getTables()
    {
        return $this->db->list_tables();
    }

    function getFields()
    {
        return $this->db->list_fields($this->db_name);
    }

    function getById($id)
    {
        if (MODEL_CACHE_ENABLED) {
            $result = $this->getFromCached('getById_' . $id);
            if ($result) {
                return $result;
            }
        }
        $this->db->where('id', $id);
        $query = $this->db->get($this->db_name);
        if ($query->num_rows() == 0) {
            return false;
        }
        $result = $query->first_row();
        if (MODEL_CACHE_ENABLED) {
            $this->addToCached('getById_' . $id, $result);
        }
        return $result;
    }

    function getAll(){
        if (MODEL_CACHE_ENABLED) {
            $result = $this->getFromCached('getAll');
            if ($result) {
                return $result;
            }
        }
        $this->db->order_by('id asc');
        $query = $this->db->get($this->db_name);
        if ($query->num_rows() == 0) {
            return false;
        }
        $result = $query->result();
        if (MODEL_CACHE_ENABLED) {
            $this->addToCached('getAll', $result);
        }
        return $result;
    }
    
    // get all record count
    function getTotalRecord(){
        if (MODEL_CACHE_ENABLED) {
            $result = $this->getFromCached('getTotalRecord');
            if ($result) {
                return $result;
            }
        }
        
        $result = $this->db->count_all($this->db_name);
        
        if (MODEL_CACHE_ENABLED) {
            $this->addToCached('getTotalRecord', $result);
        }
        return $result;
    }

    function updateById($id, $data){
        $this->db->where('id', $id);
        $data['modifiedon'] = time();
        $data['modifiedby'] = trim( $this->session->userdata('accountId'));
        $data['modifiedip'] = trim($this->input->ip_address());
        
        $this->db->update($this->db_name, $data);
        if ($this->db->affected_rows() > 0) {
            if (MODEL_CACHE_ENABLED) {
                $this->deleteCachedById($id);
            }
            return TRUE;
        }
        
        return FALSE;
    }
    
    // Delete cache by Id
    function deleteCachedById($id){
        $this->deleteFromCached('getAll');
        $this->deleteFromCached('getFull');
        $this->deleteFromCached('getTotalRecord');
        $this->deleteFromCached('getById_' . $id);
    }

    function deleteCachedByAll(){
        $this->deleteFromCached('getAll');
        $this->deleteFromCached('getFull');
        $this->deleteFromCached('getTotalRecord');
    }
    
    // Add query to server's cache
    function addToCached($id, $query_result, $time = MODEL_CACHE_DEFAULT_TIME)
    {
        if (MEMCACHED_ENABLED) {
            $this->memcached_library->delete($this->db->database.$this->db_name.$id);
            return @$this->memcached_library->add($this->db->database.$this->db_name.$id, $query_result, $time);
        }
        $this->deleteFromCached($id);
        return @$this->cache->file->save($this->db->database . $this->db_name . $id, $query_result, $time);
    }
    
    // Clear server's cache
    function clearCached()
    {
        if (MEMCACHED_ENABLED) {
            return $this->memcached_library->flush();
        }
        return $this->cache->file->clean();
    }
    
    // Delete server's cache
    function deleteFromCached($id){
        if (MEMCACHED_ENABLED) {
            return @$this->memcached_library->delete($this->db->database.$this->db_name.$id);
        }
        if (file_exists($this->config->item('cache_path') . $this->db->database . $this->db_name . $id)) {
            return @$this->cache->file->delete($this->db->database . $this->db_name . $id);
        }
        return false;
    }
    
    // Attempt to retrieve from server's cache
    function getFromCached($id){
        if (MEMCACHED_ENABLED) {
            return @$this->memcached_library->get($this->db->database.$this->db_name.$id);
        }
        if (file_exists($this->config->item('cache_path') . $this->db->database . $this->db_name . $id)) {
            return @$this->cache->file->get($this->db->database . $this->db_name . $id);
        }
        return false;
    }
}