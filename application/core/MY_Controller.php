<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        //check for session
        $sessionData = $this->session->all_userdata();
        // default template
        $this->layout->template('main');
        if (!isset($sessionData['site_lang'])){
            $this->session->set_userdata('site_lang', "english");
        }
        
        if($this->router->fetch_class() != 'welcome'){
            if(!isset($sessionData['userid']) || empty($sessionData['userid'])){
                redirect(base_url().'welcome', 'refresh');
            }
        }else if($this->router->fetch_class() == 'welcome'){	
            if(isset($sessionData['userid']) && !empty($sessionData['userid'])){
                redirect(base_url().'dashboard', 'refresh');
            }
        }
    }

}